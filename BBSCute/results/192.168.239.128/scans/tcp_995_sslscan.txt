Version: 2.0.10-static
OpenSSL 1.1.1l-dev  xx XXX xxxx

Connected to 192.168.239.128

Testing SSL server 192.168.239.128 on port 995 using SNI name 192.168.239.128

  SSL/TLS Protocols:
SSLv2     disabled
SSLv3     disabled
TLSv1.0   enabled
TLSv1.1   enabled
TLSv1.2   enabled
TLSv1.3   enabled

  TLS Fallback SCSV:
Server supports TLS Fallback SCSV

  TLS renegotiation:
Secure session renegotiation supported

  TLS Compression:
Compression disabled

  Heartbleed:
TLSv1.3 not vulnerable to heartbleed
TLSv1.2 not vulnerable to heartbleed
TLSv1.1 not vulnerable to heartbleed
TLSv1.0 not vulnerable to heartbleed

  Supported Server Cipher(s):
Preferred TLSv1.3  128 bits  TLS_AES_128_GCM_SHA256        Curve 25519 DHE 253
Accepted  TLSv1.3  256 bits  TLS_AES_256_GCM_SHA384        Curve 25519 DHE 253
Accepted  TLSv1.3  256 bits  TLS_CHACHA20_POLY1305_SHA256  Curve 25519 DHE 253
Accepted  TLSv1.3  128 bits  TLS_AES_128_CCM_SHA256        Curve 25519 DHE 253
Preferred TLSv1.2  256 bits  ECDHE-RSA-AES256-GCM-SHA384   Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  DHE-RSA-AES256-GCM-SHA384     DHE 3072 bits
Accepted  TLSv1.2  256 bits  ECDHE-RSA-CHACHA20-POLY1305   Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  DHE-RSA-CHACHA20-POLY1305     DHE 3072 bits
Accepted  TLSv1.2  256 bits  DHE-RSA-AES256-CCM            DHE 3072 bits
Accepted  TLSv1.2  128 bits  ECDHE-RSA-AES128-GCM-SHA256   Curve 25519 DHE 253
Accepted  TLSv1.2  128 bits  DHE-RSA-AES128-GCM-SHA256     DHE 3072 bits
Accepted  TLSv1.2  128 bits  DHE-RSA-AES128-CCM            DHE 3072 bits
Accepted  TLSv1.2  256 bits  ECDHE-RSA-AES256-SHA          Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  DHE-RSA-AES256-SHA            DHE 3072 bits
Accepted  TLSv1.2  128 bits  ECDHE-RSA-AES128-SHA          Curve 25519 DHE 253
Accepted  TLSv1.2  128 bits  DHE-RSA-AES128-SHA            DHE 3072 bits
Accepted  TLSv1.2  256 bits  AES256-GCM-SHA384            
Accepted  TLSv1.2  256 bits  AES256-CCM                   
Accepted  TLSv1.2  128 bits  AES128-GCM-SHA256            
Accepted  TLSv1.2  128 bits  AES128-CCM                   
Accepted  TLSv1.2  256 bits  AES256-SHA                   
Accepted  TLSv1.2  128 bits  AES128-SHA                   
Preferred TLSv1.1  256 bits  ECDHE-RSA-AES256-SHA          Curve 25519 DHE 253
Accepted  TLSv1.1  256 bits  DHE-RSA-AES256-SHA            DHE 3072 bits
Accepted  TLSv1.1  128 bits  ECDHE-RSA-AES128-SHA          Curve 25519 DHE 253
Accepted  TLSv1.1  128 bits  DHE-RSA-AES128-SHA            DHE 3072 bits
Accepted  TLSv1.1  256 bits  AES256-SHA                   
Accepted  TLSv1.1  128 bits  AES128-SHA                   
Preferred TLSv1.0  256 bits  ECDHE-RSA-AES256-SHA          Curve 25519 DHE 253
Accepted  TLSv1.0  256 bits  DHE-RSA-AES256-SHA            DHE 3072 bits
Accepted  TLSv1.0  128 bits  ECDHE-RSA-AES128-SHA          Curve 25519 DHE 253
Accepted  TLSv1.0  128 bits  DHE-RSA-AES128-SHA            DHE 3072 bits
Accepted  TLSv1.0  256 bits  AES256-SHA                   
Accepted  TLSv1.0  128 bits  AES128-SHA                   

  Server Key Exchange Group(s):
TLSv1.3  128 bits  secp256r1 (NIST P-256)
TLSv1.3  192 bits  secp384r1 (NIST P-384)
TLSv1.3  128 bits  x25519
TLSv1.3  128 bits  ffdhe3072
TLSv1.3  150 bits  ffdhe4096
TLSv1.3  175 bits  ffdhe6144
TLSv1.3  192 bits  ffdhe8192
TLSv1.2  128 bits  secp256r1 (NIST P-256)
TLSv1.2  192 bits  secp384r1 (NIST P-384)
TLSv1.2  260 bits  secp521r1 (NIST P-521)
TLSv1.2  128 bits  x25519

  SSL Certificate:
    Certificate blob:
-----BEGIN CERTIFICATE-----
MIIE6zCCA1OgAwIBAgIBATANBgkqhkiG9w0BAQsFADCBjjESMBAGA1UEAxMJbG9j
YWxob3N0MS0wKwYDVQQLEyRBdXRvbWF0aWNhbGx5LWdlbmVyYXRlZCBQT1AzIFNT
TCBrZXkxHDAaBgNVBAoTE0NvdXJpZXIgTWFpbCBTZXJ2ZXIxETAPBgNVBAcTCE5l
dyBZb3JrMQswCQYDVQQIEwJOWTELMAkGA1UEBhMCVVMwHhcNMjAwOTE3MTYyODA2
WhcNMjEwOTE3MTYyODA2WjCBjjESMBAGA1UEAxMJbG9jYWxob3N0MS0wKwYDVQQL
EyRBdXRvbWF0aWNhbGx5LWdlbmVyYXRlZCBQT1AzIFNTTCBrZXkxHDAaBgNVBAoT
E0NvdXJpZXIgTWFpbCBTZXJ2ZXIxETAPBgNVBAcTCE5ldyBZb3JrMQswCQYDVQQI
EwJOWTELMAkGA1UEBhMCVVMwggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIB
gQDIBsPdZDb45UVqWpRZiqVqbC1vCd4mXw2Qif5BWHME351unfanqY3pywEGOPha
J7HuyhLzSF2dWmF3z8I+g4C5q4xO3MglQ2CHfJyAxvfk+pD7omcaFi3N7j5JnPsJ
enmVWNalaI6bCPGcf1P5ymeHLK61FqL+/Rlaw2x2rsbA+XxNXPdrqOFA4XinNb09
EiO/qSCmL1r9Q9bTrMkByecJ7iEUK5EwQBDUCoUywnJ+Pu0gExw3mdscKSb3oNw8
IBZhY6jXGMqjrBQ4pwqWWV9/ljEXEQj6gEqSjweOyYoA3OuB9+5ppTBRzpB22bMq
kvHnCO0u9h6tSjwZ7+vxynuaVKuyxcfMLl4bO7EYy/dZjJ2fWHZtGkGm4q/HZ97r
M8gYeEoEr5s5jNmRVrxejO/9w5zNsrZCPt///bFF+h1TWvV1IaCchuxE32srOQfl
UUgJ4XhgcqD6DaG5nqtJ7LrpN0TcvP373c6J8CJ2b/JSuyHP04TvAEEJYj+vMnVG
ZsUCAwEAAaNSMFAwDAYDVR0TAQH/BAIwADAhBgNVHREEGjAYgRZwb3N0bWFzdGVy
QGV4YW1wbGUuY29tMB0GA1UdDgQWBBTFu1JxVBbqWHll0UH7hPEBv+KFizANBgkq
hkiG9w0BAQsFAAOCAYEADawbz6QNBk3+miizqqXooRU2wZcx+Du6iM92rKLNZCq+
wEXZEdxGi/WSOY7UxrJbP6dfxvyIpmwsZjFOqNr3w3l0Y/Nwdw23o6gxOlkDFt9p
dTopD2CYEwmIiRgT60ulZ+gIcHeJu4ExVQ8PDxRnWPEECodQHWrPBVyRa585FQB0
YpUMjahA98qcvWCaNAI824uDZ9frptM4syzTKFjl/CYuhXGdNDTbq1fjaOJ1MXvh
qCzKG3A4JLf3R448QtcB5n8LhgwO7w6y7XjBAPYmOcEiuBhRTzy2dzKHLhxXFaHI
J9A8csWHebvYr80Th7ELpkNgXCnu3mbr2DkWk7hbYSTfcmgi+ISkd892MOllLiu/
3dWqund8Bg2gOExQbdeyOMg4+WeQedUQ4sWjI8s7QL9o6H9kwRVsabkYGxfl56Zz
xrI2K3odZgnCnFCzlu/2cbuzNfF7DvvKHs057F3PzIVxSPuoTcgLNllr4tJqABjY
JpyNakJF76tDW03eEoAT
-----END CERTIFICATE-----
    Version: 2
    Serial Number: 1 (0x1)
    Signature Algorithm: sha256WithRSAEncryption
    Issuer: /CN=localhost/OU=Automatically-generated POP3 SSL key/O=Courier Mail Server/L=New York/ST=NY/C=US
    Not valid before: Sep 17 16:28:06 2020 GMT
    Not valid after: Sep 17 16:28:06 2021 GMT
    Subject: /CN=localhost/OU=Automatically-generated POP3 SSL key/O=Courier Mail Server/L=New York/ST=NY/C=US
    Public Key Algorithm: NULL
    RSA Public Key: (3072 bit)
      RSA Public-Key: (3072 bit)
      Modulus:
          00:c8:06:c3:dd:64:36:f8:e5:45:6a:5a:94:59:8a:
          a5:6a:6c:2d:6f:09:de:26:5f:0d:90:89:fe:41:58:
          73:04:df:9d:6e:9d:f6:a7:a9:8d:e9:cb:01:06:38:
          f8:5a:27:b1:ee:ca:12:f3:48:5d:9d:5a:61:77:cf:
          c2:3e:83:80:b9:ab:8c:4e:dc:c8:25:43:60:87:7c:
          9c:80:c6:f7:e4:fa:90:fb:a2:67:1a:16:2d:cd:ee:
          3e:49:9c:fb:09:7a:79:95:58:d6:a5:68:8e:9b:08:
          f1:9c:7f:53:f9:ca:67:87:2c:ae:b5:16:a2:fe:fd:
          19:5a:c3:6c:76:ae:c6:c0:f9:7c:4d:5c:f7:6b:a8:
          e1:40:e1:78:a7:35:bd:3d:12:23:bf:a9:20:a6:2f:
          5a:fd:43:d6:d3:ac:c9:01:c9:e7:09:ee:21:14:2b:
          91:30:40:10:d4:0a:85:32:c2:72:7e:3e:ed:20:13:
          1c:37:99:db:1c:29:26:f7:a0:dc:3c:20:16:61:63:
          a8:d7:18:ca:a3:ac:14:38:a7:0a:96:59:5f:7f:96:
          31:17:11:08:fa:80:4a:92:8f:07:8e:c9:8a:00:dc:
          eb:81:f7:ee:69:a5:30:51:ce:90:76:d9:b3:2a:92:
          f1:e7:08:ed:2e:f6:1e:ad:4a:3c:19:ef:eb:f1:ca:
          7b:9a:54:ab:b2:c5:c7:cc:2e:5e:1b:3b:b1:18:cb:
          f7:59:8c:9d:9f:58:76:6d:1a:41:a6:e2:af:c7:67:
          de:eb:33:c8:18:78:4a:04:af:9b:39:8c:d9:91:56:
          bc:5e:8c:ef:fd:c3:9c:cd:b2:b6:42:3e:df:ff:fd:
          b1:45:fa:1d:53:5a:f5:75:21:a0:9c:86:ec:44:df:
          6b:2b:39:07:e5:51:48:09:e1:78:60:72:a0:fa:0d:
          a1:b9:9e:ab:49:ec:ba:e9:37:44:dc:bc:fd:fb:dd:
          ce:89:f0:22:76:6f:f2:52:bb:21:cf:d3:84:ef:00:
          41:09:62:3f:af:32:75:46:66:c5
      Exponent: 65537 (0x10001)
    X509v3 Extensions:
      X509v3 Basic Constraints: critical
        CA:FALSE
      X509v3 Subject Alternative Name: 
        email:postmaster@example.com
      X509v3 Subject Key Identifier: 
        C5:BB:52:71:54:16:EA:58:79:65:D1:41:FB:84:F1:01:BF:E2:85:8B
  Verify Certificate:
    self signed certificate

  SSL Certificate:
Signature Algorithm: sha256WithRSAEncryption
RSA Key Strength:    3072

Subject:  localhost
Altnames: email:postmaster@example.com
Issuer:   localhost

Not valid before: Sep 17 16:28:06 2020 GMT
Not valid after:  Sep 17 16:28:06 2021 GMT
