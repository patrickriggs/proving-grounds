# Nmap 7.91 scan initiated Wed Sep  8 01:10:18 2021 as: nmap -vv --reason -Pn -sU -A --top-ports=20 --version-all -oN /home/tac0shell/hacking/offsec/pg/dawn/results/192.168.239.11/scans/_top_20_udp_nmap.txt -oX /home/tac0shell/hacking/offsec/pg/dawn/results/192.168.239.11/scans/xml/_top_20_udp_nmap.xml 192.168.239.11
Nmap scan report for 192.168.239.11
Host is up, received user-set (0.10s latency).
Scanned at 2021-09-08 01:10:18 EDT for 293s

PORT      STATE         SERVICE      REASON              VERSION
53/udp    closed        domain       port-unreach ttl 63
67/udp    closed        dhcps        port-unreach ttl 63
68/udp    closed        dhcpc        port-unreach ttl 63
69/udp    closed        tftp         port-unreach ttl 63
123/udp   closed        ntp          port-unreach ttl 63
135/udp   closed        msrpc        port-unreach ttl 63
137/udp   open          netbios-ns   udp-response        Samba nmbd netbios-ns (workgroup: WORKGROUP)
138/udp   open|filtered netbios-dgm  no-response
139/udp   closed        netbios-ssn  port-unreach ttl 63
161/udp   closed        snmp         port-unreach ttl 63
162/udp   closed        snmptrap     port-unreach ttl 63
445/udp   open|filtered microsoft-ds no-response
500/udp   open|filtered isakmp       no-response
514/udp   closed        syslog       port-unreach ttl 63
520/udp   open|filtered route        no-response
631/udp   open|filtered ipp          no-response
1434/udp  closed        ms-sql-m     port-unreach ttl 63
1900/udp  closed        upnp         port-unreach ttl 63
4500/udp  closed        nat-t-ike    port-unreach ttl 63
49152/udp closed        unknown      port-unreach ttl 63
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.91%E=4%D=9/8%OT=%CT=%CU=53%PV=Y%DS=2%DC=T%G=N%TM=613846DF%P=x86_64-pc-linux-gnu)
SEQ(II=I)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops
Service Info: Host: DAWN

Host script results:
| nbstat: NetBIOS name: DAWN, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| Names:
|   DAWN<00>             Flags: <unique><active>
|   DAWN<03>             Flags: <unique><active>
|   DAWN<20>             Flags: <unique><active>
|   \x01\x02__MSBROWSE__\x02<01>  Flags: <group><active>
|   WORKGROUP<00>        Flags: <group><active>
|   WORKGROUP<1d>        Flags: <unique><active>
|   WORKGROUP<1e>        Flags: <group><active>
| Statistics:
|   00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
|   00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
|_  00 00 00 00 00 00 00 00 00 00 00 00 00 00

TRACEROUTE (using port 4500/udp)
HOP RTT      ADDRESS
1   96.44 ms 192.168.49.1
2   96.52 ms 192.168.239.11

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Wed Sep  8 01:15:11 2021 -- 1 IP address (1 host up) scanned in 293.22 seconds
