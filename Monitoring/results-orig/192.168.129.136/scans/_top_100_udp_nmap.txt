# Nmap 7.92 scan initiated Sun Nov  7 09:55:59 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/xml/_top_100_udp_nmap.xml 192.168.129.136
Increasing send delay for 192.168.129.136 from 0 to 50 due to 11 out of 18 dropped probes since last increase.
Warning: 192.168.129.136 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.129.136 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.129.136 from 400 to 800 due to 11 out of 13 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -208868 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -205694 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -205694 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -342012 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -342012 microseconds.  Ignoring time.
Nmap scan report for 192.168.129.136
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-07 09:55:59 EST for 242s

PORT      STATE         SERVICE         REASON              VERSION
7/udp     closed        echo            port-unreach ttl 63
9/udp     closed        discard         port-unreach ttl 63
17/udp    closed        qotd            port-unreach ttl 63
19/udp    open|filtered chargen         no-response
49/udp    closed        tacacs          port-unreach ttl 63
53/udp    open|filtered domain          no-response
67/udp    closed        dhcps           port-unreach ttl 63
68/udp    closed        dhcpc           port-unreach ttl 63
69/udp    closed        tftp            port-unreach ttl 63
80/udp    open|filtered http            no-response
88/udp    closed        kerberos-sec    port-unreach ttl 63
111/udp   closed        rpcbind         port-unreach ttl 63
120/udp   closed        cfdptkt         port-unreach ttl 63
123/udp   closed        ntp             port-unreach ttl 63
135/udp   closed        msrpc           port-unreach ttl 63
136/udp   open|filtered profile         no-response
137/udp   closed        netbios-ns      port-unreach ttl 63
138/udp   closed        netbios-dgm     port-unreach ttl 63
139/udp   open|filtered netbios-ssn     no-response
158/udp   open|filtered pcmail-srv      no-response
161/udp   closed        snmp            port-unreach ttl 63
162/udp   open          snmp            udp-response        net-snmp; net-snmp SNMPv3 server
| snmp-info: 
|   enterprise: net-snmp
|   engineIDFormat: unknown
|   engineIDData: d5b56263edcc575f00000000
|   snmpEngineBoots: 12
|_  snmpEngineTime: 11m17s
177/udp   open|filtered xdmcp           no-response
427/udp   closed        svrloc          port-unreach ttl 63
443/udp   closed        https           port-unreach ttl 63
445/udp   closed        microsoft-ds    port-unreach ttl 63
497/udp   closed        retrospect      port-unreach ttl 63
500/udp   closed        isakmp          port-unreach ttl 63
514/udp   open|filtered syslog          no-response
515/udp   closed        printer         port-unreach ttl 63
518/udp   open|filtered ntalk           no-response
520/udp   closed        route           port-unreach ttl 63
593/udp   closed        http-rpc-epmap  port-unreach ttl 63
623/udp   closed        asf-rmcp        port-unreach ttl 63
626/udp   open|filtered serialnumberd   no-response
631/udp   open|filtered ipp             no-response
996/udp   closed        vsinet          port-unreach ttl 63
997/udp   open|filtered maitrd          no-response
998/udp   closed        puparp          port-unreach ttl 63
999/udp   closed        applix          port-unreach ttl 63
1022/udp  open|filtered exp2            no-response
1023/udp  closed        unknown         port-unreach ttl 63
1025/udp  closed        blackjack       port-unreach ttl 63
1026/udp  closed        win-rpc         port-unreach ttl 63
1027/udp  closed        unknown         port-unreach ttl 63
1028/udp  closed        ms-lsa          port-unreach ttl 63
1029/udp  closed        solid-mux       port-unreach ttl 63
1030/udp  closed        iad1            port-unreach ttl 63
1433/udp  open|filtered ms-sql-s        no-response
1434/udp  closed        ms-sql-m        port-unreach ttl 63
1645/udp  closed        radius          port-unreach ttl 63
1646/udp  closed        radacct         port-unreach ttl 63
1701/udp  open|filtered L2TP            no-response
1718/udp  closed        h225gatedisc    port-unreach ttl 63
1719/udp  closed        h323gatestat    port-unreach ttl 63
1812/udp  closed        radius          port-unreach ttl 63
1813/udp  closed        radacct         port-unreach ttl 63
1900/udp  closed        upnp            port-unreach ttl 63
2000/udp  closed        cisco-sccp      port-unreach ttl 63
2048/udp  closed        dls-monitor     port-unreach ttl 63
2049/udp  open|filtered nfs             no-response
2222/udp  closed        msantipiracy    port-unreach ttl 63
2223/udp  open|filtered rockwell-csp2   no-response
3283/udp  closed        netassistant    port-unreach ttl 63
3456/udp  closed        IISrpc-or-vat   port-unreach ttl 63
3703/udp  closed        adobeserver-3   port-unreach ttl 63
4444/udp  closed        krb524          port-unreach ttl 63
4500/udp  closed        nat-t-ike       port-unreach ttl 63
5000/udp  open|filtered upnp            no-response
5060/udp  closed        sip             port-unreach ttl 63
5353/udp  closed        zeroconf        port-unreach ttl 63
5632/udp  closed        pcanywherestat  port-unreach ttl 63
9200/udp  closed        wap-wsp         port-unreach ttl 63
10000/udp open|filtered ndmp            no-response
17185/udp closed        wdbrpc          port-unreach ttl 63
20031/udp closed        bakbonenetvault port-unreach ttl 63
30718/udp closed        unknown         port-unreach ttl 63
31337/udp closed        BackOrifice     port-unreach ttl 63
32768/udp closed        omad            port-unreach ttl 63
32769/udp closed        filenet-rpc     port-unreach ttl 63
32771/udp closed        sometimes-rpc6  port-unreach ttl 63
32815/udp closed        unknown         port-unreach ttl 63
33281/udp open|filtered unknown         no-response
49152/udp closed        unknown         port-unreach ttl 63
49153/udp closed        unknown         port-unreach ttl 63
49154/udp closed        unknown         port-unreach ttl 63
49156/udp open|filtered unknown         no-response
49181/udp open|filtered unknown         no-response
49182/udp closed        unknown         port-unreach ttl 63
49185/udp open|filtered unknown         no-response
49186/udp closed        unknown         port-unreach ttl 63
49188/udp closed        unknown         port-unreach ttl 63
49190/udp closed        unknown         port-unreach ttl 63
49191/udp open|filtered unknown         no-response
49192/udp closed        unknown         port-unreach ttl 63
49193/udp closed        unknown         port-unreach ttl 63
49194/udp closed        unknown         port-unreach ttl 63
49200/udp open|filtered unknown         no-response
49201/udp closed        unknown         port-unreach ttl 63
65024/udp open|filtered unknown         no-response
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.92%E=4%D=11/7%OT=%CT=%CU=7%PV=Y%DS=3%DC=T%G=N%TM=6187E9F1%P=x86_64-pc-linux-gnu)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 3 hops

TRACEROUTE (using port 1813/udp)
HOP RTT       ADDRESS
1   103.71 ms 192.168.49.1
2   ...
3   100.50 ms 192.168.129.136

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 10:00:01 2021 -- 1 IP address (1 host up) scanned in 242.46 seconds
