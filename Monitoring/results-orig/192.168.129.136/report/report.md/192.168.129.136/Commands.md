```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_quick_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/xml/_quick_tcp_nmap.xml" 192.168.129.136

nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_full_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/xml/_full_tcp_nmap.xml" 192.168.129.136

nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/xml/_top_100_udp_nmap.xml" 192.168.129.136

nmap -vv --reason -Pn -T4 -sV -p 22 --script="banner,ssh2-enum-algos,ssh-hostkey,ssh-auth-methods" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp22/tcp_22_ssh_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp22/xml/tcp_22_ssh_nmap.xml" 192.168.129.136

nmap -vv --reason -Pn -T4 -sV -p 25 --script="banner,(smtp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/tcp_25_smtp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/xml/tcp_25_smtp_nmap.xml" 192.168.129.136

hydra smtp-enum://192.168.129.136:25/vrfy -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" 2>&1

feroxbuster -u http://192.168.129.136:80/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/common.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_feroxbuster_common.txt"

curl -sSik http://192.168.129.136:80/

curl -sSikf http://192.168.129.136:80/robots.txt

nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.129.136

whatweb --color=never --no-errors -a 3 -v http://192.168.129.136:80 2>&1

wkhtmltoimage --format png http://192.168.129.136:80/ /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_screenshot.png

nmap -vv --reason -Pn -T4 -sV -p 389 --script="banner,(ldap* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/tcp_389_ldap_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/xml/tcp_389_ldap_nmap.xml" 192.168.129.136

enum4linux -a -M -l -d 192.168.129.136 2>&1

feroxbuster -u https://192.168.129.136:443/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/common.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_feroxbuster_common.txt"

curl -sSik https://192.168.129.136:443/

curl -sSikf https://192.168.129.136:443/robots.txt

nmap -vv --reason -Pn -T4 -sV -p 443 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/xml/tcp_443_https_nmap.xml" 192.168.129.136

whatweb --color=never --no-errors -a 3 -v https://192.168.129.136:443 2>&1

wkhtmltoimage --format png https://192.168.129.136:443/ /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_screenshot.png

sslscan --show-certificate --no-colour 192.168.129.136:443 2>&1

hydra smtp-enum://192.168.129.136:25/expn -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" 2>&1

nmap -vv --reason -Pn -T4 -sU -sV -p 162 --script="banner,(snmp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/udp_162_snmp-nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/xml/udp_162_snmp_nmap.xml" 192.168.129.136

feroxbuster -u http://192.168.129.136:80/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/big.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_feroxbuster_big.txt"

feroxbuster -u https://192.168.129.136:443/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/big.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_feroxbuster_big.txt"

feroxbuster -u https://192.168.129.136:443/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/raft-large-words.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_feroxbuster_raft-large-words.txt"

feroxbuster -u http://192.168.129.136:80/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/raft-large-words.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_feroxbuster_raft-large-words.txt"


```