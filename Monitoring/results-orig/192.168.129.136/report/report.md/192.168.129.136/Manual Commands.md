```bash
[*] ssh on tcp/22

	[-] Bruteforce logins:

		hydra -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e nsr -s 22 -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp22/tcp_22_ssh_hydra.txt" ssh://192.168.129.136

		medusa -U "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e ns -n 22 -O "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp22/tcp_22_ssh_medusa.txt" -M ssh -h 192.168.129.136

[*] smtp on tcp/25

	[-] Try User Enumeration using "RCPT TO". Replace <TARGET-DOMAIN> with the target's domain name:

		hydra smtp-enum://192.168.129.136:25/rcpt -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/tcp_25_smtp_user-enum_hydra_rcpt.txt" -p <TARGET-DOMAIN>

[*] http on tcp/80

	[-] (feroxbuster) Multi-threaded recursive directory/file enumeration for web servers using various wordlists:

		feroxbuster -u http://192.168.129.136:80 -t 10 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -o /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_feroxbuster_dirbuster.txt

	[-] Credential bruteforcing commands (don't run these without modifying them):

		hydra -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e nsr -s 80 -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_auth_hydra.txt" http-get://192.168.129.136/path/to/auth/area

		medusa -U "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e ns -n 80 -O "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_auth_medusa.txt" -M http -h 192.168.129.136 -m DIR:/path/to/auth/area

		hydra -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e nsr -s 80 -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_form_hydra.txt" http-post-form://192.168.129.136/path/to/login.php:username=^USER^&password=^PASS^:invalid-login-message

		medusa -U "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e ns -n 80 -O "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_form_medusa.txt" -M web-form -h 192.168.129.136 -m FORM:/path/to/login.php -m FORM-DATA:"post?username=&password=" -m DENY-SIGNAL:"invalid login message"

	[-] (nikto) old but generally reliable web server enumeration tool:

		nikto -ask=no -h http://192.168.129.136:80 2>&1 | tee "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_nikto.txt"

	[-] (wpscan) WordPress Security Scanner (useful if WordPress is found):

		wpscan --url http://192.168.129.136:80/ --no-update -e vp,vt,tt,cb,dbe,u,m --plugins-detection aggressive --plugins-version-detection aggressive -f cli-no-color 2>&1 | tee "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_wpscan.txt"

[*] ldap on tcp/389

	[-] ldapsearch command (modify before running):

		ldapsearch -x -D "<username>" -w "<password>"" -p 389 -h 192.168.129.136 -b "dc=example,dc=com" -s sub "(objectclass=*) 2>&1 | tee > "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/tcp_389_ldap_all-entries.txt"

[*] http on tcp/443

	[-] (feroxbuster) Multi-threaded recursive directory/file enumeration for web servers using various wordlists:

		feroxbuster -u https://192.168.129.136:443 -t 10 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -o /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_feroxbuster_dirbuster.txt

	[-] Credential bruteforcing commands (don't run these without modifying them):

		hydra -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e nsr -s 443 -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_auth_hydra.txt" https-get://192.168.129.136/path/to/auth/area

		medusa -U "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e ns -n 443 -O "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_auth_medusa.txt" -M http -h 192.168.129.136 -m DIR:/path/to/auth/area

		hydra -L "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e nsr -s 443 -o "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_form_hydra.txt" https-post-form://192.168.129.136/path/to/login.php:username=^USER^&password=^PASS^:invalid-login-message

		medusa -U "/usr/share/seclists/Usernames/top-usernames-shortlist.txt" -P "/usr/share/seclists/Passwords/darkweb2017-top100.txt" -e ns -n 443 -O "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_form_medusa.txt" -M web-form -h 192.168.129.136 -m FORM:/path/to/login.php -m FORM-DATA:"post?username=&password=" -m DENY-SIGNAL:"invalid login message"

	[-] (nikto) old but generally reliable web server enumeration tool:

		nikto -ask=no -h https://192.168.129.136:443 2>&1 | tee "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_nikto.txt"

	[-] (wpscan) WordPress Security Scanner (useful if WordPress is found):

		wpscan --url https://192.168.129.136:443/ --no-update -e vp,vt,tt,cb,dbe,u,m --plugins-detection aggressive --plugins-version-detection aggressive -f cli-no-color 2>&1 | tee "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_wpscan.txt"


```