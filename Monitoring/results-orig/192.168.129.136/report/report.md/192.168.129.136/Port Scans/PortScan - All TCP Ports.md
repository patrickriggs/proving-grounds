```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_full_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/xml/_full_tcp_nmap.xml" 192.168.129.136
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_full_tcp_nmap.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_full_tcp_nmap.txt):

```
# Nmap 7.92 scan initiated Sun Nov  7 09:55:59 2021 as: nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/_full_tcp_nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/xml/_full_tcp_nmap.xml 192.168.129.136
Increasing send delay for 192.168.129.136 from 0 to 5 due to 176 out of 439 dropped probes since last increase.
Increasing send delay for 192.168.129.136 from 5 to 10 due to 17 out of 41 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -153157 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -153157 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -153884 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -153884 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -155237 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -155237 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -158220 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -158220 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157807 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157807 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -158420 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -158420 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157818 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157818 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157773 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157773 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157727 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -157727 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -76605 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -76605 microseconds.  Ignoring time.
Nmap scan report for 192.168.129.136
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-07 09:55:59 EST for 974s
Not shown: 65529 closed tcp ports (reset)
PORT     STATE SERVICE    REASON         VERSION
22/tcp   open  ssh        syn-ack ttl 63 OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 b8:8c:40:f6:5f:2a:8b:f7:92:a8:81:4b:bb:59:6d:02 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMqjHxSe8UVPDyihFSjxzMKsqU1gOWFrI7Er+/4I+RstLTBrLn1gIldFGff88zYFOy5EWc37eZR/or/4qU6zMdRItYfbdAkyoBbun3MOM9jucnXobM4qQ2TgFjWK4hLk5Gcee2vFN2msegVoNf4aXvlSolQunD6h5kxhoaZ5vn5ok8RTOHH8PDkdYTKHX5a8SxR1/KQn+9d1l1aJZo05VA7qfs1P6GHMoRgKooKgVrws9ttLS8lb6yoZS8EO2mGhze84/G3KSRXID0YevcSmai0Snx3iAI4DdaFZoMhQDxwsui8L8uJpLYK4MLN2UwkuPWVsogX/PEowweR8QnCNHn
|   256 e7:bb:11:c1:2e:cd:39:91:68:4e:aa:01:f6:de:e6:19 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDxJyi14JgYiOtkyw9tQR9j86Loo9eSElOnBTrO7YeJleiYWENLJxM/T0vYil9yPzWRz/QT/FC2sqOviJiiaBNo=
|   256 0f:8e:28:a7:b7:1d:60:bf:a6:2b:dd:a3:6d:d1:4e:a4 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKohQjgFvYRY5+ccAe3zwQ3CjcMFDzoyT3zdAP+lWxc3
25/tcp   open  smtp       syn-ack ttl 63 Postfix smtpd
|_smtp-commands: ubuntu, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN
| ssl-cert: Subject: commonName=ubuntu
| Issuer: commonName=ubuntu
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-09-08T17:59:00
| Not valid after:  2030-09-06T17:59:00
| MD5:   e067 1ea3 92c2 ec73 cb21 de0e 73df cb66
| SHA-1: e39c c9b6 c35b b608 3dd0 cd25 e60f cb61 6551 da77
| -----BEGIN CERTIFICATE-----
| MIICsjCCAZqgAwIBAgIJAMvrYyFKXQezMA0GCSqGSIb3DQEBCwUAMBExDzANBgNV
| BAMMBnVidW50dTAeFw0yMDA5MDgxNzU5MDBaFw0zMDA5MDYxNzU5MDBaMBExDzAN
| BgNVBAMMBnVidW50dTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMfU
| MtszkAvFxmsng/POeWCCF0bcBPmNp6ypRqh1ywyVB6qPlacE8tPM9cDK9t1XPqFz
| +kp7ZHaOlZbk9mvq9ihmvvmlutiM9MhojRMak9oqF5LX9gjhogPRrmKI6FtlrqDn
| 33DsOwNJCxXr2CqwBJeqmIsG5tJDeGoJjXbk9ga68Pwu450fWFH92FL0PTBoXJiV
| 9sjR8wjGyVDn1pTSMQYOIYRe7DrNVsITfLYHL99az2RcjpScOl4KcxV5KVrhsdJk
| wNY4F8g64YkUF/cKCQ4Lbk2KoKkzlq7Z84BFhjujzIwJzulxvaUI+JQELigDKaik
| eyb/iFo12IMCpIhCkV8CAwEAAaMNMAswCQYDVR0TBAIwADANBgkqhkiG9w0BAQsF
| AAOCAQEAVoDANDw/Aqp3SbfYfeRGNkXEZUPSYu3CzvjWG5StwsSOOxjoilae3wiT
| u5Wb3KH61G687ozMsA8kk5BUefGMl77Q74idC++zxwRXPyeCmJ9bEPlusgB2cAKT
| 216skYYuJ0T6xEfeRpY2bQCJMTagb6xzXQmOPC3VZGWX7oxDOTobws9A+eVC/6GK
| hReCKoTkBQU85fFrLxDV7MrQfxs2q+e5f+pXtKW+m4V/3fcrnP16uk6DB9yYO9Im
| mFsOPEhf+/rVjesBWL+5dzscZWcRC6z9OLNkhCYGkya5xrQ7ajCmXdG+G5ZQrOUg
| GO/4fjpxGPhhvZISI71SLM8q2cEcGQ==
|_-----END CERTIFICATE-----
|_ssl-date: TLS randomness does not represent time
80/tcp   open  http       syn-ack ttl 63 Apache httpd 2.4.18 ((Ubuntu))
|_http-title: Nagios XI
|_http-favicon: Unknown favicon MD5: 8E1494DD4BFF0FC523A2E2A15ED59D84
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
389/tcp  open  ldap       syn-ack ttl 63 OpenLDAP 2.2.X - 2.3.X
443/tcp  open  ssl/http   syn-ack ttl 63 Apache httpd 2.4.18 ((Ubuntu))
|_http-title: Nagios XI
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
| ssl-cert: Subject: commonName=192.168.1.6/organizationName=Nagios Enterprises/stateOrProvinceName=Minnesota/countryName=US/localityName=St. Paul/organizationalUnitName=Development
| Issuer: commonName=192.168.1.6/organizationName=Nagios Enterprises/stateOrProvinceName=Minnesota/countryName=US/localityName=St. Paul/organizationalUnitName=Development
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2020-09-08T18:28:08
| Not valid after:  2030-09-06T18:28:08
| MD5:   20f0 951f 8eff 1b69 ef3f 1b1e fb4c 361f
| SHA-1: cc40 0ad7 60cf 4959 1c92 d9ab 0f06 106c 18f6 6661
| -----BEGIN CERTIFICATE-----
| MIIDxTCCAq2gAwIBAgIBADANBgkqhkiG9w0BAQUFADB9MQswCQYDVQQGEwJVUzES
| MBAGA1UECAwJTWlubmVzb3RhMREwDwYDVQQHDAhTdC4gUGF1bDEbMBkGA1UECgwS
| TmFnaW9zIEVudGVycHJpc2VzMRQwEgYDVQQLDAtEZXZlbG9wbWVudDEUMBIGA1UE
| AwwLMTkyLjE2OC4xLjYwHhcNMjAwOTA4MTgyODA4WhcNMzAwOTA2MTgyODA4WjB9
| MQswCQYDVQQGEwJVUzESMBAGA1UECAwJTWlubmVzb3RhMREwDwYDVQQHDAhTdC4g
| UGF1bDEbMBkGA1UECgwSTmFnaW9zIEVudGVycHJpc2VzMRQwEgYDVQQLDAtEZXZl
| bG9wbWVudDEUMBIGA1UEAwwLMTkyLjE2OC4xLjYwggEiMA0GCSqGSIb3DQEBAQUA
| A4IBDwAwggEKAoIBAQCe4uFtqzOvsxrF7Krjw2Pz0x+2cX/9Kfw2jMhIbR0rb5Bl
| BiYb8ifgtbB05ZL2EqfE8e/I5EwVp/dtHUds4bJSv2FfEE4xzXU0SRw0LK4FQ6u1
| ZBB2HqTGhxCN0/rmLhf0/IriWAS6l3NOR58pJW/syaqKL4OSOvG248MndIKzwNBH
| 8vVGSgEKRD0qFxbqS3pCQTsejbCimqBSqAsBJMwBcOpQnfBip8EjcTWqD8mpfmMS
| 4tHhn8k2/7UMGWbSl1erpiZKL/1SQ/V2Z2mJBF+85x4J+Rz2ealAbVt1W+G1Cy6D
| vvsK9L+RLokdPHgrzSuZGNKrJxg3nkHKwRFkZbExAgMBAAGjUDBOMB0GA1UdDgQW
| BBRVunDEJGH/2XNnyJVQYllWcHHjFjAfBgNVHSMEGDAWgBRVunDEJGH/2XNnyJVQ
| YllWcHHjFjAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQArlT8PTnzT
| dz6wmMzY9/vnBMkRnvH7vuB1MfRlnTyDy4QcTpzDBgdjkvy6MYMxsQz3TTJ+OrOn
| zPdp1NzEFGDDJQUhE22F1kzpJX8XedlHV5YRhdDKokwh2kKcyEsW6obOlC9przI5
| MpJvndKTj69peQAxrWImjD2o70WMKcoOIlbNnbPmmsKiR6jtL6G0+3ic7jPgZRRb
| WmPLzYh7GWMik7R0DWkng2x2Hq1YKNWmiGtMv3fC/w5PRpNT+/VV0NfOOJu36VB/
| rilrUGlO5q0HSx2lf1QoxYDnkQZ8/nzfAzjCYj5M4WuGKzGmkB9GPDC/REfHdu8m
| sMSOoeFVpu/b
|_-----END CERTIFICATE-----
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
|_http-server-header: Apache/2.4.18 (Ubuntu)
5667/tcp open  tcpwrapped syn-ack ttl 63
Aggressive OS guesses: Linux 3.11 - 4.1 (95%), Linux 4.4 (95%), Linux 3.16 (93%), Linux 3.13 (92%), Linux 3.10 - 3.16 (92%), Linux 2.6.32 (91%), Linux 3.2 - 3.8 (91%), Linux 3.8 (91%), WatchGuard Fireware 11.8 (91%), Kyocera CopyStar CS-2560 printer (91%)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.92%E=4%D=11/7%OT=22%CT=1%CU=38721%PV=Y%DS=2%DC=T%G=Y%TM=6187ECC
OS:D%P=x86_64-pc-linux-gnu)SEQ(SP=104%GCD=1%ISR=108%TI=Z%TS=8)SEQ(SP=104%GC
OS:D=2%ISR=108%TI=Z%II=I%TS=8)OPS(O1=M506ST11NW7%O2=M506ST11NW7%O3=M506NNT1
OS:1NW7%O4=M506ST11NW7%O5=M506ST11NW7%O6=M506ST11)WIN(W1=7120%W2=7120%W3=71
OS:20%W4=7120%W5=7120%W6=7120)ECN(R=Y%DF=Y%T=40%W=7210%O=M506NNSNW7%CC=Y%Q=
OS:)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=N)T5(R=Y%DF=Y
OS:%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=N)T7(R=N)U1(R=Y%DF=N%T=40%IPL=16
OS:4%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%CD=S)

Uptime guess: 0.014 days (since Sun Nov  7 09:52:38 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=260 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: Host:  ubuntu; OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 143/tcp)
HOP RTT       ADDRESS
1   107.77 ms 192.168.49.1
2   107.78 ms 192.168.129.136

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 10:12:13 2021 -- 1 IP address (1 host up) scanned in 974.09 seconds

```
