```bash
nmap -vv --reason -Pn -T4 -sV -p 25 --script="banner,(smtp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/tcp_25_smtp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/xml/tcp_25_smtp_nmap.xml" 192.168.129.136
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/tcp_25_smtp_nmap.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/tcp_25_smtp_nmap.txt):

```
# Nmap 7.92 scan initiated Sun Nov  7 09:56:44 2021 as: nmap -vv --reason -Pn -T4 -sV -p 25 "--script=banner,(smtp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/tcp_25_smtp_nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp25/xml/tcp_25_smtp_nmap.xml 192.168.129.136
Nmap scan report for 192.168.129.136
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-07 09:56:45 EST for 36s

PORT   STATE SERVICE REASON         VERSION
25/tcp open  smtp    syn-ack ttl 63 Postfix smtpd
|_banner: 220 ubuntu ESMTP Postfix (Ubuntu)
| smtp-vuln-cve2010-4344: 
|_  The SMTP server is not Exim: NOT VULNERABLE
| ssl-cert: Subject: commonName=ubuntu
| Issuer: commonName=ubuntu
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-09-08T17:59:00
| Not valid after:  2030-09-06T17:59:00
| MD5:   e067 1ea3 92c2 ec73 cb21 de0e 73df cb66
| SHA-1: e39c c9b6 c35b b608 3dd0 cd25 e60f cb61 6551 da77
| -----BEGIN CERTIFICATE-----
| MIICsjCCAZqgAwIBAgIJAMvrYyFKXQezMA0GCSqGSIb3DQEBCwUAMBExDzANBgNV
| BAMMBnVidW50dTAeFw0yMDA5MDgxNzU5MDBaFw0zMDA5MDYxNzU5MDBaMBExDzAN
| BgNVBAMMBnVidW50dTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMfU
| MtszkAvFxmsng/POeWCCF0bcBPmNp6ypRqh1ywyVB6qPlacE8tPM9cDK9t1XPqFz
| +kp7ZHaOlZbk9mvq9ihmvvmlutiM9MhojRMak9oqF5LX9gjhogPRrmKI6FtlrqDn
| 33DsOwNJCxXr2CqwBJeqmIsG5tJDeGoJjXbk9ga68Pwu450fWFH92FL0PTBoXJiV
| 9sjR8wjGyVDn1pTSMQYOIYRe7DrNVsITfLYHL99az2RcjpScOl4KcxV5KVrhsdJk
| wNY4F8g64YkUF/cKCQ4Lbk2KoKkzlq7Z84BFhjujzIwJzulxvaUI+JQELigDKaik
| eyb/iFo12IMCpIhCkV8CAwEAAaMNMAswCQYDVR0TBAIwADANBgkqhkiG9w0BAQsF
| AAOCAQEAVoDANDw/Aqp3SbfYfeRGNkXEZUPSYu3CzvjWG5StwsSOOxjoilae3wiT
| u5Wb3KH61G687ozMsA8kk5BUefGMl77Q74idC++zxwRXPyeCmJ9bEPlusgB2cAKT
| 216skYYuJ0T6xEfeRpY2bQCJMTagb6xzXQmOPC3VZGWX7oxDOTobws9A+eVC/6GK
| hReCKoTkBQU85fFrLxDV7MrQfxs2q+e5f+pXtKW+m4V/3fcrnP16uk6DB9yYO9Im
| mFsOPEhf+/rVjesBWL+5dzscZWcRC6z9OLNkhCYGkya5xrQ7ajCmXdG+G5ZQrOUg
| GO/4fjpxGPhhvZISI71SLM8q2cEcGQ==
|_-----END CERTIFICATE-----
|_ssl-date: TLS randomness does not represent time
| ssl-enum-ciphers: 
|   TLSv1.0: 
|     ciphers: 
|       TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA (dh 2048) - C
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_SEED_CBC_SHA (dh 2048) - A
|       TLS_DH_anon_WITH_3DES_EDE_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_128_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_256_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_RC4_128_MD5 (dh 2048) - F
|       TLS_DH_anon_WITH_SEED_CBC_SHA (dh 2048) - F
|       TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA (secp256r1) - C
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_RC4_128_SHA (secp256r1) - C
|       TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_AES_128_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_AES_256_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_RC4_128_SHA (secp256r1) - F
|       TLS_RSA_WITH_3DES_EDE_CBC_SHA (rsa 2048) - C
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_RC4_128_MD5 (rsa 2048) - C
|       TLS_RSA_WITH_RC4_128_SHA (rsa 2048) - C
|       TLS_RSA_WITH_SEED_CBC_SHA (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: client
|     warnings: 
|       64-bit block cipher 3DES vulnerable to SWEET32 attack
|       Anonymous key exchange, score capped at F
|       Broken cipher RC4 is deprecated by RFC 7465
|       Ciphersuite uses MD5 for message integrity
|   TLSv1.1: 
|     ciphers: 
|       TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA (dh 2048) - C
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_SEED_CBC_SHA (dh 2048) - A
|       TLS_DH_anon_WITH_3DES_EDE_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_128_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_256_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_RC4_128_MD5 (dh 2048) - F
|       TLS_DH_anon_WITH_SEED_CBC_SHA (dh 2048) - F
|       TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA (secp256r1) - C
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_RC4_128_SHA (secp256r1) - C
|       TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_AES_128_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_AES_256_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_RC4_128_SHA (secp256r1) - F
|       TLS_RSA_WITH_3DES_EDE_CBC_SHA (rsa 2048) - C
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_RC4_128_MD5 (rsa 2048) - C
|       TLS_RSA_WITH_RC4_128_SHA (rsa 2048) - C
|       TLS_RSA_WITH_SEED_CBC_SHA (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: client
|     warnings: 
|       64-bit block cipher 3DES vulnerable to SWEET32 attack
|       Anonymous key exchange, score capped at F
|       Broken cipher RC4 is deprecated by RFC 7465
|       Ciphersuite uses MD5 for message integrity
|   TLSv1.2: 
|     ciphers: 
|       TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA (dh 2048) - C
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 (dh 2048) - A
|       TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - A
|       TLS_DHE_RSA_WITH_SEED_CBC_SHA (dh 2048) - A
|       TLS_DH_anon_WITH_3DES_EDE_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_128_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_128_CBC_SHA256 (dh 2048) - F
|       TLS_DH_anon_WITH_AES_128_GCM_SHA256 (dh 2048) - F
|       TLS_DH_anon_WITH_AES_256_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_AES_256_CBC_SHA256 (dh 2048) - F
|       TLS_DH_anon_WITH_AES_256_GCM_SHA384 (dh 2048) - F
|       TLS_DH_anon_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - F
|       TLS_DH_anon_WITH_RC4_128_MD5 (dh 2048) - F
|       TLS_DH_anon_WITH_SEED_CBC_SHA (dh 2048) - F
|       TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA (secp256r1) - C
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (secp256r1) - A
|       TLS_ECDHE_RSA_WITH_RC4_128_SHA (secp256r1) - C
|       TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_AES_128_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_AES_256_CBC_SHA (secp256r1) - F
|       TLS_ECDH_anon_WITH_RC4_128_SHA (secp256r1) - F
|       TLS_RSA_WITH_3DES_EDE_CBC_SHA (rsa 2048) - C
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_CBC_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - A
|       TLS_RSA_WITH_RC4_128_MD5 (rsa 2048) - C
|       TLS_RSA_WITH_RC4_128_SHA (rsa 2048) - C
|       TLS_RSA_WITH_SEED_CBC_SHA (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: client
|     warnings: 
|       64-bit block cipher 3DES vulnerable to SWEET32 attack
|       Anonymous key exchange, score capped at F
|       Broken cipher RC4 is deprecated by RFC 7465
|       Ciphersuite uses MD5 for message integrity
|_  least strength: F
|_smtp-commands: ubuntu, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN
| ssl-dh-params: 
|   VULNERABLE:
|   Anonymous Diffie-Hellman Key Exchange MitM Vulnerability
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use anonymous
|       Diffie-Hellman key exchange only provide protection against passive
|       eavesdropping, and are vulnerable to active man-in-the-middle attacks
|       which could completely compromise the confidentiality and integrity
|       of any data exchanged over the resulting session.
|     Check results:
|       ANONYMOUS DH GROUP 1
|             Cipher Suite: TLS_DH_anon_WITH_AES_256_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: Unknown/Custom-generated
|             Modulus Length: 2048
|             Generator Length: 8
|             Public Key Length: 2048
|     References:
|_      https://www.ietf.org/rfc/rfc2246.txt
Service Info: Host:  ubuntu

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 09:57:21 2021 -- 1 IP address (1 host up) scanned in 37.14 seconds

```
