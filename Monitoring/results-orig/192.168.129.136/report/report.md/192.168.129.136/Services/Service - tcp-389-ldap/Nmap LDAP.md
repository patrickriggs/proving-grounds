```bash
nmap -vv --reason -Pn -T4 -sV -p 389 --script="banner,(ldap* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/tcp_389_ldap_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/xml/tcp_389_ldap_nmap.xml" 192.168.129.136
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/tcp_389_ldap_nmap.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/tcp_389_ldap_nmap.txt):

```
# Nmap 7.92 scan initiated Sun Nov  7 09:56:44 2021 as: nmap -vv --reason -Pn -T4 -sV -p 389 "--script=banner,(ldap* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/tcp_389_ldap_nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/xml/tcp_389_ldap_nmap.xml 192.168.129.136
Nmap scan report for 192.168.129.136
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-07 09:56:45 EST for 22s

PORT    STATE SERVICE REASON         VERSION
389/tcp open  ldap    syn-ack ttl 63 OpenLDAP 2.2.X - 2.3.X
| ldap-search: 
|   Context: dc=nodomain
|     dn: dc=nodomain
|         objectClass: top
|         objectClass: dcObject
|         objectClass: organization
|         o: nodomain
|         dc: nodomain
|     dn: cn=admin,dc=nodomain
|         objectClass: simpleSecurityObject
|         objectClass: organizationalRole
|         cn: admin
|_        description: LDAP administrator
| ldap-rootdse: 
| LDAP Results
|   <ROOT>
|       namingContexts: dc=nodomain
|       supportedControl: 2.16.840.1.113730.3.4.18
|       supportedControl: 2.16.840.1.113730.3.4.2
|       supportedControl: 1.3.6.1.4.1.4203.1.10.1
|       supportedControl: 1.3.6.1.1.22
|       supportedControl: 1.2.840.113556.1.4.319
|       supportedControl: 1.2.826.0.1.3344810.2.3
|       supportedControl: 1.3.6.1.1.13.2
|       supportedControl: 1.3.6.1.1.13.1
|       supportedControl: 1.3.6.1.1.12
|       supportedExtension: 1.3.6.1.4.1.4203.1.11.1
|       supportedExtension: 1.3.6.1.4.1.4203.1.11.3
|       supportedExtension: 1.3.6.1.1.8
|       supportedLDAPVersion: 3
|       supportedSASLMechanisms: DIGEST-MD5
|       supportedSASLMechanisms: NTLM
|       supportedSASLMechanisms: CRAM-MD5
|_      subschemaSubentry: cn=Subschema

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 09:57:07 2021 -- 1 IP address (1 host up) scanned in 22.98 seconds

```
