```bash
enum4linux -a -M -l -d 192.168.129.136 2>&1
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/enum4linux.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp389/enum4linux.txt):

```
Starting enum4linux v0.8.9 ( http://labs.portcullis.co.uk/application/enum4linux/ ) on Sun Nov  7 09:56:44 2021

 ==========================
|    Target Information    |
 ==========================
Target ........... 192.168.129.136
RID Range ........ 500-550,1000-1050
Username ......... ''
Password ......... ''
Known Usernames .. administrator, guest, krbtgt, domain admins, root, bin, none


 =======================================================
|    Enumerating Workgroup/Domain on 192.168.129.136    |
 =======================================================
[E] Can't find workgroup/domain


 ===============================================
|    Nbtstat Information for 192.168.129.136    |
 ===============================================
Use of uninitialized value $global_workgroup in concatenation (.) or string at ./enum4linux.pl line 437.
Looking up status of 192.168.129.136
No reply from 192.168.129.136

 ========================================
|    Session Check on 192.168.129.136    |
 ========================================
[E] Server doesn't allow session using username '', password ''.  Aborting remainder of tests.


```
