```bash
nmap -vv --reason -Pn -T4 -sU -sV -p 162 --script="banner,(snmp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/udp_162_snmp-nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/xml/udp_162_snmp_nmap.xml" 192.168.129.136
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/udp_162_snmp-nmap.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/udp_162_snmp-nmap.txt):

```
# Nmap 7.92 scan initiated Sun Nov  7 10:00:01 2021 as: nmap -vv --reason -Pn -T4 -sU -sV -p 162 "--script=banner,(snmp* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/udp_162_snmp-nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/udp162/xml/udp_162_snmp_nmap.xml 192.168.129.136
Nmap scan report for 192.168.129.136
Host is up, received user-set.
Scanned at 2021-11-07 10:00:02 EST for 59s

PORT    STATE SERVICE REASON       VERSION
162/udp open  snmp    udp-response net-snmp; net-snmp SNMPv3 server
| snmp-info: 
|   enterprise: net-snmp
|   engineIDFormat: unknown
|   engineIDData: d5b56263edcc575f00000000
|   snmpEngineBoots: 12
|_  snmpEngineTime: 13m10s

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 10:01:01 2021 -- 1 IP address (1 host up) scanned in 59.27 seconds

```
