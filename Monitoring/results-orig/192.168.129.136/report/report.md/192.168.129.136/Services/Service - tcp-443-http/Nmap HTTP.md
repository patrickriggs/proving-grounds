```bash
nmap -vv --reason -Pn -T4 -sV -p 443 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/xml/tcp_443_https_nmap.xml" 192.168.129.136
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_nmap.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_nmap.txt):

```
# Nmap 7.92 scan initiated Sun Nov  7 09:56:44 2021 as: nmap -vv --reason -Pn -T4 -sV -p 443 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/tcp_443_https_nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp443/xml/tcp_443_https_nmap.xml 192.168.129.136
Nmap scan report for 192.168.129.136
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-07 09:56:45 EST for 101s

PORT    STATE SERVICE  REASON         VERSION
443/tcp open  ssl/http syn-ack ttl 63 Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1
|     /nagiosxi/
|       php: 1
|     /nagiosxi/about/
|       Other: 1
|     /nagiosxi/images/
|       ico: 1; png: 4
|     /nagiosxi/includes/css/
|       css: 2
|     /nagiosxi/includes/js/
|       js: 2
|   Longest directory structure:
|     Depth: 3
|     Dir: /nagiosxi/includes/css/
|   Total files found (by extension):
|_    Other: 2; css: 2; ico: 1; js: 2; php: 1; png: 4
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
| http-vhosts: 
| cvs : 400
|_127 names had status 200
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php
|_ssl-date: TLS randomness does not represent time
| ssl-cert: Subject: commonName=192.168.1.6/organizationName=Nagios Enterprises/stateOrProvinceName=Minnesota/countryName=US/localityName=St. Paul/organizationalUnitName=Development
| Issuer: commonName=192.168.1.6/organizationName=Nagios Enterprises/stateOrProvinceName=Minnesota/countryName=US/localityName=St. Paul/organizationalUnitName=Development
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2020-09-08T18:28:08
| Not valid after:  2030-09-06T18:28:08
| MD5:   20f0 951f 8eff 1b69 ef3f 1b1e fb4c 361f
| SHA-1: cc40 0ad7 60cf 4959 1c92 d9ab 0f06 106c 18f6 6661
| -----BEGIN CERTIFICATE-----
| MIIDxTCCAq2gAwIBAgIBADANBgkqhkiG9w0BAQUFADB9MQswCQYDVQQGEwJVUzES
| MBAGA1UECAwJTWlubmVzb3RhMREwDwYDVQQHDAhTdC4gUGF1bDEbMBkGA1UECgwS
| TmFnaW9zIEVudGVycHJpc2VzMRQwEgYDVQQLDAtEZXZlbG9wbWVudDEUMBIGA1UE
| AwwLMTkyLjE2OC4xLjYwHhcNMjAwOTA4MTgyODA4WhcNMzAwOTA2MTgyODA4WjB9
| MQswCQYDVQQGEwJVUzESMBAGA1UECAwJTWlubmVzb3RhMREwDwYDVQQHDAhTdC4g
| UGF1bDEbMBkGA1UECgwSTmFnaW9zIEVudGVycHJpc2VzMRQwEgYDVQQLDAtEZXZl
| bG9wbWVudDEUMBIGA1UEAwwLMTkyLjE2OC4xLjYwggEiMA0GCSqGSIb3DQEBAQUA
| A4IBDwAwggEKAoIBAQCe4uFtqzOvsxrF7Krjw2Pz0x+2cX/9Kfw2jMhIbR0rb5Bl
| BiYb8ifgtbB05ZL2EqfE8e/I5EwVp/dtHUds4bJSv2FfEE4xzXU0SRw0LK4FQ6u1
| ZBB2HqTGhxCN0/rmLhf0/IriWAS6l3NOR58pJW/syaqKL4OSOvG248MndIKzwNBH
| 8vVGSgEKRD0qFxbqS3pCQTsejbCimqBSqAsBJMwBcOpQnfBip8EjcTWqD8mpfmMS
| 4tHhn8k2/7UMGWbSl1erpiZKL/1SQ/V2Z2mJBF+85x4J+Rz2ealAbVt1W+G1Cy6D
| vvsK9L+RLokdPHgrzSuZGNKrJxg3nkHKwRFkZbExAgMBAAGjUDBOMB0GA1UdDgQW
| BBRVunDEJGH/2XNnyJVQYllWcHHjFjAfBgNVHSMEGDAWgBRVunDEJGH/2XNnyJVQ
| YllWcHHjFjAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQArlT8PTnzT
| dz6wmMzY9/vnBMkRnvH7vuB1MfRlnTyDy4QcTpzDBgdjkvy6MYMxsQz3TTJ+OrOn
| zPdp1NzEFGDDJQUhE22F1kzpJX8XedlHV5YRhdDKokwh2kKcyEsW6obOlC9przI5
| MpJvndKTj69peQAxrWImjD2o70WMKcoOIlbNnbPmmsKiR6jtL6G0+3ic7jPgZRRb
| WmPLzYh7GWMik7R0DWkng2x2Hq1YKNWmiGtMv3fC/w5PRpNT+/VV0NfOOJu36VB/
| rilrUGlO5q0HSx2lf1QoxYDnkQZ8/nzfAzjCYj5M4WuGKzGmkB9GPDC/REfHdu8m
| sMSOoeFVpu/b
|_-----END CERTIFICATE-----
|_http-litespeed-sourcecode-download: Request with null byte did not work. This web server might not be vulnerable
| http-headers: 
|   Date: Sun, 07 Nov 2021 14:57:04 GMT
|   Server: Apache/2.4.18 (Ubuntu)
|   Connection: close
|   Content-Type: text/html; charset=UTF-8
|   
|_  (Request type: HEAD)
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-rfi-spider: ERROR: Script execution failed (use -d to debug)
| http-auth-finder: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.129.136
|   url                                                                                      method
|   https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1  FORM
|   https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1  FORM
|_  https://192.168.129.136:443/nagiosxi/login.php                                           FORM
| ssl-enum-ciphers: 
|   TLSv1.0: 
|     ciphers: 
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - F
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - F
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - F
|     compressors: 
|       NULL
|     cipher preference: client
|     warnings: 
|       Insecure certificate signature (SHA1), score capped at F
|   TLSv1.1: 
|     ciphers: 
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - F
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - F
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - F
|     compressors: 
|       NULL
|     cipher preference: client
|     warnings: 
|       Insecure certificate signature (SHA1), score capped at F
|   TLSv1.2: 
|     ciphers: 
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 (dh 2048) - F
|       TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 (dh 2048) - F
|       TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA (dh 2048) - F
|       TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA (dh 2048) - F
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 (secp256r1) - F
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (secp256r1) - F
|       TLS_RSA_WITH_AES_128_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_AES_128_CBC_SHA256 (rsa 2048) - F
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - F
|       TLS_RSA_WITH_AES_256_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_AES_256_CBC_SHA256 (rsa 2048) - F
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - F
|       TLS_RSA_WITH_CAMELLIA_128_CBC_SHA (rsa 2048) - F
|       TLS_RSA_WITH_CAMELLIA_256_CBC_SHA (rsa 2048) - F
|     compressors: 
|       NULL
|     cipher preference: client
|     warnings: 
|       Insecure certificate signature (SHA1), score capped at F
|_  least strength: F
|_http-title: Nagios XI
|_http-malware-host: Host appears to be clean
|_http-vuln-cve2017-1001000: ERROR: Script execution failed (use -d to debug)
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-mobileversion-checker: No mobile version detected.
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.129.136
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/dashboards.js?1555548979
|     Line number: 4
|     Comment: 
|         <!--<input type='submit' class='submitbutton' name='cancelButton' value='"+_('Cancel')+"' id='cancelCloneDashboardButton'>-->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 134
|     Comment: 
|         /* Set colors for tables */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 433
|     Comment: 
|         <!-- end footer div -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 500
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-cube-grid">
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 209
|     Comment: 
|         <!--- HEADER START -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 708
|     Comment: 
|         /* Host/Service details page Performance Graphs tab */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 268
|     Comment: 
|         <!-- CHILD FOOTER END -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 158
|     Comment: 
|         <!-- clipboard plugin -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 270
|     Comment: 
|         <!--page-->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 946
|     Comment: 
|         /* Screen < 1100px */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 151
|     Comment: 
|         <!-- jquery autocomplete -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 41
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-double-bounce">
|          *      <div class="sk-double-bounce1"></div>
|          *      <div class="sk-double-bounce2"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 2
|     Comment: 
|         <!-- <!DOCTYPE html> -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/bootstrap.3.min.css
|     Line number: 1
|     Comment: 
|         /*!
|          * Bootstrap v3.3.4 (http://getbootstrap.com)
|          * Copyright 2011-2015 Twitter, Inc.
|          * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 184
|     Comment: 
|         <!-- D3 Graphing Library -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/bootstrap.3.min.css
|     Line number: 5
|     Comment: 
|         /*! normalize.css v3.0.2 | MIT License | git.io/normalize */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 189
|     Comment: 
|         <!-- jScrollPane -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 904
|     Comment: 
|         /* For Overlays */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 1040
|     Comment: 
|         /* Updated install section */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 625
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-fading-circle">
|          *      <div class="sk-circle1 sk-circle"></div>
|          *      <div class="sk-circle2 sk-circle"></div>
|          *      <div class="sk-circle3 sk-circle"></div>
|          *      <div class="sk-circle4 sk-circle"></div>
|          *      <div class="sk-circle5 sk-circle"></div>
|          *      <div class="sk-circle6 sk-circle"></div>
|          *      <div class="sk-circle7 sk-circle"></div>
|          *      <div class="sk-circle8 sk-circle"></div>
|          *      <div class="sk-circle9 sk-circle"></div>
|          *      <div class="sk-circle10 sk-circle"></div>
|          *      <div class="sk-circle11 sk-circle"></div>
|          *      <div class="sk-circle12 sk-circle"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 177
|     Comment: 
|         /* Hack to make FF rotate in the right direction */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 416
|     Comment: 
|         <!--mainframe-->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 1
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-rotating-plane"></div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 33
|     Comment: 
|          // Language string for translations
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 2
|     Comment: 
|         <!-- Produced by Nagios XI. Copyright (c) 2008-2021 Nagios Enterprises, LLC (www.nagios.com). All Rights Reserved. -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 126
|     Comment: 
|          // Translation helper function
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 1072
|     Comment: 
|         /* Modern form layout (for deploy agent section) */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 330
|     Comment: 
|         /* FORMS */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 251
|     Comment: 
|         /* Fix perfdata in tabs on status pages */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 418
|     Comment: 
|         <!-- FOOTER START -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 618
|     Comment: 
|         /* log entries */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 836
|     Comment: 
|         /* New report layout */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 134
|     Comment: 
|         <!-- main jquery libraries -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 821
|     Comment: 
|         /* Special help section */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 1010
|     Comment: 
|         /* User Macro Component */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 250
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-chasing-dots">
|          *      <div class="sk-dot1"></div>
|          *      <div class="sk-dot2"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 722
|     Comment: 
|         /* Graph Explorer CSS for Dashlets */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 580
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-wordpress">
|          *      <span class="sk-inner-circle"></span>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 21
|     Comment: 
|         <!-- Global variables & Javascript translation text -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 435
|     Comment: 
|         <!-- FOOTER END -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 881
|     Comment: 
|         /* Screen < 1500px */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 78
|     Comment: 
|         /* jQuery-UI overwriting */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 3
|     Comment: 
|         <!-- Powered by the Nagios Synthesis Framework -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 361
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-circle">
|          *      <div class="sk-circle1 sk-circle"></div>
|          *      <div class="sk-circle2 sk-circle"></div>
|          *      <div class="sk-circle3 sk-circle"></div>
|          *      <div class="sk-circle4 sk-circle"></div>
|          *      <div class="sk-circle5 sk-circle"></div>
|          *      <div class="sk-circle6 sk-circle"></div>
|          *      <div class="sk-circle7 sk-circle"></div>
|          *      <div class="sk-circle8 sk-circle"></div>
|          *      <div class="sk-circle9 sk-circle"></div>
|          *      <div class="sk-circle10 sk-circle"></div>
|          *      <div class="sk-circle11 sk-circle"></div>
|          *      <div class="sk-circle12 sk-circle"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 872
|     Comment: 
|         /*font-weight: bold; font-size: 1.3rem;*/
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 88
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-wave">
|          *      <div class="sk-rect1"></div>
|          *      <div class="sk-rect2"></div>
|          *      <div class="sk-rect3"></div>
|          *      <div class="sk-rect4"></div>
|          *      <div class="sk-rect5"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 251
|     Comment: 
|         
|         
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 716
|     Comment: 
|         /* Gauge classes for Outboard Dashlets */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 265
|     Comment: 
|         <!-- CHILD FOOTER START -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 215
|     Comment: 
|         <!--- CHILD HEADER END -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 273
|     Comment: 
|         <!-- This page requires a web browser which supports frames. -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 203
|     Comment: 
|         <!--- CHILD HEADER START -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 332
|     Comment: 
|         /* defaults */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 517
|     Comment: 
|         /*
|            * Spinner positions
|            * 1 2 3
|            * 4 5 6
|            * 7 8 9
|            */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 802
|     Comment: 
|         /* Color overrides for fontawesome icons */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 480
|     Comment: 
|         /* STANDARD TABLE DEFAULTS */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 888
|     Comment: 
|         /* Screen < 1200px */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 333
|     Comment: 
|         /* Prevent first frame from flickering when animation starts */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 18
|     Comment: 
|         <!-- Adding Font-Awesome for all themes -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 148
|     Comment: 
|         <!-- spin kit -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 1026
|     Comment: 
|         /* warning/critical highcharts buttons */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/bootstrap.3.min.css
|     Line number: 5
|     Comment: 
|         /*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 346
|     Comment: 
|         
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 154
|     Comment: 
|         <!-- colorpicker -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 215
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-pulse"></div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 144
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-wandering-cubes">
|          *      <div class="sk-cube1"></div>
|          *      <div class="sk-cube2"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Line number: 256
|     Comment: 
|         
|         
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 395
|     Comment: 
|         /* SORTED TABLE STYLES */
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 179
|     Comment: 
|         <!-- Highcharts Graphing Library -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 162
|     Comment: 
|         <!-- XI JS Scripts -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/about/main.php?&amp;legal=
|     Line number: 175
|     Comment: 
|         <!-- XI CSS -->
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 311
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-three-bounce">
|          *      <div class="sk-bounce1"></div>
|          *      <div class="sk-bounce2"></div>
|          *      <div class="sk-bounce3"></div>
|          *    </div>
|          *
|          */
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/css/base.css
|     Line number: 774
|     Comment: 
|_        /* SLA Dashlet */
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
| http-php-version: Logo query returned unknown hash a08cd93c8ad241022550b128bb494eff
|_Credits query returned unknown hash a08cd93c8ad241022550b128bb494eff
|_http-date: Sun, 07 Nov 2021 14:57:03 GMT; 0s from local time.
|_http-chrono: Request times for /; avg: 583.17ms; min: 552.98ms; max: 609.47ms
| http-security-headers: 
|   Strict_Transport_Security: 
|_    HSTS not configured in HTTPS Server
|_http-config-backup: ERROR: Script execution failed (use -d to debug)
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34
| http-fileupload-exploiter: 
|   
|     Couldn't find a file-type field.
|   
|_    Couldn't find a file-type field.
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
| http-grep: 
|   (1) https://192.168.129.136:443/nagiosxi/about/?legal: 
|     (1) ip: 
|       + 192.168.129.136
|   (1) https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1: 
|     (1) email: 
|_      + sales@nagios.com
|_http-errors: Couldn't find any error pages.
| http-unsafe-output-escaping: 
|   Characters ['] reflected in parameter redirect at https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|_  Characters ['] reflected in parameter redirect at https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-feed: Couldn't find any feeds.
| http-csrf: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.129.136
|   Found the following possible CSRF vulnerabilities: 
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|     Form id: loginform
|     Form action: /nagiosxi/login.php
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|     Form id: loginform
|     Form action: /nagiosxi/login.php
|     
|     Path: https://192.168.129.136:443/nagiosxi/login.php
|     Form id: loginform
|     Form action: /nagiosxi/login.php
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/dashboards.js?1555548979
|     Form id: adddashboard_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/dashboards.js?1555548979
|     Form id: editdashboard_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/dashboards.js?1555548979
|     Form id: deletedashboard_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/dashboards.js?1555548979
|     Form id: clonedashboard_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/views.js?1555548979
|     Form id: addview_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/views.js?1555548979
|     Form id: editview_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: https://192.168.129.136:443/nagiosxi/includes/js/views.js?1555548979
|     Form id: addview_form
|_    Form action: "+ajax_helper_url+"

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 09:58:26 2021 -- 1 IP address (1 host up) scanned in 101.49 seconds

```
