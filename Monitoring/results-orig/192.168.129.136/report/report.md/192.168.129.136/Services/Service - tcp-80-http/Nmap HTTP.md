```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.129.136
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.92 scan initiated Sun Nov  7 09:56:44 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.129.136
Nmap scan report for 192.168.129.136
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-07 09:56:45 EST for 48s

Bug in http-security-headers: no string output.
PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.18 ((Ubuntu))
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.129.136
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 1026
|     Comment: 
|         /* warning/critical highcharts buttons */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 480
|     Comment: 
|         /* STANDARD TABLE DEFAULTS */
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 256
|     Comment: 
|         
|         
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 177
|     Comment: 
|         /* Hack to make FF rotate in the right direction */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 251
|     Comment: 
|         /* Fix perfdata in tabs on status pages */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 580
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-wordpress">
|          *      <span class="sk-inner-circle"></span>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 716
|     Comment: 
|         /* Gauge classes for Outboard Dashlets */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 162
|     Comment: 
|         <!-- XI JS Scripts -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 722
|     Comment: 
|         /* Graph Explorer CSS for Dashlets */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 802
|     Comment: 
|         /* Color overrides for fontawesome icons */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 21
|     Comment: 
|         <!-- Global variables & Javascript translation text -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 332
|     Comment: 
|         /* defaults */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 126
|     Comment: 
|          // Translation helper function
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 78
|     Comment: 
|         /* jQuery-UI overwriting */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 361
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-circle">
|          *      <div class="sk-circle1 sk-circle"></div>
|          *      <div class="sk-circle2 sk-circle"></div>
|          *      <div class="sk-circle3 sk-circle"></div>
|          *      <div class="sk-circle4 sk-circle"></div>
|          *      <div class="sk-circle5 sk-circle"></div>
|          *      <div class="sk-circle6 sk-circle"></div>
|          *      <div class="sk-circle7 sk-circle"></div>
|          *      <div class="sk-circle8 sk-circle"></div>
|          *      <div class="sk-circle9 sk-circle"></div>
|          *      <div class="sk-circle10 sk-circle"></div>
|          *      <div class="sk-circle11 sk-circle"></div>
|          *      <div class="sk-circle12 sk-circle"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 151
|     Comment: 
|         <!-- jquery autocomplete -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 618
|     Comment: 
|         /* log entries */
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 416
|     Comment: 
|         <!--mainframe-->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/font-awesome.min.css?1555548979
|     Line number: 1
|     Comment: 
|         /*!
|          *  Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
|          *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 203
|     Comment: 
|         <!--- CHILD HEADER START -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 435
|     Comment: 
|         <!-- FOOTER END -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 418
|     Comment: 
|         <!-- FOOTER START -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 134
|     Comment: 
|         /* Set colors for tables */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 154
|     Comment: 
|         <!-- colorpicker -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 179
|     Comment: 
|         <!-- Highcharts Graphing Library -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 175
|     Comment: 
|         <!-- XI CSS -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 158
|     Comment: 
|         <!-- clipboard plugin -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 330
|     Comment: 
|         /* FORMS */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/bootstrap.3.min.css
|     Line number: 1
|     Comment: 
|         /*!
|          * Bootstrap v3.3.4 (http://getbootstrap.com)
|          * Copyright 2011-2015 Twitter, Inc.
|          * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 41
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-double-bounce">
|          *      <div class="sk-double-bounce1"></div>
|          *      <div class="sk-double-bounce2"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 18
|     Comment: 
|         <!-- Adding Font-Awesome for all themes -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 881
|     Comment: 
|         /* Screen < 1500px */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 311
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-three-bounce">
|          *      <div class="sk-bounce1"></div>
|          *      <div class="sk-bounce2"></div>
|          *      <div class="sk-bounce3"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 243
|     Comment: 
|         <!--page-->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 836
|     Comment: 
|         /* New report layout */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 33
|     Comment: 
|          // Language string for translations
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 395
|     Comment: 
|         /* SORTED TABLE STYLES */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 184
|     Comment: 
|         <!-- D3 Graphing Library -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 625
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-fading-circle">
|          *      <div class="sk-circle1 sk-circle"></div>
|          *      <div class="sk-circle2 sk-circle"></div>
|          *      <div class="sk-circle3 sk-circle"></div>
|          *      <div class="sk-circle4 sk-circle"></div>
|          *      <div class="sk-circle5 sk-circle"></div>
|          *      <div class="sk-circle6 sk-circle"></div>
|          *      <div class="sk-circle7 sk-circle"></div>
|          *      <div class="sk-circle8 sk-circle"></div>
|          *      <div class="sk-circle9 sk-circle"></div>
|          *      <div class="sk-circle10 sk-circle"></div>
|          *      <div class="sk-circle11 sk-circle"></div>
|          *      <div class="sk-circle12 sk-circle"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 3
|     Comment: 
|         <!-- Powered by the Nagios Synthesis Framework -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 88
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-wave">
|          *      <div class="sk-rect1"></div>
|          *      <div class="sk-rect2"></div>
|          *      <div class="sk-rect3"></div>
|          *      <div class="sk-rect4"></div>
|          *      <div class="sk-rect5"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 517
|     Comment: 
|         /*
|            * Spinner positions
|            * 1 2 3
|            * 4 5 6
|            * 7 8 9
|            */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 215
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-pulse"></div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 500
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-cube-grid">
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *      <div class="sk-cube"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 346
|     Comment: 
|         
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 2
|     Comment: 
|         <!-- <!DOCTYPE html> -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 904
|     Comment: 
|         /* For Overlays */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 946
|     Comment: 
|         /* Screen < 1100px */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 821
|     Comment: 
|         /* Special help section */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 333
|     Comment: 
|         /* Prevent first frame from flickering when animation starts */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 250
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-chasing-dots">
|          *      <div class="sk-dot1"></div>
|          *      <div class="sk-dot2"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 144
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-wandering-cubes">
|          *      <div class="sk-cube1"></div>
|          *      <div class="sk-cube2"></div>
|          *    </div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 433
|     Comment: 
|         <!-- end footer div -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/spinkit.css?1555548979
|     Line number: 1
|     Comment: 
|         /*
|          *  Usage:
|          *
|          *    <div class="sk-spinner sk-spinner-rotating-plane"></div>
|          *
|          */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 241
|     Comment: 
|         <!-- CHILD FOOTER END -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 238
|     Comment: 
|         <!-- CHILD FOOTER START -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 2
|     Comment: 
|         <!-- Produced by Nagios XI. Copyright (c) 2008-2021 Nagios Enterprises, LLC (www.nagios.com). All Rights Reserved. -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/bootstrap.3.min.css
|     Line number: 5
|     Comment: 
|         /*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 1010
|     Comment: 
|         /* User Macro Component */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 148
|     Comment: 
|         <!-- spin kit -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 134
|     Comment: 
|         <!-- main jquery libraries -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/bootstrap.3.min.css
|     Line number: 5
|     Comment: 
|         /*! normalize.css v3.0.2 | MIT License | git.io/normalize */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 708
|     Comment: 
|         /* Host/Service details page Performance Graphs tab */
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 215
|     Comment: 
|         <!--- CHILD HEADER END -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 872
|     Comment: 
|         /*font-weight: bold; font-size: 1.3rem;*/
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 888
|     Comment: 
|         /* Screen < 1200px */
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 189
|     Comment: 
|         <!-- jScrollPane -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/about/main.php?legal
|     Line number: 246
|     Comment: 
|         <!-- This page requires a web browser which supports frames. -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 1072
|     Comment: 
|         /* Modern form layout (for deploy agent section) */
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 1040
|     Comment: 
|         /* Updated install section */
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 251
|     Comment: 
|         
|         
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Line number: 209
|     Comment: 
|         <!--- HEADER START -->
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/css/base.css?1555548979
|     Line number: 774
|     Comment: 
|_        /* SLA Dashlet */
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1
|     /nagiosxi/
|       php: 1
|     /nagiosxi/about/
|       Other: 1; php: 1
|     /nagiosxi/images/
|       ico: 1; png: 4
|     /nagiosxi/includes/css/
|       css: 3
|     /nagiosxi/includes/js/
|       js: 3
|   Longest directory structure:
|     Depth: 3
|     Dir: /nagiosxi/includes/css/
|   Total files found (by extension):
|_    Other: 2; css: 3; ico: 1; js: 3; php: 2; png: 4
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-malware-host: Host appears to be clean
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-php-version: Logo query returned unknown hash a08cd93c8ad241022550b128bb494eff
|_Credits query returned unknown hash a08cd93c8ad241022550b128bb494eff
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
| http-headers: 
|   Date: Sun, 07 Nov 2021 14:56:58 GMT
|   Server: Apache/2.4.18 (Ubuntu)
|   Connection: close
|   Content-Type: text/html; charset=UTF-8
|   
|_  (Request type: HEAD)
| http-csrf: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.129.136
|   Found the following possible CSRF vulnerabilities: 
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|     Form id: loginform
|     Form action: /nagiosxi/login.php
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|     Form id: loginform
|     Form action: /nagiosxi/login.php
|     
|     Path: http://192.168.129.136:80/nagiosxi/login.php
|     Form id: loginform
|     Form action: /nagiosxi/login.php
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/js/views.js?1555548979
|     Form id: addview_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/js/views.js?1555548979
|     Form id: editview_form
|     Form action: "+ajax_helper_url+"
|     
|     Path: http://192.168.129.136:80/nagiosxi/includes/js/views.js?1555548979
|     Form id: addview_form
|_    Form action: "+ajax_helper_url+"
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
|_http-chrono: Request times for /; avg: 362.31ms; min: 296.02ms; max: 399.92ms
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-vuln-cve2017-1001000: ERROR: Script execution failed (use -d to debug)
|_http-title: Nagios XI
|_http-feed: Couldn't find any feeds.
|_http-errors: Couldn't find any error pages.
| http-auth-finder: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.129.136
|   url                                                                                    method
|   http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1  FORM
|   http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1  FORM
|_  http://192.168.129.136:80/nagiosxi/login.php                                           FORM
|_http-mobileversion-checker: No mobile version detected.
|_http-date: Sun, 07 Nov 2021 14:56:53 GMT; -1s from local time.
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-favicon: Unknown favicon MD5: 8E1494DD4BFF0FC523A2E2A15ED59D84
|_http-litespeed-sourcecode-download: Request with null byte did not work. This web server might not be vulnerable
|_http-config-backup: ERROR: Script execution failed (use -d to debug)
| http-fileupload-exploiter: 
|   
|_    Couldn't find a file-type field.
| http-vhosts: 
|_128 names had status 200
| http-unsafe-output-escaping: 
|   Characters ['] reflected in parameter redirect at http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
|_  Characters ['] reflected in parameter redirect at http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1
| http-grep: 
|   (1) http://192.168.129.136:80/nagiosxi/about/: 
|     (1) ip: 
|       + 192.168.129.136
|   (1) http://192.168.129.136:80/nagiosxi/login.php?redirect=/nagiosxi/index.php%3F&noauth=1: 
|     (1) email: 
|_      + sales@nagios.com
|_http-rfi-spider: ERROR: Script execution failed (use -d to debug)
|_http-referer-checker: Couldn't find any cross-domain scripts.

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Nov  7 09:57:33 2021 -- 1 IP address (1 host up) scanned in 48.36 seconds

```
