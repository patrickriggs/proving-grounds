```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.129.136:80 2>&1
```

[/home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_whatweb.txt](file:///home/tac0shell/hacking/pg/Monitoring/results/192.168.129.136/scans/tcp80/tcp_80_http_whatweb.txt):

```
WhatWeb report for http://192.168.129.136:80
Status    : 200 OK
Title     : Nagios XI
IP        : 192.168.129.136
Country   : RESERVED, ZZ

Summary   : Apache[2.4.18], JQuery[3.3.1], HTML5, Script[text/javascript], HTTPServer[Ubuntu Linux][Apache/2.4.18 (Ubuntu)]

Detected Plugins:
[ Apache ]
	The Apache HTTP Server Project is an effort to develop and
	maintain an open-source HTTP server for modern operating
	systems including UNIX and Windows NT. The goal of this
	project is to provide a secure, efficient and extensible
	server that provides HTTP services in sync with the current
	HTTP standards.

	Version      : 2.4.18 (from HTTP Server Header)
	Google Dorks: (3)
	Website     : http://httpd.apache.org/

[ HTML5 ]
	HTML version 5, detected by the doctype declaration


[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Ubuntu Linux
	String       : Apache/2.4.18 (Ubuntu) (from server string)

[ JQuery ]
	A fast, concise, JavaScript that simplifies how to traverse
	HTML documents, handle events, perform animations, and add
	AJAX.

	Version      : 3.3.1
	Website     : http://jquery.com/

[ Script ]
	This plugin detects instances of script HTML elements and
	returns the script language/type.

	String       : text/javascript

HTTP Headers:
	HTTP/1.1 200 OK
	Date: Sun, 07 Nov 2021 14:56:51 GMT
	Server: Apache/2.4.18 (Ubuntu)
	Vary: Accept-Encoding
	Content-Encoding: gzip
	Content-Length: 1120
	Connection: close
	Content-Type: text/html; charset=UTF-8



```
