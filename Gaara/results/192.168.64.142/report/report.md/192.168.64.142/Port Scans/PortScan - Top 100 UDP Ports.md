```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/xml/_top_100_udp_nmap.xml" 192.168.64.142
```

[/home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.92 scan initiated Fri Oct 29 00:09:41 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/xml/_top_100_udp_nmap.xml 192.168.64.142
Warning: 192.168.64.142 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.64.142 from 100 to 200 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.64.142 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.64.142 from 400 to 800 due to 11 out of 11 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -53990 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -54988 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -54988 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -55630 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -55630 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -55656 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -55656 microseconds.  Ignoring time.
Nmap scan report for 192.168.64.142
Host is up, received user-set (0.10s latency).
Scanned at 2021-10-29 00:09:41 EDT for 186s
Not shown: 90 closed udp ports (port-unreach)
PORT      STATE         SERVICE       REASON      VERSION
69/udp    open|filtered tftp          no-response
123/udp   open|filtered ntp           no-response
177/udp   open|filtered xdmcp         no-response
497/udp   open|filtered retrospect    no-response
1025/udp  open|filtered blackjack     no-response
1027/udp  open|filtered unknown       no-response
2048/udp  open|filtered dls-monitor   no-response
2223/udp  open|filtered rockwell-csp2 no-response
5000/udp  open|filtered upnp          no-response
32769/udp open|filtered filenet-rpc   no-response
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.92%E=4%D=10/29%OT=%CT=%CU=7%PV=Y%DS=2%DC=T%G=N%TM=617B74BF%P=x86_64-pc-linux-gnu)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 135/udp)
HOP RTT       ADDRESS
1   99.72 ms  192.168.49.1
2   100.86 ms 192.168.64.142

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Oct 29 00:12:47 2021 -- 1 IP address (1 host up) scanned in 185.68 seconds

```
