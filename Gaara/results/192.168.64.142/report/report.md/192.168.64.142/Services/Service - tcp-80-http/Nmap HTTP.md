```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.64.142
```

[/home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.92 scan initiated Fri Oct 29 00:10:00 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Gaara/results/192.168.64.142/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.64.142
Nmap scan report for 192.168.64.142
Host is up, received user-set (0.10s latency).
Scanned at 2021-10-29 00:10:00 EDT for 21s

Bug in http-security-headers: no string output.
PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.38 ((Debian))
|_http-csrf: Couldn't find any CSRF vulnerabilities.
| http-vhosts: 
|_128 names had status 200
|_http-mobileversion-checker: No mobile version detected.
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-server-header: Apache/2.4.38 (Debian)
|_http-litespeed-sourcecode-download: Request with null byte did not work. This web server might not be vulnerable
|_http-malware-host: Host appears to be clean
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1; jpg: 1
|   Longest directory structure:
|     Depth: 0
|     Dir: /
|   Total files found (by extension):
|_    Other: 1; jpg: 1
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-errors: Couldn't find any error pages.
| http-php-version: Logo query returned unknown hash 071fc34ff24dee06ca02cde653727f1e
|_Credits query returned unknown hash 071fc34ff24dee06ca02cde653727f1e
|_http-fetch: Please enter the complete path of the directory to save data in.
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-date: Fri, 29 Oct 2021 04:10:07 GMT; 0s from local time.
| http-methods: 
|_  Supported Methods: OPTIONS HEAD GET POST
|_http-chrono: Request times for /; avg: 239.33ms; min: 215.52ms; max: 249.46ms
|_http-title: Gaara
| http-headers: 
|   Date: Fri, 29 Oct 2021 04:10:08 GMT
|   Server: Apache/2.4.38 (Debian)
|   Last-Modified: Tue, 30 Mar 2021 12:04:12 GMT
|   ETag: "89-5bebfcbe49cf9"
|   Accept-Ranges: bytes
|   Content-Length: 137
|   Vary: Accept-Encoding
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
|_http-config-backup: ERROR: Script execution failed (use -d to debug)
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-comments-displayer: Couldn't find any comments.
|_http-exif-spider: ERROR: Script execution failed (use -d to debug)
|_http-feed: Couldn't find any feeds.

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Oct 29 00:10:21 2021 -- 1 IP address (1 host up) scanned in 21.34 seconds

```
