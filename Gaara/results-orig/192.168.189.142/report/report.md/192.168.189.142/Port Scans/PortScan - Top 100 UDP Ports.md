```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/xml/_top_100_udp_nmap.xml" 192.168.189.142
```

[/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.91 scan initiated Thu Oct 28 14:55:47 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/xml/_top_100_udp_nmap.xml 192.168.189.142
Increasing send delay for 192.168.189.142 from 50 to 100 due to 11 out of 13 dropped probes since last increase.
Increasing send delay for 192.168.189.142 from 100 to 200 due to 11 out of 11 dropped probes since last increase.
Warning: 192.168.189.142 giving up on port because retransmission cap hit (6).
adjust_timeouts2: packet supposedly had rtt of -81068 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -81068 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -129021 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -129021 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -256883 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -256883 microseconds.  Ignoring time.
Nmap scan report for 192.168.189.142
Host is up, received user-set (0.033s latency).
Scanned at 2021-10-28 14:55:48 EDT for 330s

PORT      STATE         SERVICE         REASON              VERSION
7/udp     open|filtered echo            no-response
9/udp     open|filtered discard         no-response
17/udp    closed        qotd            port-unreach ttl 63
19/udp    closed        chargen         port-unreach ttl 63
49/udp    closed        tacacs          port-unreach ttl 63
53/udp    open|filtered domain          no-response
67/udp    closed        dhcps           port-unreach ttl 63
68/udp    open|filtered dhcpc           no-response
69/udp    closed        tftp            port-unreach ttl 63
80/udp    open|filtered http            no-response
88/udp    open|filtered kerberos-sec    no-response
111/udp   closed        rpcbind         port-unreach ttl 63
120/udp   closed        cfdptkt         port-unreach ttl 63
123/udp   open|filtered ntp             no-response
135/udp   closed        msrpc           port-unreach ttl 63
136/udp   open|filtered profile         no-response
137/udp   open|filtered netbios-ns      no-response
138/udp   open|filtered netbios-dgm     no-response
139/udp   closed        netbios-ssn     port-unreach ttl 63
158/udp   open|filtered pcmail-srv      no-response
161/udp   closed        snmp            port-unreach ttl 63
162/udp   closed        snmptrap        port-unreach ttl 63
177/udp   open|filtered xdmcp           no-response
427/udp   closed        svrloc          port-unreach ttl 63
443/udp   open|filtered https           no-response
445/udp   open|filtered microsoft-ds    no-response
497/udp   open|filtered retrospect      no-response
500/udp   closed        isakmp          port-unreach ttl 63
514/udp   closed        syslog          port-unreach ttl 63
515/udp   open|filtered printer         no-response
518/udp   closed        ntalk           port-unreach ttl 63
520/udp   open|filtered route           no-response
593/udp   closed        http-rpc-epmap  port-unreach ttl 63
623/udp   open|filtered asf-rmcp        no-response
626/udp   open|filtered serialnumberd   no-response
631/udp   closed        ipp             port-unreach ttl 63
996/udp   closed        vsinet          port-unreach ttl 63
997/udp   open|filtered maitrd          no-response
998/udp   closed        puparp          port-unreach ttl 63
999/udp   closed        applix          port-unreach ttl 63
1022/udp  open|filtered exp2            no-response
1023/udp  open|filtered unknown         no-response
1025/udp  closed        blackjack       port-unreach ttl 63
1026/udp  open|filtered win-rpc         no-response
1027/udp  closed        unknown         port-unreach ttl 63
1028/udp  open|filtered ms-lsa          no-response
1029/udp  closed        solid-mux       port-unreach ttl 63
1030/udp  open|filtered iad1            no-response
1433/udp  open|filtered ms-sql-s        no-response
1434/udp  open|filtered ms-sql-m        no-response
1645/udp  open|filtered radius          no-response
1646/udp  open|filtered radacct         no-response
1701/udp  closed        L2TP            port-unreach ttl 63
1718/udp  open|filtered h225gatedisc    no-response
1719/udp  open|filtered h323gatestat    no-response
1812/udp  closed        radius          port-unreach ttl 63
1813/udp  closed        radacct         port-unreach ttl 63
1900/udp  closed        upnp            port-unreach ttl 63
2000/udp  closed        cisco-sccp      port-unreach ttl 63
2048/udp  closed        dls-monitor     port-unreach ttl 63
2049/udp  open|filtered nfs             no-response
2222/udp  closed        msantipiracy    port-unreach ttl 63
2223/udp  closed        rockwell-csp2   port-unreach ttl 63
3283/udp  open|filtered netassistant    no-response
3456/udp  closed        IISrpc-or-vat   port-unreach ttl 63
3703/udp  closed        adobeserver-3   port-unreach ttl 63
4444/udp  open|filtered krb524          no-response
4500/udp  closed        nat-t-ike       port-unreach ttl 63
5000/udp  closed        upnp            port-unreach ttl 63
5060/udp  open|filtered sip             no-response
5353/udp  closed        zeroconf        port-unreach ttl 63
5632/udp  closed        pcanywherestat  port-unreach ttl 63
9200/udp  open|filtered wap-wsp         no-response
10000/udp closed        ndmp            port-unreach ttl 63
17185/udp closed        wdbrpc          port-unreach ttl 63
20031/udp open|filtered bakbonenetvault no-response
30718/udp closed        unknown         port-unreach ttl 63
31337/udp open|filtered BackOrifice     no-response
|_backorifice-info: ERROR: Script execution failed (use -d to debug)
32768/udp open|filtered omad            no-response
32769/udp open|filtered filenet-rpc     no-response
32771/udp open|filtered sometimes-rpc6  no-response
32815/udp closed        unknown         port-unreach ttl 63
33281/udp closed        unknown         port-unreach ttl 63
49152/udp open|filtered unknown         no-response
49153/udp closed        unknown         port-unreach ttl 63
49154/udp closed        unknown         port-unreach ttl 63
49156/udp closed        unknown         port-unreach ttl 63
49181/udp closed        unknown         port-unreach ttl 63
49182/udp open|filtered unknown         no-response
49185/udp open|filtered unknown         no-response
49186/udp open|filtered unknown         no-response
49188/udp closed        unknown         port-unreach ttl 63
49190/udp open|filtered unknown         no-response
49191/udp open|filtered unknown         no-response
49192/udp closed        unknown         port-unreach ttl 63
49193/udp closed        unknown         port-unreach ttl 63
49194/udp open|filtered unknown         no-response
49200/udp closed        unknown         port-unreach ttl 63
49201/udp closed        unknown         port-unreach ttl 63
65024/udp closed        unknown         port-unreach ttl 63
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.91%E=4%D=10/28%OT=%CT=%CU=17%PV=Y%DS=2%DC=T%G=N%TM=617AF37E%P=x86_64-pc-linux-gnu)
SEQ(II=I)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 2223/udp)
HOP RTT      ADDRESS
1   25.08 ms 192.168.49.1
2   19.71 ms 192.168.189.142

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Oct 28 15:01:18 2021 -- 1 IP address (1 host up) scanned in 330.73 seconds

```
