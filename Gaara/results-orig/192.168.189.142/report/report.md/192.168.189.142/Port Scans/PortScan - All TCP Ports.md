```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN "/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_full_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/xml/_full_tcp_nmap.xml" 192.168.189.142
```

[/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_full_tcp_nmap.txt](file:///home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_full_tcp_nmap.txt):

```
# Nmap 7.91 scan initiated Thu Oct 28 14:55:47 2021 as: nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN /home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/_full_tcp_nmap.txt -oX /home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/xml/_full_tcp_nmap.xml 192.168.189.142
Warning: 192.168.189.142 giving up on port because retransmission cap hit (6).
adjust_timeouts2: packet supposedly had rtt of -235051 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -235051 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -752123 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -752123 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -502784 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -502784 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -668622 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -668622 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -232777 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -232777 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -780054 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -780054 microseconds.  Ignoring time.
Nmap scan report for 192.168.189.142
Host is up, received user-set (0.030s latency).
Scanned at 2021-10-28 14:55:48 EDT for 296s
Not shown: 65520 closed ports
Reason: 65520 resets
PORT      STATE    SERVICE       REASON         VERSION
22/tcp    open     ssh           syn-ack ttl 63 OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 3e:a3:6f:64:03:33:1e:76:f8:e4:98:fe:be:e9:8e:58 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDS8evJ7ywX5kz396YcIuR+rucTJ/OAK1SSpQoyx6Avj3v1/ZeRvikDEBZRZE4KMV4/+LraxOvCIb0rkU98B5WME6IReWvGTbF99x6wc2sDCG5haD5/OI6At8xrEQPV6FL8NqipouEeYXU5lp/aR7vsdJAs/748uo6Xu4xwUWKFit3RvCHAdhuNfXj5bpiWESerc6mjRm1dPIwIUjJb2zBKTMFiVxpl8R3BXRLV7ISaKQwEo5zp8OzfxDF0YQ5WxMSaKu6fsBh/XDHr+m2A7TLPfIJPS2i2Y8EPxymUahuhSq63nNSaaWNdSZwpbL0qCBPdn1jtTjh26fGbmPeFVdw1
|   256 6c:0e:b5:00:e7:42:44:48:65:ef:fe:d7:7c:e6:64:d5 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPFPC21nXnF1t6XmiDOwcXTza1K6jFzzUhlI+zb878mxsPin/9KvLlW9up9ECWVVTKbiIieN8cD0rF7wb3EjkHA=
|   256 b7:51:f2:f9:85:57:66:a8:65:54:2e:05:f9:40:d2:f4 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBprcu3jXo9TbgN5tBKvrojw4OFUkQIH+dITgacg3BLV
80/tcp    open     http          syn-ack ttl 63 Apache httpd 2.4.38 ((Debian))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Gaara
335/tcp   filtered unknown       no-response
1012/tcp  filtered unknown       no-response
1268/tcp  filtered propel-msgsys no-response
7376/tcp  filtered unknown       no-response
19567/tcp filtered unknown       no-response
27997/tcp filtered unknown       no-response
37998/tcp filtered unknown       no-response
55503/tcp filtered unknown       no-response
56138/tcp filtered unknown       no-response
57788/tcp filtered unknown       no-response
60321/tcp filtered unknown       no-response
60720/tcp filtered unknown       no-response
62439/tcp filtered unknown       no-response
Aggressive OS guesses: Linux 2.6.32 (91%), Linux 2.6.32 or 3.10 (91%), Linux 2.6.39 (91%), Linux 3.10 - 3.12 (91%), Linux 4.4 (91%), Synology DiskStation Manager 5.1 (90%), Linux 2.6.35 (90%), Linux 4.9 (90%), Linux 3.4 (90%), Linux 3.5 (90%)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.91%E=4%D=10/28%OT=22%CT=1%CU=34435%PV=Y%DS=2%DC=T%G=Y%TM=617AF3
OS:5C%P=x86_64-pc-linux-gnu)SEQ(SP=FF%GCD=1%ISR=10F%TI=Z%II=I%TS=A)SEQ(SP=F
OS:F%GCD=1%ISR=10F%TI=Z%TS=A)OPS(O1=M54EST11NW7%O2=M54EST11NW7%O3=M54ENNT11
OS:NW7%O4=M54EST11NW7%O5=M54EST11NW7%O6=M54EST11)WIN(W1=FE88%W2=FE88%W3=FE8
OS:8%W4=FE88%W5=FE88%W6=FE88)ECN(R=Y%DF=Y%T=40%W=FAF0%O=M54ENNSNW7%CC=Y%Q=)
OS:T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=N)T5(R=Y%DF=Y%
OS:T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=N)T7(R=N)U1(R=Y%DF=N%T=40%IPL=164
OS:%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%CD=S)

Uptime guess: 44.509 days (since Tue Sep 14 02:48:27 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=255 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 199/tcp)
HOP RTT      ADDRESS
1   37.91 ms 192.168.49.1
2   38.96 ms 192.168.189.142

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Oct 28 15:00:44 2021 -- 1 IP address (1 host up) scanned in 297.26 seconds

```
