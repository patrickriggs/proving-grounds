```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.189.142
```

[/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.91 scan initiated Thu Oct 28 14:56:11 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.189.142
Nmap scan report for 192.168.189.142
Host is up, received user-set (0.093s latency).
Scanned at 2021-10-28 14:56:12 EDT for 59s

PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.38 ((Debian))
|_http-aspnet-debug: ERROR: Script execution failed (use -d to debug)
|_http-chrono: Request times for /; avg: 3982.88ms; min: 50.15ms; max: 13872.05ms
|_http-comments-displayer: Couldn't find any comments.
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-date: Thu, 28 Oct 2021 18:56:46 GMT; 0s from local time.
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-errors: Couldn't find any error pages.
|_http-exif-spider: ERROR: Script execution failed (use -d to debug)
|_http-feed: Couldn't find any feeds.
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-headers: 
|   Date: Thu, 28 Oct 2021 18:56:46 GMT
|   Server: Apache/2.4.38 (Debian)
|   Last-Modified: Tue, 30 Mar 2021 12:04:12 GMT
|   ETag: "89-5bebfcbe49cf9"
|   Accept-Ranges: bytes
|   Content-Length: 137
|   Vary: Accept-Encoding
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-malware-host: Host appears to be clean
| http-methods: 
|_  Supported Methods: HEAD POST OPTIONS
|_http-mobileversion-checker: No mobile version detected.
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-security-headers: 
|_http-server-header: Apache/2.4.38 (Debian)
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1; jpg: 1
|   Longest directory structure:
|     Depth: 0
|     Dir: /
|   Total files found (by extension):
|_    Other: 1; jpg: 1
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-title: Gaara
|_http-traceroute: ERROR: Script execution failed (use -d to debug)
| http-useragent-tester: 
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|   Change in Status Code: 
|     URI::Fetch: 200
|     Python-urllib/2.5: 200
|     MFC_Tear_Sample: 200
|     GT::WWW: 200
|     Zend_Http_Client: 200
|     PHP/: 200
|     Snoopy: 200
|     Wget/1.13.4 (linux-gnu): 200
|     PHPCrawl: 200
|     HTTP::Lite: 200
|     WWW-Mechanize/1.34: 200
|     libcurl-agent/1.0: 200
|     lwp-trivial: 200
|     PECL::HTTP: 200
|_    http client: 200
| http-vhosts: 
| 85 names had status ERROR
|_43 names had status 200
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Oct 28 14:57:11 2021 -- 1 IP address (1 host up) scanned in 59.57 seconds

```
