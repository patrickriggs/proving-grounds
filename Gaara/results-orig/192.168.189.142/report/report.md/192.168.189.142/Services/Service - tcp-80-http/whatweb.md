```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.189.142:80 2>&1
```

[/home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/tcp_80_http_whatweb.txt](file:///home/tac0shell/hacking/pg/Gaara/results/192.168.189.142/scans/tcp80/tcp_80_http_whatweb.txt):

```
WhatWeb report for http://192.168.189.142:80
Status    : 200 OK
Title     : Gaara
IP        : 192.168.189.142
Country   : RESERVED, ZZ

Summary   : HTTPServer[Debian Linux][Apache/2.4.38 (Debian)]

Detected Plugins:
[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Debian Linux
	String       : Apache/2.4.38 (Debian) (from server string)

HTTP Headers:
	HTTP/1.1 200 OK
	Date: Thu, 28 Oct 2021 18:56:14 GMT
	Server: Apache/2.4.38 (Debian)
	Last-Modified: Tue, 30 Mar 2021 12:04:12 GMT
	ETag: "89-5bebfcbe49cf9-gzip"
	Accept-Ranges: bytes
	Vary: Accept-Encoding
	Content-Encoding: gzip
	Content-Length: 113
	Connection: close
	Content-Type: text/html



```
