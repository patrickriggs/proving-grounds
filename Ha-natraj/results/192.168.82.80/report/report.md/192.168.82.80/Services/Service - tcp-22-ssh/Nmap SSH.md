```bash
nmap -vv --reason -Pn -T4 -sV -p 22 --script="banner,ssh2-enum-algos,ssh-hostkey,ssh-auth-methods" -oN "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp22/tcp_22_ssh_nmap.txt" -oX "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp22/xml/tcp_22_ssh_nmap.xml" 192.168.82.80
```

[/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp22/tcp_22_ssh_nmap.txt](file:///home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp22/tcp_22_ssh_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Oct 30 05:31:37 2021 as: nmap -vv --reason -Pn -T4 -sV -p 22 --script=banner,ssh2-enum-algos,ssh-hostkey,ssh-auth-methods -oN /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp22/tcp_22_ssh_nmap.txt -oX /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp22/xml/tcp_22_ssh_nmap.xml 192.168.82.80
Nmap scan report for 192.168.82.80
Host is up, received user-set (0.096s latency).
Scanned at 2021-10-30 05:31:38 EDT for 3s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
|_banner: SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3
| ssh2-enum-algos: 
|   kex_algorithms: (10)
|       curve25519-sha256
|       curve25519-sha256@libssh.org
|       ecdh-sha2-nistp256
|       ecdh-sha2-nistp384
|       ecdh-sha2-nistp521
|       diffie-hellman-group-exchange-sha256
|       diffie-hellman-group16-sha512
|       diffie-hellman-group18-sha512
|       diffie-hellman-group14-sha256
|       diffie-hellman-group14-sha1
|   server_host_key_algorithms: (5)
|       ssh-rsa
|       rsa-sha2-512
|       rsa-sha2-256
|       ecdsa-sha2-nistp256
|       ssh-ed25519
|   encryption_algorithms: (6)
|       chacha20-poly1305@openssh.com
|       aes128-ctr
|       aes192-ctr
|       aes256-ctr
|       aes128-gcm@openssh.com
|       aes256-gcm@openssh.com
|   mac_algorithms: (10)
|       umac-64-etm@openssh.com
|       umac-128-etm@openssh.com
|       hmac-sha2-256-etm@openssh.com
|       hmac-sha2-512-etm@openssh.com
|       hmac-sha1-etm@openssh.com
|       umac-64@openssh.com
|       umac-128@openssh.com
|       hmac-sha2-256
|       hmac-sha2-512
|       hmac-sha1
|   compression_algorithms: (2)
|       none
|_      zlib@openssh.com
| ssh-hostkey: 
|   2048 d9:9f:da:f4:2e:67:01:92:d5:da:7f:70:d0:06:b3:92 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+Gv/kpy3r+s15xcQ3TABj4bHKW6cfSBW4Nm8UutdX8W6JJam+7EOpwOpbsItLbkm2nrWEB72D47z5ayx63Hn+e8qGn8Vw9yzZS0z2JSMOWyeYVEYM4G9dPZlUJavoOOe4zrWiiZtj3IbMZy1wnjhaEgne5sC27o+1a73+Lgwz/xik+XtlCEUyxK+RnUa7dEEF9HIy+5B2qptnrUdISLDzXMwUFRlXM7GlA84Y8X0DLs90YNaDCxnvjkp5VOTIWDKtt78U+9ClEgWMkfHGSpiuvGMm1AHCkFPLtNfDoF6pYm2lOI4Lv090Ce/TRqBFCPq1oL6MrpkSpq6tXhEh4wox
|   256 bc:ea:f1:3b:fa:7c:05:0c:92:95:92:e9:e7:d2:07:71 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBN7p17tEdnU25MlcknnznQEFmFu3wnoXy7Tam4z8/7sv+l/G3FkLJkfyeRCHMo5Y+z6rGNfB1Zt9jshB8TDkdCg=
|   256 f0:24:5b:7a:3b:d6:b7:94:c4:4b:fe:57:21:f8:00:61 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPugHptbBU0i1SJ0DkVvuyGN9HsQf0GzlPTdJYJqKE+U
| ssh-auth-methods: 
|   Supported authentication methods: 
|     publickey
|_    password
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Oct 30 05:31:41 2021 -- 1 IP address (1 host up) scanned in 3.56 seconds

```
