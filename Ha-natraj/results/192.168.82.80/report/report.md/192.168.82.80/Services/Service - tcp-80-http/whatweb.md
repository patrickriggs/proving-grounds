```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.82.80:80 2>&1
```

[/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/tcp_80_http_whatweb.txt](file:///home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/tcp_80_http_whatweb.txt):

```
WhatWeb report for http://192.168.82.80:80
Status    : 200 OK
Title     : HA:Natraj
IP        : 192.168.82.80
Country   : RESERVED, ZZ

Summary   : Apache[2.4.29], HTML5, Script, HTTPServer[Ubuntu Linux][Apache/2.4.29 (Ubuntu)]

Detected Plugins:
[ Apache ]
	The Apache HTTP Server Project is an effort to develop and
	maintain an open-source HTTP server for modern operating
	systems including UNIX and Windows NT. The goal of this
	project is to provide a secure, efficient and extensible
	server that provides HTTP services in sync with the current
	HTTP standards.

	Version      : 2.4.29 (from HTTP Server Header)
	Google Dorks: (3)
	Website     : http://httpd.apache.org/

[ HTML5 ]
	HTML version 5, detected by the doctype declaration


[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Ubuntu Linux
	String       : Apache/2.4.29 (Ubuntu) (from server string)

[ Script ]
	This plugin detects instances of script HTML elements and
	returns the script language/type.


HTTP Headers:
	HTTP/1.1 200 OK
	Date: Sat, 30 Oct 2021 09:31:41 GMT
	Server: Apache/2.4.29 (Ubuntu)
	Last-Modified: Wed, 03 Jun 2020 14:42:30 GMT
	ETag: "38a1-5a72f099ae180-gzip"
	Accept-Ranges: bytes
	Vary: Accept-Encoding
	Content-Encoding: gzip
	Content-Length: 5377
	Connection: close
	Content-Type: text/html



```
