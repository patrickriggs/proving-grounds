```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.82.80
```

[/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Oct 30 05:31:38 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.82.80
Nmap scan report for 192.168.82.80
Host is up, received user-set (0.094s latency).
Scanned at 2021-10-30 05:31:38 EDT for 23s

Bug in http-security-headers: no string output.
PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.29 ((Ubuntu))
| http-vhosts: 
|_128 names had status 200
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.82.80
|     
|     Path: http://192.168.82.80:80/
|     Line number: 94
|     Comment: 
|         <!-- End Gallery Section -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 108
|     Comment: 
|         <!-- End About Section -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 22
|     Comment: 
|         <!-- Sidebar with image -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 167
|     Comment: 
|         <!-- END PAGE CONTENT -->
|     
|     Path: http://192.168.82.80:80/style.css
|     Line number: 148
|     Comment: 
|         /* Colors */
|     
|     Path: http://192.168.82.80:80/
|     Line number: 119
|     Comment: 
|         <!-- End Etymology Section -->
|     
|     Path: http://192.168.82.80:80/font.css
|     Line number: 17
|     Comment: 
|         /* vietnamese */
|     
|     Path: http://192.168.82.80:80/
|     Line number: 97
|     Comment: 
|         <!-- About Section -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 43
|     Comment: 
|         <!-- Page Content -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 171
|     Comment: 
|         
|         
|     
|     Path: http://192.168.82.80:80/
|     Line number: 58
|     Comment: 
|         <!-- Gallery Section -->
|     
|     Path: http://192.168.82.80:80/style.css
|     Line number: 26
|     Comment: 
|         /* End extract */
|     
|     Path: http://192.168.82.80:80/font.css
|     Line number: 33
|     Comment: 
|         /* latin */
|     
|     Path: http://192.168.82.80:80/
|     Line number: 164
|     Comment: 
|         <!-- End footer -->
|     
|     Path: http://192.168.82.80:80/font.css
|     Line number: 25
|     Comment: 
|         /* latin-ext */
|     
|     Path: http://192.168.82.80:80/
|     Line number: 92
|     Comment: 
|         <!-- End photo grid -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 120
|     Comment: 
|         <!-- Depiction Section -->
|     
|     Path: http://192.168.82.80:80/style.css
|     Line number: 1
|     Comment: 
|         /* W3.CSS 4.13 June 2019 by Jan Egil and Borge Refsnes */
|     
|     Path: http://192.168.82.80:80/style.css
|     Line number: 3
|     Comment: 
|         /* Extract from normalize.css by Nicolas Gallagher and Jonathan Neal git.io/normalize */
|     
|     Path: http://192.168.82.80:80/font.css
|     Line number: 9
|     Comment: 
|         /* cyrillic */
|     
|     Path: http://192.168.82.80:80/font.css
|     Line number: 1
|     Comment: 
|         /* cyrillic-ext */
|     
|     Path: http://192.168.82.80:80/
|     Line number: 156
|     Comment: 
|         <!-- Footer -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 151
|     Comment: 
|         <!-- End Characteristics Section -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 63
|     Comment: 
|         <!-- Grid for photos -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 27
|     Comment: 
|         <!-- Hidden Sidebar (reveals when clicked on menu icon)-->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 46
|     Comment: 
|         <!-- Menu icon to open sidebar -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 49
|     Comment: 
|         <!-- Header -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 109
|     Comment: 
|         <!-- Etymology Section -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 129
|     Comment: 
|         <!-- End Depiction Section -->
|     
|     Path: http://192.168.82.80:80/
|     Line number: 130
|     Comment: 
|_        <!-- Characteristics Section -->
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-server-header: Apache/2.4.29 (Ubuntu)
| http-enum: 
|   /console/: Potentially interesting directory w/ listing on 'apache/2.4.29 (ubuntu)'
|_  /images/: Potentially interesting directory w/ listing on 'apache/2.4.29 (ubuntu)'
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1; css: 1
|     /images/
|       jpg: 18
|   Longest directory structure:
|     Depth: 1
|     Dir: /images/
|   Total files found (by extension):
|_    Other: 1; css: 1; jpg: 18
|_http-feed: Couldn't find any feeds.
| http-internal-ip-disclosure: 
|_  Internal IP Leaked: 127.0.0.1
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-exif-spider: ERROR: Script execution failed (use -d to debug)
|_http-errors: Couldn't find any error pages.
|_http-mobileversion-checker: No mobile version detected.
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-php-version: Logo query returned unknown hash 18036202884039981bde11d5ab889f31
|_Credits query returned unknown hash 18036202884039981bde11d5ab889f31
|_http-litespeed-sourcecode-download: Request with null byte did not work. This web server might not be vulnerable
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-chrono: Request times for /; avg: 406.65ms; min: 360.12ms; max: 475.22ms
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-malware-host: Host appears to be clean
|_http-config-backup: ERROR: Script execution failed (use -d to debug)
| http-headers: 
|   Date: Sat, 30 Oct 2021 09:31:46 GMT
|   Server: Apache/2.4.29 (Ubuntu)
|   Last-Modified: Wed, 03 Jun 2020 14:42:30 GMT
|   ETag: "38a1-5a72f099ae180"
|   Accept-Ranges: bytes
|   Content-Length: 14497
|   Vary: Accept-Encoding
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-date: Sat, 30 Oct 2021 09:31:47 GMT; 0s from local time.
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
|_http-title: HA:Natraj
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Oct 30 05:32:01 2021 -- 1 IP address (1 host up) scanned in 23.83 seconds

```
