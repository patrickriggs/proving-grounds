```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_quick_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/xml/_quick_tcp_nmap.xml" 192.168.82.80
```

[/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_quick_tcp_nmap.txt](file:///home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_quick_tcp_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Oct 30 05:31:05 2021 as: nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_quick_tcp_nmap.txt -oX /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/xml/_quick_tcp_nmap.xml 192.168.82.80
Increasing send delay for 192.168.82.80 from 0 to 5 due to 259 out of 647 dropped probes since last increase.
Increasing send delay for 192.168.82.80 from 5 to 10 due to 39 out of 97 dropped probes since last increase.
Nmap scan report for 192.168.82.80
Host is up, received user-set (0.095s latency).
Scanned at 2021-10-30 05:31:05 EDT for 32s
Not shown: 998 closed tcp ports (reset)
PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 d9:9f:da:f4:2e:67:01:92:d5:da:7f:70:d0:06:b3:92 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+Gv/kpy3r+s15xcQ3TABj4bHKW6cfSBW4Nm8UutdX8W6JJam+7EOpwOpbsItLbkm2nrWEB72D47z5ayx63Hn+e8qGn8Vw9yzZS0z2JSMOWyeYVEYM4G9dPZlUJavoOOe4zrWiiZtj3IbMZy1wnjhaEgne5sC27o+1a73+Lgwz/xik+XtlCEUyxK+RnUa7dEEF9HIy+5B2qptnrUdISLDzXMwUFRlXM7GlA84Y8X0DLs90YNaDCxnvjkp5VOTIWDKtt78U+9ClEgWMkfHGSpiuvGMm1AHCkFPLtNfDoF6pYm2lOI4Lv090Ce/TRqBFCPq1oL6MrpkSpq6tXhEh4wox
|   256 bc:ea:f1:3b:fa:7c:05:0c:92:95:92:e9:e7:d2:07:71 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBN7p17tEdnU25MlcknnznQEFmFu3wnoXy7Tam4z8/7sv+l/G3FkLJkfyeRCHMo5Y+z6rGNfB1Zt9jshB8TDkdCg=
|   256 f0:24:5b:7a:3b:d6:b7:94:c4:4b:fe:57:21:f8:00:61 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPugHptbBU0i1SJ0DkVvuyGN9HsQf0GzlPTdJYJqKE+U
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.29 ((Ubuntu))
|_http-title: HA:Natraj
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.29 (Ubuntu)
Aggressive OS guesses: Linux 4.9 (94%), Linux 3.10 - 3.12 (94%), Linux 4.4 (94%), Linux 3.10 (92%), Linux 3.11 - 4.1 (91%), Linux 3.10 - 3.16 (91%), Linux 2.6.32 (91%), Linux 2.6.32 or 3.10 (91%), Linux 4.0 (90%), Synology DiskStation Manager 5.1 (90%)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.92%E=4%D=10/30%OT=22%CT=1%CU=37412%PV=Y%DS=2%DC=T%G=Y%TM=617D10
OS:F9%P=x86_64-pc-linux-gnu)SEQ(SP=FB%GCD=1%ISR=10E%TI=Z%TS=A)OPS(O1=M506ST
OS:11NW7%O2=M506ST11NW7%O3=M506NNT11NW7%O4=M506ST11NW7%O5=M506ST11NW7%O6=M5
OS:06ST11)WIN(W1=7120%W2=7120%W3=7120%W4=7120%W5=7120%W6=7120)ECN(R=Y%DF=Y%
OS:TG=40%W=7210%O=M506NNSNW7%CC=Y%Q=)ECN(R=Y%DF=Y%T=40%W=7210%O=M506NNSNW7%
OS:CC=Y%Q=)T1(R=Y%DF=Y%TG=40%S=O%A=S+%F=AS%RD=0%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S
OS:+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=N)T5(R=Y%DF=Y%TG=40%W=0%S=Z%A=S+%F=AR%
OS:O=%RD=0%Q=)T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=N)T7(R=N)U
OS:1(R=N)U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE
OS:(R=Y%DFI=N%TG=40%CD=S)IE(R=Y%DFI=N%T=40%CD=S)

Uptime guess: 35.020 days (since Sat Sep 25 05:02:15 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=251 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 23/tcp)
HOP RTT      ADDRESS
1   94.24 ms 192.168.49.1
2   94.41 ms 192.168.82.80

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Oct 30 05:31:37 2021 -- 1 IP address (1 host up) scanned in 32.88 seconds

```
