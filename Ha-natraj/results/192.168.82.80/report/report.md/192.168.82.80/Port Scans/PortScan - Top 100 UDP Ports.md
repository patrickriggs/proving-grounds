```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/xml/_top_100_udp_nmap.xml" 192.168.82.80
```

[/home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Oct 30 05:31:05 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Ha-natraj/results/192.168.82.80/scans/xml/_top_100_udp_nmap.xml 192.168.82.80
Warning: 192.168.82.80 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.82.80 from 100 to 200 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.82.80 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.82.80 from 400 to 800 due to 11 out of 11 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -59188 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -60579 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -60579 microseconds.  Ignoring time.
Nmap scan report for 192.168.82.80
Host is up, received user-set (0.095s latency).
Scanned at 2021-10-30 05:31:05 EDT for 304s

PORT      STATE         SERVICE         REASON              VERSION
7/udp     open|filtered echo            no-response
9/udp     open|filtered discard         no-response
17/udp    open|filtered qotd            no-response
19/udp    closed        chargen         port-unreach ttl 63
49/udp    open|filtered tacacs          no-response
53/udp    closed        domain          port-unreach ttl 63
67/udp    closed        dhcps           port-unreach ttl 63
68/udp    closed        dhcpc           port-unreach ttl 63
69/udp    closed        tftp            port-unreach ttl 63
80/udp    closed        http            port-unreach ttl 63
88/udp    closed        kerberos-sec    port-unreach ttl 63
111/udp   closed        rpcbind         port-unreach ttl 63
120/udp   open|filtered cfdptkt         no-response
123/udp   closed        ntp             port-unreach ttl 63
135/udp   closed        msrpc           port-unreach ttl 63
136/udp   closed        profile         port-unreach ttl 63
137/udp   closed        netbios-ns      port-unreach ttl 63
138/udp   open|filtered netbios-dgm     no-response
139/udp   closed        netbios-ssn     port-unreach ttl 63
158/udp   open|filtered pcmail-srv      no-response
161/udp   closed        snmp            port-unreach ttl 63
162/udp   open|filtered snmptrap        no-response
177/udp   closed        xdmcp           port-unreach ttl 63
427/udp   open|filtered svrloc          no-response
443/udp   open|filtered https           no-response
445/udp   closed        microsoft-ds    port-unreach ttl 63
497/udp   closed        retrospect      port-unreach ttl 63
500/udp   closed        isakmp          port-unreach ttl 63
514/udp   closed        syslog          port-unreach ttl 63
515/udp   closed        printer         port-unreach ttl 63
518/udp   open|filtered ntalk           no-response
520/udp   closed        route           port-unreach ttl 63
593/udp   closed        http-rpc-epmap  port-unreach ttl 63
623/udp   open|filtered asf-rmcp        no-response
626/udp   closed        serialnumberd   port-unreach ttl 63
631/udp   closed        ipp             port-unreach ttl 63
996/udp   open|filtered vsinet          no-response
997/udp   open|filtered maitrd          no-response
998/udp   open|filtered puparp          no-response
999/udp   open|filtered applix          no-response
1022/udp  open|filtered exp2            no-response
1023/udp  open|filtered unknown         no-response
1025/udp  closed        blackjack       port-unreach ttl 63
1026/udp  closed        win-rpc         port-unreach ttl 63
1027/udp  closed        unknown         port-unreach ttl 63
1028/udp  open|filtered ms-lsa          no-response
1029/udp  open|filtered solid-mux       no-response
1030/udp  open|filtered iad1            no-response
1433/udp  closed        ms-sql-s        port-unreach ttl 63
1434/udp  closed        ms-sql-m        port-unreach ttl 63
1645/udp  closed        radius          port-unreach ttl 63
1646/udp  closed        radacct         port-unreach ttl 63
1701/udp  open|filtered L2TP            no-response
1718/udp  open|filtered h225gatedisc    no-response
1719/udp  closed        h323gatestat    port-unreach ttl 63
1812/udp  closed        radius          port-unreach ttl 63
1813/udp  open|filtered radacct         no-response
1900/udp  closed        upnp            port-unreach ttl 63
2000/udp  open|filtered cisco-sccp      no-response
2048/udp  open|filtered dls-monitor     no-response
2049/udp  open|filtered nfs             no-response
2222/udp  closed        msantipiracy    port-unreach ttl 63
2223/udp  closed        rockwell-csp2   port-unreach ttl 63
3283/udp  closed        netassistant    port-unreach ttl 63
3456/udp  closed        IISrpc-or-vat   port-unreach ttl 63
3703/udp  open|filtered adobeserver-3   no-response
4444/udp  closed        krb524          port-unreach ttl 63
4500/udp  closed        nat-t-ike       port-unreach ttl 63
5000/udp  closed        upnp            port-unreach ttl 63
5060/udp  closed        sip             port-unreach ttl 63
5353/udp  closed        zeroconf        port-unreach ttl 63
5632/udp  open|filtered pcanywherestat  no-response
9200/udp  closed        wap-wsp         port-unreach ttl 63
10000/udp closed        ndmp            port-unreach ttl 63
17185/udp open|filtered wdbrpc          no-response
20031/udp closed        bakbonenetvault port-unreach ttl 63
30718/udp closed        unknown         port-unreach ttl 63
31337/udp closed        BackOrifice     port-unreach ttl 63
32768/udp open|filtered omad            no-response
32769/udp closed        filenet-rpc     port-unreach ttl 63
32771/udp closed        sometimes-rpc6  port-unreach ttl 63
32815/udp open|filtered unknown         no-response
33281/udp closed        unknown         port-unreach ttl 63
49152/udp closed        unknown         port-unreach ttl 63
49153/udp closed        unknown         port-unreach ttl 63
49154/udp closed        unknown         port-unreach ttl 63
49156/udp closed        unknown         port-unreach ttl 63
49181/udp closed        unknown         port-unreach ttl 63
49182/udp closed        unknown         port-unreach ttl 63
49185/udp open|filtered unknown         no-response
49186/udp open|filtered unknown         no-response
49188/udp open|filtered unknown         no-response
49190/udp closed        unknown         port-unreach ttl 63
49191/udp open|filtered unknown         no-response
49192/udp closed        unknown         port-unreach ttl 63
49193/udp open|filtered unknown         no-response
49194/udp open|filtered unknown         no-response
49200/udp closed        unknown         port-unreach ttl 63
49201/udp open|filtered unknown         no-response
65024/udp open|filtered unknown         no-response
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.92%E=4%D=10/30%OT=%CT=%CU=19%PV=Y%DS=2%DC=T%G=N%TM=617D1209%P=x86_64-pc-linux-gnu)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 514/udp)
HOP RTT      ADDRESS
1   96.27 ms 192.168.49.1
2   96.48 ms 192.168.82.80

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Oct 30 05:36:09 2021 -- 1 IP address (1 host up) scanned in 304.55 seconds

```
