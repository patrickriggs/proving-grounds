```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.149.35:80 2>&1
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/tcp80/tcp_80_http_whatweb.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/tcp80/tcp_80_http_whatweb.txt):

```
WhatWeb report for http://192.168.149.35:80
Status    : 200 OK
Title     : Apache2 Ubuntu Default Page: It works
IP        : 192.168.149.35
Country   : RESERVED, ZZ

Summary   : Apache[2.4.29], HTTPServer[Ubuntu Linux][Apache/2.4.29 (Ubuntu)]

Detected Plugins:
[ Apache ]
	The Apache HTTP Server Project is an effort to develop and
	maintain an open-source HTTP server for modern operating
	systems including UNIX and Windows NT. The goal of this
	project is to provide a secure, efficient and extensible
	server that provides HTTP services in sync with the current
	HTTP standards.

	Version      : 2.4.29 (from HTTP Server Header)
	Google Dorks: (3)
	Website     : http://httpd.apache.org/

[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Ubuntu Linux
	String       : Apache/2.4.29 (Ubuntu) (from server string)

HTTP Headers:
	HTTP/1.1 200 OK
	Date: Sat, 23 Oct 2021 20:29:58 GMT
	Server: Apache/2.4.29 (Ubuntu)
	Last-Modified: Sun, 20 Oct 2019 15:04:12 GMT
	ETag: "2aa6-59558e1434548-gzip"
	Accept-Ranges: bytes
	Vary: Accept-Encoding
	Content-Encoding: gzip
	Content-Length: 3138
	Connection: close
	Content-Type: text/html



```
