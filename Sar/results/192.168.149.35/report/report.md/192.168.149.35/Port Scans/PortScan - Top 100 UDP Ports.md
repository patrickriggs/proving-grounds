```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/xml/_top_100_udp_nmap.xml" 192.168.149.35
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.91 scan initiated Sat Oct 23 16:29:33 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Sar/results/192.168.149.35/scans/xml/_top_100_udp_nmap.xml 192.168.149.35
Warning: 192.168.149.35 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.149.35 from 100 to 200 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.149.35 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.149.35 from 400 to 800 due to 11 out of 11 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -55298 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -54604 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -54604 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -207910 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -207910 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -210598 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -210598 microseconds.  Ignoring time.
Nmap scan report for 192.168.149.35
Host is up, received user-set (0.088s latency).
Scanned at 2021-10-23 16:29:33 EDT for 246s
Not shown: 83 closed ports
Reason: 83 port-unreaches
PORT      STATE         SERVICE       REASON      VERSION
67/udp    open|filtered dhcps         no-response
68/udp    open|filtered dhcpc         no-response
120/udp   open|filtered cfdptkt       no-response
138/udp   open|filtered netbios-dgm   no-response
139/udp   open|filtered netbios-ssn   no-response
161/udp   open|filtered snmp          no-response
177/udp   open|filtered xdmcp         no-response
623/udp   open|filtered asf-rmcp      no-response
626/udp   open|filtered serialnumberd no-response
631/udp   open|filtered ipp           no-response
996/udp   open|filtered vsinet        no-response
1023/udp  open|filtered unknown       no-response
1812/udp  open|filtered radius        no-response
2000/udp  open|filtered cisco-sccp    no-response
3703/udp  open|filtered adobeserver-3 no-response
5353/udp  open|filtered zeroconf      no-response
33281/udp open|filtered unknown       no-response
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.91%E=4%D=10/23%OT=%CT=%CU=7%PV=Y%DS=2%DC=T%G=N%TM=617471A3%P=x86_64-pc-linux-gnu)
SEQ(II=I)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 49193/udp)
HOP RTT      ADDRESS
1   97.83 ms 192.168.49.1
2   98.09 ms 192.168.149.35

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Oct 23 16:33:39 2021 -- 1 IP address (1 host up) scanned in 246.93 seconds

```
