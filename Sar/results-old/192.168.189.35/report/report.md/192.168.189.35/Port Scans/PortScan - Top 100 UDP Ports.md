```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/xml/_top_100_udp_nmap.xml" 192.168.189.35
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.91 scan initiated Thu Oct 21 14:27:03 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/xml/_top_100_udp_nmap.xml 192.168.189.35
Warning: 192.168.189.35 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.189.35 from 100 to 200 due to 11 out of 22 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -79908 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -79908 microseconds.  Ignoring time.
Nmap scan report for 192.168.189.35
Host is up, received user-set (0.098s latency).
Scanned at 2021-10-21 14:27:04 EDT for 444s

PORT      STATE         SERVICE         REASON              VERSION
7/udp     closed        echo            port-unreach ttl 63
9/udp     closed        discard         port-unreach ttl 63
17/udp    closed        qotd            port-unreach ttl 63
19/udp    open|filtered chargen         no-response
49/udp    open|filtered tacacs          no-response
53/udp    closed        domain          port-unreach ttl 63
67/udp    open|filtered dhcps           no-response
68/udp    closed        dhcpc           port-unreach ttl 63
69/udp    closed        tftp            port-unreach ttl 63
80/udp    closed        http            port-unreach ttl 63
88/udp    open|filtered kerberos-sec    no-response
111/udp   open|filtered rpcbind         no-response
120/udp   open|filtered cfdptkt         no-response
123/udp   closed        ntp             port-unreach ttl 63
135/udp   closed        msrpc           port-unreach ttl 63
136/udp   closed        profile         port-unreach ttl 63
137/udp   open|filtered netbios-ns      no-response
138/udp   open|filtered netbios-dgm     no-response
139/udp   open|filtered netbios-ssn     no-response
158/udp   closed        pcmail-srv      port-unreach ttl 63
161/udp   open|filtered snmp            no-response
162/udp   closed        snmptrap        port-unreach ttl 63
177/udp   open|filtered xdmcp           no-response
427/udp   open|filtered svrloc          no-response
443/udp   open|filtered https           no-response
445/udp   closed        microsoft-ds    port-unreach ttl 63
497/udp   open|filtered retrospect      no-response
500/udp   closed        isakmp          port-unreach ttl 63
514/udp   closed        syslog          port-unreach ttl 63
515/udp   open|filtered printer         no-response
518/udp   open|filtered ntalk           no-response
520/udp   open|filtered route           no-response
593/udp   open|filtered http-rpc-epmap  no-response
623/udp   open|filtered asf-rmcp        no-response
626/udp   closed        serialnumberd   port-unreach ttl 63
631/udp   open|filtered ipp             no-response
996/udp   closed        vsinet          port-unreach ttl 63
997/udp   open|filtered maitrd          no-response
998/udp   closed        puparp          port-unreach ttl 63
999/udp   open|filtered applix          no-response
1022/udp  closed        exp2            port-unreach ttl 63
1023/udp  open|filtered unknown         no-response
1025/udp  closed        blackjack       port-unreach ttl 63
1026/udp  closed        win-rpc         port-unreach ttl 63
1027/udp  closed        unknown         port-unreach ttl 63
1028/udp  open|filtered ms-lsa          no-response
1029/udp  open|filtered solid-mux       no-response
1030/udp  open|filtered iad1            no-response
1433/udp  open|filtered ms-sql-s        no-response
1434/udp  open|filtered ms-sql-m        no-response
1645/udp  open|filtered radius          no-response
1646/udp  open|filtered radacct         no-response
1701/udp  closed        L2TP            port-unreach ttl 63
1718/udp  open|filtered h225gatedisc    no-response
1719/udp  open|filtered h323gatestat    no-response
1812/udp  closed        radius          port-unreach ttl 63
1813/udp  open|filtered radacct         no-response
1900/udp  closed        upnp            port-unreach ttl 63
2000/udp  open|filtered cisco-sccp      no-response
2048/udp  closed        dls-monitor     port-unreach ttl 63
2049/udp  closed        nfs             port-unreach ttl 63
2222/udp  open|filtered msantipiracy    no-response
2223/udp  open|filtered rockwell-csp2   no-response
3283/udp  closed        netassistant    port-unreach ttl 63
3456/udp  open|filtered IISrpc-or-vat   no-response
3703/udp  open|filtered adobeserver-3   no-response
4444/udp  open|filtered krb524          no-response
4500/udp  closed        nat-t-ike       port-unreach ttl 63
5000/udp  open|filtered upnp            no-response
5060/udp  closed        sip             port-unreach ttl 63
5353/udp  open|filtered zeroconf        no-response
5632/udp  open|filtered pcanywherestat  no-response
9200/udp  closed        wap-wsp         port-unreach ttl 63
10000/udp closed        ndmp            port-unreach ttl 63
17185/udp open|filtered wdbrpc          no-response
20031/udp open|filtered bakbonenetvault no-response
30718/udp closed        unknown         port-unreach ttl 63
31337/udp closed        BackOrifice     port-unreach ttl 63
32768/udp closed        omad            port-unreach ttl 63
32769/udp open|filtered filenet-rpc     no-response
32771/udp closed        sometimes-rpc6  port-unreach ttl 63
32815/udp open|filtered unknown         no-response
33281/udp closed        unknown         port-unreach ttl 63
49152/udp open|filtered unknown         no-response
49153/udp open|filtered unknown         no-response
49154/udp open|filtered unknown         no-response
49156/udp closed        unknown         port-unreach ttl 63
49181/udp open|filtered unknown         no-response
49182/udp open|filtered unknown         no-response
49185/udp closed        unknown         port-unreach ttl 63
49186/udp open|filtered unknown         no-response
49188/udp open|filtered unknown         no-response
49190/udp closed        unknown         port-unreach ttl 63
49191/udp closed        unknown         port-unreach ttl 63
49192/udp open|filtered unknown         no-response
49193/udp open|filtered unknown         no-response
49194/udp closed        unknown         port-unreach ttl 63
49200/udp open|filtered unknown         no-response
49201/udp closed        unknown         port-unreach ttl 63
65024/udp closed        unknown         port-unreach ttl 63
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.91%E=4%D=10/21%OT=%CT=%CU=7%PV=Y%DS=2%DC=T%G=N%TM=6171B2B4%P=x86_64-pc-linux-gnu)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 10000/udp)
HOP RTT       ADDRESS
1   100.09 ms 192.168.49.1
2   100.24 ms 192.168.189.35

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Oct 21 14:34:28 2021 -- 1 IP address (1 host up) scanned in 445.11 seconds

```
