```bash
feroxbuster -u http://192.168.189.35:80/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/common.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_common.txt"
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_common.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_common.txt):

```
403        9l       28w      279c http://192.168.189.35/.hta
403        9l       28w      279c http://192.168.189.35/.htaccess
403        9l       28w      279c http://192.168.189.35/.hta.txt
403        9l       28w      279c http://192.168.189.35/.htaccess.txt
403        9l       28w      279c http://192.168.189.35/.hta.html
403        9l       28w      279c http://192.168.189.35/.hta.php
403        9l       28w      279c http://192.168.189.35/.htaccess.html
403        9l       28w      279c http://192.168.189.35/.htpasswd
403        9l       28w      279c http://192.168.189.35/.hta.asp
403        9l       28w      279c http://192.168.189.35/.htaccess.php
403        9l       28w      279c http://192.168.189.35/.htpasswd.txt
403        9l       28w      279c http://192.168.189.35/.htaccess.asp
403        9l       28w      279c http://192.168.189.35/.hta.aspx
403        9l       28w      279c http://192.168.189.35/.htpasswd.html
403        9l       28w      279c http://192.168.189.35/.hta.jsp
403        9l       28w      279c http://192.168.189.35/.htaccess.aspx
403        9l       28w      279c http://192.168.189.35/.htpasswd.php
403        9l       28w      279c http://192.168.189.35/.htaccess.jsp
403        9l       28w      279c http://192.168.189.35/.htpasswd.asp
403        9l       28w      279c http://192.168.189.35/.htpasswd.aspx
403        9l       28w      279c http://192.168.189.35/.htpasswd.jsp
200      375l      964w    10918c http://192.168.189.35/index.html
200     1170l     5860w        0c http://192.168.189.35/phpinfo.php
200        1l        1w        9c http://192.168.189.35/robots.txt
403        9l       28w      279c http://192.168.189.35/server-status

```
```bash
feroxbuster -u http://192.168.189.35:80/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/big.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_big.txt"
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_big.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_big.txt):

```
403        9l       28w      279c http://192.168.189.35/.htpasswd
403        9l       28w      279c http://192.168.189.35/.htaccess
403        9l       28w      279c http://192.168.189.35/.htpasswd.txt
403        9l       28w      279c http://192.168.189.35/.htpasswd.html
403        9l       28w      279c http://192.168.189.35/.htpasswd.php
403        9l       28w      279c http://192.168.189.35/.htaccess.txt
403        9l       28w      279c http://192.168.189.35/.htpasswd.asp
403        9l       28w      279c http://192.168.189.35/.htaccess.html
403        9l       28w      279c http://192.168.189.35/.htpasswd.aspx
403        9l       28w      279c http://192.168.189.35/.htaccess.php
403        9l       28w      279c http://192.168.189.35/.htpasswd.jsp
403        9l       28w      279c http://192.168.189.35/.htaccess.asp
403        9l       28w      279c http://192.168.189.35/.htaccess.aspx
403        9l       28w      279c http://192.168.189.35/.htaccess.jsp
200      375l      964w    10918c http://192.168.189.35/index.html
200     1170l     5860w        0c http://192.168.189.35/phpinfo.php
200        1l        1w        9c http://192.168.189.35/robots.txt
403        9l       28w      279c http://192.168.189.35/server-status

```
```bash
feroxbuster -u http://192.168.189.35:80/ -t 10 -w /usr/share/seclists/Discovery/Web-Content/raft-large-words.txt -x "txt,html,php,asp,aspx,jsp" -v -k -n -q -o "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_raft-large-words.txt"
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_raft-large-words.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_feroxbuster_raft-large-words.txt):

```
403        9l       28w      279c http://192.168.189.35/.php
403        9l       28w      279c http://192.168.189.35/.html
403        9l       28w      279c http://192.168.189.35/.html.txt
403        9l       28w      279c http://192.168.189.35/.html.html
403        9l       28w      279c http://192.168.189.35/.html.php
403        9l       28w      279c http://192.168.189.35/.html.asp
403        9l       28w      279c http://192.168.189.35/.html.aspx
403        9l       28w      279c http://192.168.189.35/.html.jsp
200      375l      964w    10918c http://192.168.189.35/index.html
403        9l       28w      279c http://192.168.189.35/.htm
403        9l       28w      279c http://192.168.189.35/.htm.txt
403        9l       28w      279c http://192.168.189.35/.htm.html
403        9l       28w      279c http://192.168.189.35/.htm.php
403        9l       28w      279c http://192.168.189.35/.htm.asp
403        9l       28w      279c http://192.168.189.35/.htm.aspx
403        9l       28w      279c http://192.168.189.35/.htm.jsp
200      375l      964w    10918c http://192.168.189.35/
403        9l       28w      279c http://192.168.189.35/.htaccess
403        9l       28w      279c http://192.168.189.35/.htaccess.txt
403        9l       28w      279c http://192.168.189.35/.htaccess.html
403        9l       28w      279c http://192.168.189.35/.htaccess.php
403        9l       28w      279c http://192.168.189.35/.htaccess.asp
403        9l       28w      279c http://192.168.189.35/.htaccess.aspx
403        9l       28w      279c http://192.168.189.35/.htaccess.jsp
200        1l        1w        9c http://192.168.189.35/robots.txt
200     1170l     5860w        0c http://192.168.189.35/phpinfo.php
403        9l       28w      279c http://192.168.189.35/.phtml
403        9l       28w      279c http://192.168.189.35/.htc
403        9l       28w      279c http://192.168.189.35/.htc.txt
403        9l       28w      279c http://192.168.189.35/.htc.html
403        9l       28w      279c http://192.168.189.35/.htc.php
403        9l       28w      279c http://192.168.189.35/.htc.asp
403        9l       28w      279c http://192.168.189.35/.htc.aspx
403        9l       28w      279c http://192.168.189.35/.htc.jsp
403        9l       28w      279c http://192.168.189.35/.html_var_DE
403        9l       28w      279c http://192.168.189.35/.html_var_DE.txt
403        9l       28w      279c http://192.168.189.35/.html_var_DE.html
403        9l       28w      279c http://192.168.189.35/.html_var_DE.php
403        9l       28w      279c http://192.168.189.35/.html_var_DE.asp
403        9l       28w      279c http://192.168.189.35/.html_var_DE.aspx
403        9l       28w      279c http://192.168.189.35/.html_var_DE.jsp
403        9l       28w      279c http://192.168.189.35/server-status
403        9l       28w      279c http://192.168.189.35/.htpasswd
403        9l       28w      279c http://192.168.189.35/.htpasswd.txt
403        9l       28w      279c http://192.168.189.35/.htpasswd.html
403        9l       28w      279c http://192.168.189.35/.htpasswd.php
403        9l       28w      279c http://192.168.189.35/.htpasswd.asp
403        9l       28w      279c http://192.168.189.35/.htpasswd.aspx
403        9l       28w      279c http://192.168.189.35/.htpasswd.jsp
403        9l       28w      279c http://192.168.189.35/.html.
403        9l       28w      279c http://192.168.189.35/.html..txt
403        9l       28w      279c http://192.168.189.35/.html..html
403        9l       28w      279c http://192.168.189.35/.html..php
403        9l       28w      279c http://192.168.189.35/.html..asp
403        9l       28w      279c http://192.168.189.35/.html..aspx
403        9l       28w      279c http://192.168.189.35/.html..jsp
403        9l       28w      279c http://192.168.189.35/.html.html.txt
403        9l       28w      279c http://192.168.189.35/.html.html.html
403        9l       28w      279c http://192.168.189.35/.html.html.php
403        9l       28w      279c http://192.168.189.35/.html.html.asp
403        9l       28w      279c http://192.168.189.35/.html.html.aspx
403        9l       28w      279c http://192.168.189.35/.html.html.jsp
403        9l       28w      279c http://192.168.189.35/.htpasswds
403        9l       28w      279c http://192.168.189.35/.htpasswds.txt
403        9l       28w      279c http://192.168.189.35/.htpasswds.html
403        9l       28w      279c http://192.168.189.35/.htpasswds.php
403        9l       28w      279c http://192.168.189.35/.htpasswds.asp
403        9l       28w      279c http://192.168.189.35/.htpasswds.aspx
403        9l       28w      279c http://192.168.189.35/.htpasswds.jsp
403        9l       28w      279c http://192.168.189.35/.htm.
403        9l       28w      279c http://192.168.189.35/.htm..txt
403        9l       28w      279c http://192.168.189.35/.htm..html
403        9l       28w      279c http://192.168.189.35/.htm..php
403        9l       28w      279c http://192.168.189.35/.htm..asp
403        9l       28w      279c http://192.168.189.35/.htm..aspx
403        9l       28w      279c http://192.168.189.35/.htm..jsp
403        9l       28w      279c http://192.168.189.35/.htmll
403        9l       28w      279c http://192.168.189.35/.htmll.txt
403        9l       28w      279c http://192.168.189.35/.htmll.html
403        9l       28w      279c http://192.168.189.35/.htmll.php
403        9l       28w      279c http://192.168.189.35/.phps
403        9l       28w      279c http://192.168.189.35/.htmll.asp
403        9l       28w      279c http://192.168.189.35/.htmll.aspx
403        9l       28w      279c http://192.168.189.35/.htmll.jsp
403        9l       28w      279c http://192.168.189.35/.html.old
403        9l       28w      279c http://192.168.189.35/.html.old.txt
403        9l       28w      279c http://192.168.189.35/.html.old.html
403        9l       28w      279c http://192.168.189.35/.html.old.php
403        9l       28w      279c http://192.168.189.35/.html.old.asp
403        9l       28w      279c http://192.168.189.35/.html.old.aspx
403        9l       28w      279c http://192.168.189.35/.html.old.jsp
403        9l       28w      279c http://192.168.189.35/.ht
403        9l       28w      279c http://192.168.189.35/.ht.txt
403        9l       28w      279c http://192.168.189.35/.html.bak
403        9l       28w      279c http://192.168.189.35/.ht.html
403        9l       28w      279c http://192.168.189.35/.html.bak.txt
403        9l       28w      279c http://192.168.189.35/.ht.php
403        9l       28w      279c http://192.168.189.35/.html.bak.html
403        9l       28w      279c http://192.168.189.35/.ht.asp
403        9l       28w      279c http://192.168.189.35/.html.bak.php
403        9l       28w      279c http://192.168.189.35/.ht.aspx
403        9l       28w      279c http://192.168.189.35/.html.bak.asp
403        9l       28w      279c http://192.168.189.35/.ht.jsp
403        9l       28w      279c http://192.168.189.35/.html.bak.aspx
403        9l       28w      279c http://192.168.189.35/.html.bak.jsp
403        9l       28w      279c http://192.168.189.35/.htm.htm
403        9l       28w      279c http://192.168.189.35/.htm.htm.txt
403        9l       28w      279c http://192.168.189.35/.htm.htm.html
403        9l       28w      279c http://192.168.189.35/.htm.htm.php
403        9l       28w      279c http://192.168.189.35/.htm.htm.asp
403        9l       28w      279c http://192.168.189.35/.htm.htm.aspx
403        9l       28w      279c http://192.168.189.35/.htm.htm.jsp
403        9l       28w      279c http://192.168.189.35/.hta
403        9l       28w      279c http://192.168.189.35/.hta.txt
403        9l       28w      279c http://192.168.189.35/.htgroup
403        9l       28w      279c http://192.168.189.35/.html1
403        9l       28w      279c http://192.168.189.35/.hta.html
403        9l       28w      279c http://192.168.189.35/.htgroup.txt
403        9l       28w      279c http://192.168.189.35/.html1.txt
403        9l       28w      279c http://192.168.189.35/.htgroup.html
403        9l       28w      279c http://192.168.189.35/.hta.php
403        9l       28w      279c http://192.168.189.35/.html1.html
403        9l       28w      279c http://192.168.189.35/.htgroup.php
403        9l       28w      279c http://192.168.189.35/.hta.asp
403        9l       28w      279c http://192.168.189.35/.html1.php
403        9l       28w      279c http://192.168.189.35/.hta.aspx
403        9l       28w      279c http://192.168.189.35/.htgroup.asp
403        9l       28w      279c http://192.168.189.35/.html1.asp
403        9l       28w      279c http://192.168.189.35/.hta.jsp
403        9l       28w      279c http://192.168.189.35/.htgroup.aspx
403        9l       28w      279c http://192.168.189.35/.html1.aspx
403        9l       28w      279c http://192.168.189.35/.htgroup.jsp
403        9l       28w      279c http://192.168.189.35/.html1.jsp
403        9l       28w      279c http://192.168.189.35/.html.LCK
403        9l       28w      279c http://192.168.189.35/.html.LCK.txt
403        9l       28w      279c http://192.168.189.35/.html.printable
403        9l       28w      279c http://192.168.189.35/.html.LCK.html
403        9l       28w      279c http://192.168.189.35/.html.printable.txt
403        9l       28w      279c http://192.168.189.35/.html.LCK.php
403        9l       28w      279c http://192.168.189.35/.html.printable.html
403        9l       28w      279c http://192.168.189.35/.html.LCK.asp
403        9l       28w      279c http://192.168.189.35/.html.printable.php
403        9l       28w      279c http://192.168.189.35/.html.LCK.aspx
403        9l       28w      279c http://192.168.189.35/.html.printable.asp
403        9l       28w      279c http://192.168.189.35/.html.printable.aspx
403        9l       28w      279c http://192.168.189.35/.html.LCK.jsp
403        9l       28w      279c http://192.168.189.35/.html.printable.jsp
403        9l       28w      279c http://192.168.189.35/.htm.LCK
403        9l       28w      279c http://192.168.189.35/.htm.LCK.txt
403        9l       28w      279c http://192.168.189.35/.htm.LCK.html
403        9l       28w      279c http://192.168.189.35/.htm.LCK.php
403        9l       28w      279c http://192.168.189.35/.htm.LCK.asp
403        9l       28w      279c http://192.168.189.35/.htm.LCK.aspx
403        9l       28w      279c http://192.168.189.35/.htm.LCK.jsp
403        9l       28w      279c http://192.168.189.35/.htaccess.bak
403        9l       28w      279c http://192.168.189.35/.htaccess.bak.txt
403        9l       28w      279c http://192.168.189.35/.htmls
403        9l       28w      279c http://192.168.189.35/.html.php.txt
403        9l       28w      279c http://192.168.189.35/.htx
403        9l       28w      279c http://192.168.189.35/.htaccess.bak.html
403        9l       28w      279c http://192.168.189.35/.htmls.txt
403        9l       28w      279c http://192.168.189.35/.html.php.html
403        9l       28w      279c http://192.168.189.35/.htx.txt
403        9l       28w      279c http://192.168.189.35/.htaccess.bak.php
403        9l       28w      279c http://192.168.189.35/.htmls.html
403        9l       28w      279c http://192.168.189.35/.html.php.php
403        9l       28w      279c http://192.168.189.35/.htaccess.bak.asp
403        9l       28w      279c http://192.168.189.35/.htx.html
403        9l       28w      279c http://192.168.189.35/.htmls.php
403        9l       28w      279c http://192.168.189.35/.html.php.asp
403        9l       28w      279c http://192.168.189.35/.htx.php
403        9l       28w      279c http://192.168.189.35/.htaccess.bak.aspx
403        9l       28w      279c http://192.168.189.35/.htmls.asp
403        9l       28w      279c http://192.168.189.35/.html.php.aspx
403        9l       28w      279c http://192.168.189.35/.htx.asp
403        9l       28w      279c http://192.168.189.35/.htaccess.bak.jsp
403        9l       28w      279c http://192.168.189.35/.htmls.aspx
403        9l       28w      279c http://192.168.189.35/.html.php.jsp
403        9l       28w      279c http://192.168.189.35/.htx.aspx
403        9l       28w      279c http://192.168.189.35/.htmls.jsp
403        9l       28w      279c http://192.168.189.35/.htx.jsp
403        9l       28w      279c http://192.168.189.35/.htlm
403        9l       28w      279c http://192.168.189.35/.htm2
403        9l       28w      279c http://192.168.189.35/.html-
403        9l       28w      279c http://192.168.189.35/.htuser
403        9l       28w      279c http://192.168.189.35/.htlm.txt
403        9l       28w      279c http://192.168.189.35/.htm2.txt
403        9l       28w      279c http://192.168.189.35/.html-.txt
403        9l       28w      279c http://192.168.189.35/.htlm.html
403        9l       28w      279c http://192.168.189.35/.htuser.txt
403        9l       28w      279c http://192.168.189.35/.html-.html
403        9l       28w      279c http://192.168.189.35/.htm2.html
403        9l       28w      279c http://192.168.189.35/.htlm.php
403        9l       28w      279c http://192.168.189.35/.htuser.html
403        9l       28w      279c http://192.168.189.35/.html-.php
403        9l       28w      279c http://192.168.189.35/.htm2.php
403        9l       28w      279c http://192.168.189.35/.htlm.asp
403        9l       28w      279c http://192.168.189.35/.htuser.php
403        9l       28w      279c http://192.168.189.35/.html-.asp
403        9l       28w      279c http://192.168.189.35/.htm2.asp
403        9l       28w      279c http://192.168.189.35/.htlm.aspx
403        9l       28w      279c http://192.168.189.35/.htuser.asp
403        9l       28w      279c http://192.168.189.35/.htm2.aspx
403        9l       28w      279c http://192.168.189.35/.html-.aspx
403        9l       28w      279c http://192.168.189.35/.htlm.jsp
403        9l       28w      279c http://192.168.189.35/.htuser.aspx
403        9l       28w      279c http://192.168.189.35/.html-.jsp
403        9l       28w      279c http://192.168.189.35/.htm2.jsp
403        9l       28w      279c http://192.168.189.35/.htuser.jsp
403        9l       28w      279c http://192.168.189.35/.htacess
403        9l       28w      279c http://192.168.189.35/.htm.d
403        9l       28w      279c http://192.168.189.35/.htacess.txt
403        9l       28w      279c http://192.168.189.35/.htm.d.txt
403        9l       28w      279c http://192.168.189.35/.htacess.html
403        9l       28w      279c http://192.168.189.35/.htm.d.html
403        9l       28w      279c http://192.168.189.35/.htm.old
403        9l       28w      279c http://192.168.189.35/.htacess.php
403        9l       28w      279c http://192.168.189.35/.htm.html.txt
403        9l       28w      279c http://192.168.189.35/.html-1
403        9l       28w      279c http://192.168.189.35/.htm.old.txt
403        9l       28w      279c http://192.168.189.35/.htm.d.php
403        9l       28w      279c http://192.168.189.35/.htacess.asp
403        9l       28w      279c http://192.168.189.35/.htm.html.html
403        9l       28w      279c http://192.168.189.35/.htm.d.asp
403        9l       28w      279c http://192.168.189.35/.html-1.txt
403        9l       28w      279c http://192.168.189.35/.htm.old.html
403        9l       28w      279c http://192.168.189.35/.html.orig
403        9l       28w      279c http://192.168.189.35/.htacess.aspx
403        9l       28w      279c http://192.168.189.35/.html.sav
403        9l       28w      279c http://192.168.189.35/.htm.html.php
403        9l       28w      279c http://192.168.189.35/.html_
403        9l       28w      279c http://192.168.189.35/.htm.d.aspx
403        9l       28w      279c http://192.168.189.35/.html-1.html
403        9l       28w      279c http://192.168.189.35/.htm.old.php
403        9l       28w      279c http://192.168.189.35/.html.orig.txt
403        9l       28w      279c http://192.168.189.35/.html_files
403        9l       28w      279c http://192.168.189.35/.htacess.jsp
403        9l       28w      279c http://192.168.189.35/.htmlpar
403        9l       28w      279c http://192.168.189.35/.html.sav.txt
403        9l       28w      279c http://192.168.189.35/.htm.html.asp
403        9l       28w      279c http://192.168.189.35/.html_.txt
403        9l       28w      279c http://192.168.189.35/.htm.d.jsp
403        9l       28w      279c http://192.168.189.35/.html.orig.html
403        9l       28w      279c http://192.168.189.35/.html-1.php
403        9l       28w      279c http://192.168.189.35/.htm.old.asp
403        9l       28w      279c http://192.168.189.35/.html_files.txt
403        9l       28w      279c http://192.168.189.35/.html.sav.html
403        9l       28w      279c http://192.168.189.35/.htmlprint
403        9l       28w      279c http://192.168.189.35/.htmlpar.txt
403        9l       28w      279c http://192.168.189.35/.htm.html.aspx
403        9l       28w      279c http://192.168.189.35/.html_.html
403        9l       28w      279c http://192.168.189.35/.hts
403        9l       28w      279c http://192.168.189.35/.html-1.asp
403        9l       28w      279c http://192.168.189.35/.html.orig.php
403        9l       28w      279c http://192.168.189.35/.html_files.html
403        9l       28w      279c http://192.168.189.35/.htm.old.aspx
403        9l       28w      279c http://192.168.189.35/.html.sav.php
403        9l       28w      279c http://192.168.189.35/.htmlprint.txt
403        9l       28w      279c http://192.168.189.35/.htm.html.jsp
403        9l       28w      279c http://192.168.189.35/.htmlpar.html
403        9l       28w      279c http://192.168.189.35/.html_.php
403        9l       28w      279c http://192.168.189.35/.hts.txt
403        9l       28w      279c http://192.168.189.35/.html-1.aspx
403        9l       28w      279c http://192.168.189.35/.htmlprint.html
403        9l       28w      279c http://192.168.189.35/.htm.old.jsp
403        9l       28w      279c http://192.168.189.35/.html_files.php
403        9l       28w      279c http://192.168.189.35/.html.orig.asp
403        9l       28w      279c http://192.168.189.35/.htmlpar.php
403        9l       28w      279c http://192.168.189.35/.html.sav.asp
403        9l       28w      279c http://192.168.189.35/.html_.asp
403        9l       28w      279c http://192.168.189.35/.hts.html
403        9l       28w      279c http://192.168.189.35/.htmlprint.php
403        9l       28w      279c http://192.168.189.35/.html-1.jsp
403        9l       28w      279c http://192.168.189.35/.html.orig.aspx
403        9l       28w      279c http://192.168.189.35/.html_files.asp
403        9l       28w      279c http://192.168.189.35/.html.sav.aspx
403        9l       28w      279c http://192.168.189.35/.htmlpar.asp
403        9l       28w      279c http://192.168.189.35/.html_.aspx
403        9l       28w      279c http://192.168.189.35/.hts.php
403        9l       28w      279c http://192.168.189.35/.htmlpar.aspx
403        9l       28w      279c http://192.168.189.35/.html_files.aspx
403        9l       28w      279c http://192.168.189.35/.htmlprint.asp
403        9l       28w      279c http://192.168.189.35/.html.sav.jsp
403        9l       28w      279c http://192.168.189.35/.html.orig.jsp
403        9l       28w      279c http://192.168.189.35/.html_.jsp
403        9l       28w      279c http://192.168.189.35/.hts.asp
403        9l       28w      279c http://192.168.189.35/.htmlpar.jsp
403        9l       28w      279c http://192.168.189.35/.htmlprint.aspx
403        9l       28w      279c http://192.168.189.35/.html_files.jsp
403        9l       28w      279c http://192.168.189.35/.hts.aspx
403        9l       28w      279c http://192.168.189.35/.htmlprint.jsp
403        9l       28w      279c http://192.168.189.35/.hts.jsp

```
