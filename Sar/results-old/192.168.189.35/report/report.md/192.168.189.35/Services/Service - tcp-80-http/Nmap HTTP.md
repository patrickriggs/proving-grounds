```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.189.35
```

[/home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.91 scan initiated Thu Oct 21 14:27:28 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Sar/results/192.168.189.35/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.189.35
Nmap scan report for 192.168.189.35
Host is up, received user-set (0.098s latency).
Scanned at 2021-10-21 14:27:29 EDT for 34s

PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.29 ((Ubuntu))
|_http-chrono: Request times for /; avg: 339.16ms; min: 267.59ms; max: 384.40ms
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.189.35
|     
|     Path: http://192.168.189.35:80/
|     Line number: 4
|     Comment: 
|         <!--
|             Modified from the Debian original for Ubuntu
|             Last updated: 2016-11-16
|             See: https://launchpad.net/bugs/1288690
|           -->
|     
|     Path: http://192.168.189.35:80/
|     Line number: 201
|     Comment: 
|         <!--      <div class="table_of_contents floating_element">
|                 <div class="section_header section_header_grey">
|                   TABLE OF CONTENTS
|                 </div>
|                 <div class="table_of_contents_item floating_element">
|                   <a href="#about">About</a>
|                 </div>
|                 <div class="table_of_contents_item floating_element">
|                   <a href="#changes">Changes</a>
|                 </div>
|                 <div class="table_of_contents_item floating_element">
|                   <a href="#scope">Scope</a>
|                 </div>
|                 <div class="table_of_contents_item floating_element">
|                   <a href="#files">Config files</a>
|                 </div>
|               </div>
|_        -->
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-date: Thu, 21 Oct 2021 18:27:38 GMT; -1s from local time.
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
| http-enum: 
|   /robots.txt: Robots file
|_  /phpinfo.php: Possible information file
| http-errors: 
| Spidering limited to: maxpagecount=40; withinhost=192.168.189.35
|   Found the following error pages: 
|   
|   Error Code: 404
|_  	http://192.168.189.35:80/manual
|_http-feed: Couldn't find any feeds.
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-grep: 
|   (1) http://192.168.189.35:80/manual: 
|     (1) ip: 
|_      + 192.168.189.35
| http-headers: 
|   Date: Thu, 21 Oct 2021 18:27:37 GMT
|   Server: Apache/2.4.29 (Ubuntu)
|   Last-Modified: Sun, 20 Oct 2019 15:04:12 GMT
|   ETag: "2aa6-59558e1434548"
|   Accept-Ranges: bytes
|   Content-Length: 10918
|   Vary: Accept-Encoding
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-litespeed-sourcecode-download: Request with null byte did not work. This web server might not be vulnerable
|_http-malware-host: Host appears to be clean
| http-methods: 
|_  Supported Methods: OPTIONS HEAD GET POST
|_http-mobileversion-checker: No mobile version detected.
| http-php-version: Logo query returned unknown hash 3526531ccd6c6a1d2340574a305a18f8
|_Credits query returned unknown hash 3526531ccd6c6a1d2340574a305a18f8
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-security-headers: 
|_http-server-header: Apache/2.4.29 (Ubuntu)
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1
|     /icons/
|       png: 1
|   Longest directory structure:
|     Depth: 1
|     Dir: /icons/
|   Total files found (by extension):
|_    Other: 1; png: 1
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-title: Apache2 Ubuntu Default Page: It works
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34
| http-vhosts: 
|_128 names had status 200
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Oct 21 14:28:03 2021 -- 1 IP address (1 host up) scanned in 34.91 seconds

```
