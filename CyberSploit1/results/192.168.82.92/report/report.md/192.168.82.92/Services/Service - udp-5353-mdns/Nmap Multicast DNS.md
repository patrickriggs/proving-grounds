```bash
nmap -vv --reason -Pn -T4 -sU -sV -p 5353 --script="banner,(dns* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/udp5353/udp_5353_multicastdns_nmap.txt" -oX "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/udp5353/xml/udp_5353_multicastdns_nmap.xml" 192.168.82.92
```

[/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/udp5353/udp_5353_multicastdns_nmap.txt](file:///home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/udp5353/udp_5353_multicastdns_nmap.txt):

```
# Nmap 7.91 scan initiated Tue Oct 26 00:34:05 2021 as: nmap -vv --reason -Pn -T4 -sU -sV -p 5353 "--script=banner,(dns* or ssl*) and not (brute or broadcast or dos or external or fuzzer)" -oN /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/udp5353/udp_5353_multicastdns_nmap.txt -oX /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/udp5353/xml/udp_5353_multicastdns_nmap.xml 192.168.82.92
Nmap scan report for 192.168.82.92
Host is up, received user-set.
Scanned at 2021-10-26 00:34:05 EDT for 7s

PORT     STATE SERVICE REASON       VERSION
5353/udp open  mdns    udp-response DNS-based service discovery
| dns-service-discovery: 
|   9/tcp workstation
|     Address=192.168.82.92 fe80::250:56ff:febf:c282
|   22/tcp udisks-ssh
|_    Address=192.168.82.92 fe80::250:56ff:febf:c282

Host script results:
|_dns-brute: Can't guess domain of "192.168.82.92"; use dns-brute.domain script argument.

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Oct 26 00:34:12 2021 -- 1 IP address (1 host up) scanned in 6.78 seconds

```
