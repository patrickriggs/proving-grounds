```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.82.92
```

[/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.91 scan initiated Tue Oct 26 00:31:10 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.82.92
Nmap scan report for 192.168.82.92
Host is up, received user-set (0.099s latency).
Scanned at 2021-10-26 00:31:10 EDT for 29s

PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.2.22 ((Ubuntu))
|_http-apache-negotiation: mod_negotiation enabled.
|_http-chrono: Request times for /; avg: 285.53ms; min: 233.03ms; max: 363.30ms
| http-comments-displayer: 
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.82.92
|     
|     Path: http://192.168.82.92:80/
|     Line number: 8
|     Comment: 
|         <!-- Bootstrap CSS -->
|     
|     Path: http://192.168.82.92:80/
|     Line number: 35
|     Comment: 
|         <!-- jQuery first, then Popper.js, then Bootstrap JS -->
|     
|     Path: http://192.168.82.92:80/
|     Line number: 34
|     Comment: 
|         <!-- Optional JavaScript -->
|     
|     Path: http://192.168.82.92:80/
|     Line number: 48
|     Comment: 
|         <!-------------username:itsskv--------------------->
|     
|     Path: http://192.168.82.92:80/
|     Line number: 4
|     Comment: 
|_        <!-- Required meta tags -->
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-date: Tue, 26 Oct 2021 04:31:20 GMT; +2s from local time.
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
| http-enum: 
|_  /robots.txt: Robots file
|_http-errors: Couldn't find any error pages.
|_http-feed: Couldn't find any feeds.
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-grep: 
|   (1) http://192.168.82.92:80/: 
|     (1) email: 
|_      + popper.js@1.16
| http-headers: 
|   Date: Tue, 26 Oct 2021 04:31:22 GMT
|   Server: Apache/2.2.22 (Ubuntu)
|   Last-Modified: Sat, 27 Jun 2020 04:46:41 GMT
|   ETag: "256ef-91d-5a909830c2d14"
|   Accept-Ranges: bytes
|   Content-Length: 2333
|   Vary: Accept-Encoding
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-malware-host: Host appears to be clean
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-mobileversion-checker: No mobile version detected.
| http-php-version: Logo query returned unknown hash 09ff34a2637732f00cbf489be5aee058
|_Credits query returned unknown hash 09ff34a2637732f00cbf489be5aee058
| http-referer-checker: 
| Spidering limited to: maxpagecount=30
|   https://stackpath.bootstrapcdn.com:443/bootstrap/4.5.0/js/bootstrap.min.js
|   https://cdn.jsdelivr.net:443/npm/popper.js@1.16.0/dist/umd/popper.min.js
|_  https://code.jquery.com:443/jquery-3.5.1.slim.min.js
|_http-security-headers: 
|_http-server-header: Apache/2.2.22 (Ubuntu)
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1; gif: 1
|   Longest directory structure:
|     Depth: 0
|     Dir: /
|   Total files found (by extension):
|_    Other: 1; gif: 1
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-title: Hello Pentester!
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34
| http-vhosts: 
|_128 names had status 200
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Oct 26 00:31:39 2021 -- 1 IP address (1 host up) scanned in 29.41 seconds

```
