```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.82.92:80 2>&1
```

[/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/tcp_80_http_whatweb.txt](file:///home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/tcp80/tcp_80_http_whatweb.txt):

```
WhatWeb report for http://192.168.82.92:80
Status    : 200 OK
Title     : Hello Pentester!
IP        : 192.168.82.92
Country   : RESERVED, ZZ

Summary   : Apache[2.2.22], Bootstrap[4.5.0], JQuery, HTML5, Script, HTTPServer[Ubuntu Linux][Apache/2.2.22 (Ubuntu)]

Detected Plugins:
[ Apache ]
	The Apache HTTP Server Project is an effort to develop and
	maintain an open-source HTTP server for modern operating
	systems including UNIX and Windows NT. The goal of this
	project is to provide a secure, efficient and extensible
	server that provides HTTP services in sync with the current
	HTTP standards.

	Version      : 2.2.22 (from HTTP Server Header)
	Google Dorks: (3)
	Website     : http://httpd.apache.org/

[ Bootstrap ]
	Bootstrap is an open source toolkit for developing with
	HTML, CSS, and JS.

	Version      : 4.5.0
	Version      : 4.5.0
	Website     : https://getbootstrap.com/

[ HTML5 ]
	HTML version 5, detected by the doctype declaration


[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Ubuntu Linux
	String       : Apache/2.2.22 (Ubuntu) (from server string)

[ JQuery ]
	A fast, concise, JavaScript that simplifies how to traverse
	HTML documents, handle events, perform animations, and add
	AJAX.

	Website     : http://jquery.com/

[ Script ]
	This plugin detects instances of script HTML elements and
	returns the script language/type.


HTTP Headers:
	HTTP/1.1 200 OK
	Date: Tue, 26 Oct 2021 04:31:13 GMT
	Server: Apache/2.2.22 (Ubuntu)
	Last-Modified: Sat, 27 Jun 2020 04:46:41 GMT
	ETag: "256ef-91d-5a909830c2d14"
	Accept-Ranges: bytes
	Vary: Accept-Encoding
	Content-Encoding: gzip
	Content-Length: 1087
	Connection: close
	Content-Type: text/html



```
