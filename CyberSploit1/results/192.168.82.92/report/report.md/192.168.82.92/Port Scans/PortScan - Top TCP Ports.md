```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_quick_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/xml/_quick_tcp_nmap.xml" 192.168.82.92
```

[/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_quick_tcp_nmap.txt](file:///home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_quick_tcp_nmap.txt):

```
# Nmap 7.91 scan initiated Tue Oct 26 00:30:38 2021 as: nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_quick_tcp_nmap.txt -oX /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/xml/_quick_tcp_nmap.xml 192.168.82.92
Increasing send delay for 192.168.82.92 from 0 to 5 due to 279 out of 697 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -420109 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -420109 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -419692 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -419692 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -468347 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -468347 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -235351 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -235351 microseconds.  Ignoring time.
Nmap scan report for 192.168.82.92
Host is up, received user-set (0.098s latency).
Scanned at 2021-10-26 00:30:38 EDT for 31s
Not shown: 998 closed ports
Reason: 998 resets
PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 5.9p1 Debian 5ubuntu1.10 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 01:1b:c8:fe:18:71:28:60:84:6a:9f:30:35:11:66:3d (DSA)
| ssh-dss AAAAB3NzaC1kc3MAAACBAIvXzKChMFQjoRVJPY3oKyzdX27i0MDEbmLG3yRuSLiPgYBF4jGq+7mn848WJSbLPpNAa/6xMgQb5BhhSTHgA77kg1gS8IhXvpoMlixJoJmGVBAqobxKAEbZnfbSKfjtJk7jI9mfoZjmOhznnzEwsODjzEDqtBYGEo4Mf/9KUX/jAAAAFQCdv46IJ36Dkyv7av5KP+Ghs7TzSwAAAIAVxh4PVUljX8ECckYq40LJ/jRL4qWhLSctMRK9J34+WSe2RHpRKRB+0eTpjffzNktRgFgKJJwW+3kd4HzOROMDvLEuhdrALiiNwqxYzIv70i+mwxNWoghoUcslgXOmeTAvyiW/jNU/Uav39nutehkX62PfvTRrulRzlbayMbkU4AAAAIABwdKXqzEKdPr7L+bCBLEe06k3Vd2bWvTOD3wwGzz+rzvmcexiPvgc1xRYE6Fno0QG2yfow9cvgrajyiDoMdUVogH7N8hm3+KbaKnO8mA7jkxVMACpfanwHRVJfM/+PHPOvML2v8QJ7JYGRgwlTyI5UxqUw9YuJSNJWThRWyw49A==
|   2048 d9:53:14:a3:7f:99:51:40:3f:49:ef:ef:7f:8b:35:de (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDAgVBhkY/5TpbZpI7WmUiKX7koUuK6+K+usitE5rg6V326mmdJKt69IFmq4gcgpqXuImopLdGczY/8ulNoEj3aaPckhAVG5CLmlGMvRR5h2AW6pI7pUI9NkyAtLkm0fkyLDKLvKS32KSQ9jSdVPeXeCE0EpGJW5J5QOMWxEbS4z3XnLlkLqGz/wPRCwupYjJ+UsAgHfJVDKC7foPZj1ft/XX9oqcNkcykxz3AQtn0sEEZ8MfuWyePiVgYmsDLl0tBGdm0p9GExfWE0KAhpScWaxJzKmSFfzAjVpg60SyegBAhcIs0xMS18cBAS05OHNLKmMCfzOqm+8AjbVAyl+RF3
|   256 ef:43:5b:d0:c0:eb:ee:3e:76:61:5c:6d:ce:15:fe:7e (ECDSA)
|_ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPHaPjoo6jLLN7KcEqjZCEXgAdRiejIMlLihehQ7+dmmxs4SqtjA8I8EjiqZpVL6kgSmDX5BpNxmyHjWJRHIC9U=
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.2.22 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.2.22 (Ubuntu)
|_http-title: Hello Pentester!
Aggressive OS guesses: Linux 3.11 - 4.1 (94%), Linux 4.4 (94%), Linux 3.16 (93%), Linux 3.10 - 3.16 (92%), Linux 3.13 (91%), Linux 2.6.32 (91%), Linux 3.2 - 3.8 (91%), Linux 3.8 (91%), Kyocera CopyStar CS-2560 printer (90%), IPFire 2.11 firewall (Linux 2.6.32) (90%)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.91%E=4%D=10/26%OT=22%CT=1%CU=33690%PV=Y%DS=2%DC=T%G=Y%TM=617784
OS:8D%P=x86_64-pc-linux-gnu)SEQ(SP=108%GCD=1%ISR=10B%TI=Z%TS=8)SEQ(SP=108%G
OS:CD=1%ISR=10B%TI=Z%II=I%TS=8)OPS(O1=M506ST11NW7%O2=M506ST11NW7%O3=M506NNT
OS:11NW7%O4=M506ST11NW7%O5=M506ST11NW7%O6=M506ST11)WIN(W1=7120%W2=7120%W3=7
OS:120%W4=7120%W5=7120%W6=7120)ECN(R=Y%DF=Y%T=40%W=7210%O=M506NNSNW7%CC=Y%Q
OS:=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=N)T5(R=Y%DF=
OS:Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=N)T7(R=N)U1(R=Y%DF=N%T=40%IPL=1
OS:64%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)U1(R=N)IE(R=Y%DFI=N%T=40%CD=S)

Uptime guess: 0.003 days (since Tue Oct 26 00:27:22 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=264 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 23/tcp)
HOP RTT       ADDRESS
1   101.28 ms 192.168.49.1
2   101.76 ms 192.168.82.92

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Oct 26 00:31:09 2021 -- 1 IP address (1 host up) scanned in 31.88 seconds

```
