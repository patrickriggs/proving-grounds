```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/xml/_top_100_udp_nmap.xml" 192.168.82.92
```

[/home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.91 scan initiated Tue Oct 26 00:30:38 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/CyberSploit1/results/192.168.82.92/scans/xml/_top_100_udp_nmap.xml 192.168.82.92
Warning: 192.168.82.92 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.82.92 from 100 to 200 due to 11 out of 12 dropped probes since last increase.
Increasing send delay for 192.168.82.92 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.82.92 from 400 to 800 due to 11 out of 11 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -67479 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -208958 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -208958 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -205756 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -205756 microseconds.  Ignoring time.
Nmap scan report for 192.168.82.92
Host is up, received user-set (0.099s latency).
Scanned at 2021-10-26 00:30:38 EDT for 207s
Not shown: 83 closed ports
Reason: 83 port-unreaches
PORT      STATE         SERVICE        REASON       VERSION
68/udp    open|filtered dhcpc          no-response
69/udp    open|filtered tftp           no-response
137/udp   open|filtered netbios-ns     no-response
162/udp   open|filtered snmptrap       no-response
515/udp   open|filtered printer        no-response
999/udp   open|filtered applix         no-response
1719/udp  open|filtered h323gatestat   no-response
1900/udp  open|filtered upnp           no-response
5353/udp  open          mdns           udp-response DNS-based service discovery
| dns-service-discovery: 
|   9/tcp workstation
|     Address=192.168.82.92 fe80::250:56ff:febf:c282
|   22/tcp udisks-ssh
|_    Address=192.168.82.92 fe80::250:56ff:febf:c282
32771/udp open|filtered sometimes-rpc6 no-response
49153/udp open|filtered unknown        no-response
49154/udp open|filtered unknown        no-response
49156/udp open|filtered unknown        no-response
49185/udp open|filtered unknown        no-response
49194/udp open|filtered unknown        no-response
49200/udp open|filtered unknown        no-response
65024/udp open|filtered unknown        no-response
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.91%E=4%D=10/26%OT=%CT=%CU=7%PV=Y%DS=2%DC=T%G=N%TM=6177853D%P=x86_64-pc-linux-gnu)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 20031/udp)
HOP RTT       ADDRESS
1   97.35 ms  192.168.49.1
2   109.42 ms 192.168.82.92

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Tue Oct 26 00:34:05 2021 -- 1 IP address (1 host up) scanned in 207.43 seconds

```
