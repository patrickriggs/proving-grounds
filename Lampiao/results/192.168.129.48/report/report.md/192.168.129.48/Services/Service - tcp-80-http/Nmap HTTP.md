```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.129.48
```

[/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Nov  6 07:23:00 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.129.48
Nmap scan report for 192.168.129.48
Host is up, received user-set (0.098s latency).
Scanned at 2021-11-06 07:23:01 EDT for 0s

PORT   STATE  SERVICE REASON       VERSION
80/tcp closed http    reset ttl 63

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Nov  6 07:23:01 2021 -- 1 IP address (1 host up) scanned in 0.56 seconds

```
