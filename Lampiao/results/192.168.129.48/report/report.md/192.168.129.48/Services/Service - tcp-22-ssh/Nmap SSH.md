```bash
nmap -vv --reason -Pn -T4 -sV -p 22 --script="banner,ssh2-enum-algos,ssh-hostkey,ssh-auth-methods" -oN "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp22/tcp_22_ssh_nmap.txt" -oX "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp22/xml/tcp_22_ssh_nmap.xml" 192.168.129.48
```

[/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp22/tcp_22_ssh_nmap.txt](file:///home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp22/tcp_22_ssh_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Nov  6 07:23:00 2021 as: nmap -vv --reason -Pn -T4 -sV -p 22 --script=banner,ssh2-enum-algos,ssh-hostkey,ssh-auth-methods -oN /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp22/tcp_22_ssh_nmap.txt -oX /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp22/xml/tcp_22_ssh_nmap.xml 192.168.129.48
Nmap scan report for 192.168.129.48
Host is up, received user-set (0.098s latency).
Scanned at 2021-11-06 07:23:00 EDT for 4s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
|_banner: SSH-2.0-OpenSSH_6.6.1p1 Ubuntu-2ubuntu2.13
| ssh-auth-methods: 
|   Supported authentication methods: 
|     publickey
|_    password
| ssh2-enum-algos: 
|   kex_algorithms: (8)
|       curve25519-sha256@libssh.org
|       ecdh-sha2-nistp256
|       ecdh-sha2-nistp384
|       ecdh-sha2-nistp521
|       diffie-hellman-group-exchange-sha256
|       diffie-hellman-group-exchange-sha1
|       diffie-hellman-group14-sha1
|       diffie-hellman-group1-sha1
|   server_host_key_algorithms: (4)
|       ssh-rsa
|       ssh-dss
|       ecdsa-sha2-nistp256
|       ssh-ed25519
|   encryption_algorithms: (16)
|       aes128-ctr
|       aes192-ctr
|       aes256-ctr
|       arcfour256
|       arcfour128
|       aes128-gcm@openssh.com
|       aes256-gcm@openssh.com
|       chacha20-poly1305@openssh.com
|       aes128-cbc
|       3des-cbc
|       blowfish-cbc
|       cast128-cbc
|       aes192-cbc
|       aes256-cbc
|       arcfour
|       rijndael-cbc@lysator.liu.se
|   mac_algorithms: (19)
|       hmac-md5-etm@openssh.com
|       hmac-sha1-etm@openssh.com
|       umac-64-etm@openssh.com
|       umac-128-etm@openssh.com
|       hmac-sha2-256-etm@openssh.com
|       hmac-sha2-512-etm@openssh.com
|       hmac-ripemd160-etm@openssh.com
|       hmac-sha1-96-etm@openssh.com
|       hmac-md5-96-etm@openssh.com
|       hmac-md5
|       hmac-sha1
|       umac-64@openssh.com
|       umac-128@openssh.com
|       hmac-sha2-256
|       hmac-sha2-512
|       hmac-ripemd160
|       hmac-ripemd160@openssh.com
|       hmac-sha1-96
|       hmac-md5-96
|   compression_algorithms: (2)
|       none
|_      zlib@openssh.com
| ssh-hostkey: 
|   1024 46:b1:99:60:7d:81:69:3c:ae:1f:c7:ff:c3:66:e3:10 (DSA)
| ssh-dss AAAAB3NzaC1kc3MAAACBAKeg3YDejlMII2nywaeS2HFxd09ak99X7NdFEfHDe/Fng3UwA+gQjhQZ03h09BWb45SfR2EIHLWQ4cN8NN+8bajVwsLwItjKNis+mVMI4Jd8HFMV064cuzcB+xbikI8jzV1GIN4Gclifo+luxym7exJvHgKcLpL1rNVZjzYxPhofAAAAFQCKP3vJ9wD7JSGsDao7IA97RPWROwAAAIAOFHw5FJFFG3bpKsmzhluq0dj1VdltQ51Wd3lqWFtoSncq14ZWMunQhHkKt+KLuPIccv1XmqJrbP9HEWe2E8hl4oT3R7vzbEB/nvVILX3y68TR2/o0Iu5JMgy4uyXMVFFbdpZ3cOv4+fDbn7Yy9shhE+T144Utr0WvHHGvcged4QAAAIEAmqW1JA1Dj7CjHW64mRG+7uDNvb8InZplGWMVd0JINWgr1is4gRDnwldXukIDSA71cTkS3Al6mMCu0nftLqxZKodcIeuGuKBWIHSTKN3/pzVrFjOiOfUQK7lH3pHzR6DxpOLOVLMsP4qOGa6CBG9R4UREUSFZ+j6mVSPgo+tU9do=
|   2048 f3:e8:88:f2:2d:d0:b2:54:0b:9c:ad:61:33:59:55:93 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCohkf0Lql5Q9/2RQx+I7+nJJ9hZfi+p0nYiwkia9NTSQlbQZ09JUGvfxRE3pYke/zu9TNCIcKCSdVuIg7VCIjvyyXlxIfhGm1KDIxa4yVSYY6nlp0PlNe/eMJu0eHmCul/RZR+QMml4Ov/DD7tBNARreXZtxgGG1cUp/51ad31VxOW0xZ8mteMAqyBYRmGPcE5EMFhB7iis8TGr5ZNvEq246RRG9yzDECYdOcGu0CaWdBn1CO9VKsr393RSEAY7dYDqDXssvA9Dw81Oqkek59OmLXBS0WFgnjxpfbmdfvbDsm9WQ2jTMgq6NTp6yYYlYoxxc4kkwJDgO0lD75gN6+Z
|   256 ce:63:2a:f7:53:6e:46:e2:ae:81:e3:ff:b7:16:f4:52 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJgCFIaCKti2RYMo5AGFAE91s78Z0eBZp4I+MlPV2Sw9oTZaTTbGBeLLKpsHHAs0mw1rUm36GxzU4F1oU57nBcE=
|   256 c6:55:ca:07:37:65:e3:06:c1:d6:5b:77:dc:23:df:cc (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAq63V1lqtuey7Q5i7rr9auAAqKBs27r5xq5k27l3XSb
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Nov  6 07:23:04 2021 -- 1 IP address (1 host up) scanned in 3.94 seconds

```
