```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.129.48:1898 2>&1
```

[/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp1898/tcp_1898_http_whatweb.txt](file:///home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/tcp1898/tcp_1898_http_whatweb.txt):

```
WhatWeb report for http://192.168.129.48:1898
Status    : 200 OK
Title     : Lampião
IP        : 192.168.129.48
Country   : RESERVED, ZZ

Summary   : Apache[2.4.7], JQuery, MetaGenerator[Drupal 7 (http://drupal.org)], Drupal, UncommonHeaders[x-content-type-options,x-generator], PHP[5.5.9-1ubuntu4.24], PasswordField[pass], X-Powered-By[PHP/5.5.9-1ubuntu4.24], Content-Language[en], Script[text/javascript], HTTPServer[Ubuntu Linux][Apache/2.4.7 (Ubuntu)], X-Frame-Options[SAMEORIGIN]

Detected Plugins:
[ Apache ]
	The Apache HTTP Server Project is an effort to develop and
	maintain an open-source HTTP server for modern operating
	systems including UNIX and Windows NT. The goal of this
	project is to provide a secure, efficient and extensible
	server that provides HTTP services in sync with the current
	HTTP standards.

	Version      : 2.4.7 (from HTTP Server Header)
	Google Dorks: (3)
	Website     : http://httpd.apache.org/

[ Content-Language ]
	Detect the content-language setting from the HTTP header.

	String       : en

[ Drupal ]
	Drupal is an opensource CMS written in PHP.

	Aggressive function available (check plugin file or details).
	Google Dorks: (1)
	Website     : http://www.drupal.org

[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Ubuntu Linux
	String       : Apache/2.4.7 (Ubuntu) (from server string)

[ JQuery ]
	A fast, concise, JavaScript that simplifies how to traverse
	HTML documents, handle events, perform animations, and add
	AJAX.

	Website     : http://jquery.com/

[ MetaGenerator ]
	This plugin identifies meta generator tags and extracts its
	value.

	String       : Drupal 7 (http://drupal.org)

[ PHP ]
	PHP is a widely-used general-purpose scripting language
	that is especially suited for Web development and can be
	embedded into HTML. This plugin identifies PHP errors,
	modules and versions and extracts the local file path and
	username if present.

	Version      : 5.5.9-1ubuntu4.24
	Google Dorks: (2)
	Website     : http://www.php.net/

[ PasswordField ]
	find password fields

	String       : pass (from field name)

[ Script ]
	This plugin detects instances of script HTML elements and
	returns the script language/type.

	String       : text/javascript

[ UncommonHeaders ]
	Uncommon HTTP server headers. The blacklist includes all
	the standard headers and many non standard but common ones.
	Interesting but fairly common headers should have their own
	plugins, eg. x-powered-by, server and x-aspnet-version.
	Info about headers can be found at www.http-stats.com

	String       : x-content-type-options,x-generator (from headers)

[ X-Frame-Options ]
	This plugin retrieves the X-Frame-Options value from the
	HTTP header. - More Info:
	http://msdn.microsoft.com/en-us/library/cc288472%28VS.85%29.
	aspx

	String       : SAMEORIGIN

[ X-Powered-By ]
	X-Powered-By HTTP header

	String       : PHP/5.5.9-1ubuntu4.24 (from x-powered-by string)

HTTP Headers:
	HTTP/1.1 200 OK
	Date: Sat, 06 Nov 2021 11:26:59 GMT
	Server: Apache/2.4.7 (Ubuntu)
	X-Powered-By: PHP/5.5.9-1ubuntu4.24
	Expires: Sun, 19 Nov 1978 05:00:00 GMT
	Cache-Control: no-cache, must-revalidate
	X-Content-Type-Options: nosniff
	Content-Language: en
	X-Frame-Options: SAMEORIGIN
	X-Generator: Drupal 7 (http://drupal.org)
	Vary: Accept-Encoding
	Content-Encoding: gzip
	Content-Length: 3269
	Connection: close
	Content-Type: text/html; charset=utf-8



```
