```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_quick_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/xml/_quick_tcp_nmap.xml" 192.168.129.48
```

[/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_quick_tcp_nmap.txt](file:///home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_quick_tcp_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Nov  6 07:21:43 2021 as: nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -oN /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_quick_tcp_nmap.txt -oX /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/xml/_quick_tcp_nmap.xml 192.168.129.48
Increasing send delay for 192.168.129.48 from 0 to 5 due to 133 out of 331 dropped probes since last increase.
Increasing send delay for 192.168.129.48 from 5 to 10 due to 11 out of 25 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -103720 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -103720 microseconds.  Ignoring time.
Nmap scan report for 192.168.129.48
Host is up, received user-set (0.099s latency).
Scanned at 2021-11-06 07:21:43 EDT for 77s
Not shown: 998 closed tcp ports (reset)
PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 46:b1:99:60:7d:81:69:3c:ae:1f:c7:ff:c3:66:e3:10 (DSA)
| ssh-dss AAAAB3NzaC1kc3MAAACBAKeg3YDejlMII2nywaeS2HFxd09ak99X7NdFEfHDe/Fng3UwA+gQjhQZ03h09BWb45SfR2EIHLWQ4cN8NN+8bajVwsLwItjKNis+mVMI4Jd8HFMV064cuzcB+xbikI8jzV1GIN4Gclifo+luxym7exJvHgKcLpL1rNVZjzYxPhofAAAAFQCKP3vJ9wD7JSGsDao7IA97RPWROwAAAIAOFHw5FJFFG3bpKsmzhluq0dj1VdltQ51Wd3lqWFtoSncq14ZWMunQhHkKt+KLuPIccv1XmqJrbP9HEWe2E8hl4oT3R7vzbEB/nvVILX3y68TR2/o0Iu5JMgy4uyXMVFFbdpZ3cOv4+fDbn7Yy9shhE+T144Utr0WvHHGvcged4QAAAIEAmqW1JA1Dj7CjHW64mRG+7uDNvb8InZplGWMVd0JINWgr1is4gRDnwldXukIDSA71cTkS3Al6mMCu0nftLqxZKodcIeuGuKBWIHSTKN3/pzVrFjOiOfUQK7lH3pHzR6DxpOLOVLMsP4qOGa6CBG9R4UREUSFZ+j6mVSPgo+tU9do=
|   2048 f3:e8:88:f2:2d:d0:b2:54:0b:9c:ad:61:33:59:55:93 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCohkf0Lql5Q9/2RQx+I7+nJJ9hZfi+p0nYiwkia9NTSQlbQZ09JUGvfxRE3pYke/zu9TNCIcKCSdVuIg7VCIjvyyXlxIfhGm1KDIxa4yVSYY6nlp0PlNe/eMJu0eHmCul/RZR+QMml4Ov/DD7tBNARreXZtxgGG1cUp/51ad31VxOW0xZ8mteMAqyBYRmGPcE5EMFhB7iis8TGr5ZNvEq246RRG9yzDECYdOcGu0CaWdBn1CO9VKsr393RSEAY7dYDqDXssvA9Dw81Oqkek59OmLXBS0WFgnjxpfbmdfvbDsm9WQ2jTMgq6NTp6yYYlYoxxc4kkwJDgO0lD75gN6+Z
|   256 ce:63:2a:f7:53:6e:46:e2:ae:81:e3:ff:b7:16:f4:52 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJgCFIaCKti2RYMo5AGFAE91s78Z0eBZp4I+MlPV2Sw9oTZaTTbGBeLLKpsHHAs0mw1rUm36GxzU4F1oU57nBcE=
|   256 c6:55:ca:07:37:65:e3:06:c1:d6:5b:77:dc:23:df:cc (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAq63V1lqtuey7Q5i7rr9auAAqKBs27r5xq5k27l3XSb
80/tcp open  http?   syn-ack ttl 63
| fingerprint-strings: 
|   NULL: 
|     _____ _ _ 
|     |_|/ ___ ___ __ _ ___ _ _ 
|     \x20| __/ (_| __ \x20|_| |_ 
|     ___/ __| |___/ ___|__,_|___/__, ( ) 
|     |___/ 
|     ______ _ _ _ 
|     ___(_) | | | |
|     \x20/ _` | / _ / _` | | | |/ _` | |
|_    __,_|__,_|_| |_|
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port80-TCP:V=7.92%I=9%D=11/6%Time=61866553%P=x86_64-pc-linux-gnu%r(NULL
SF:,1179,"\x20_____\x20_\x20\x20\x20_\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\|_\x20\x20\x20_\|\x20\|\x20\(\x
SF:20\)\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\n\x20\x20\|\x20\|\x20\|\x20\|_\|/\x20___\x20\x20\x20\x20___\x20\x20
SF:__\x20_\x20___\x20_\x20\x20\x20_\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n
SF:\x20\x20\|\x20\|\x20\|\x20__\|\x20/\x20__\|\x20\x20/\x20_\x20\\/\x20_`\
SF:x20/\x20__\|\x20\|\x20\|\x20\|\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20_\
SF:|\x20\|_\|\x20\|_\x20\x20\\__\x20\\\x20\|\x20\x20__/\x20\(_\|\x20\\__\x
SF:20\\\x20\|_\|\x20\|_\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2
SF:0\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20\\___/\x20\\__\|
SF:\x20\|___/\x20\x20\\___\|\\__,_\|___/\\__,\x20\(\x20\)\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\n\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20__/\x20\|/\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\n\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\|___/\x20\x20\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\n______\x20_\x20\x20\x20\x20\x20\x20\x20_\x20\x20\x20\x
SF:20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20_\x20\n\|\x20\x20___\(_\)\x20\x20\x
SF:20\x20\x20\|\x20\|\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\
SF:x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20
SF:\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\|\x20\|\n\
SF:|\x20\|_\x20\x20\x20_\x20\x20\x20\x20__\|\x20\|_\x20\x20\x20_\x20_\x20_
SF:_\x20___\x20\x20\x20__\x20_\x20\x20\x20\x20___\x20\x20__\x20_\x20_\x20\
SF:x20\x20_\x20\x20__\x20_\|\x20\|\n\|\x20\x20_\|\x20\|\x20\|\x20\x20/\x20
SF:_`\x20\|\x20\|\x20\|\x20\|\x20'_\x20`\x20_\x20\\\x20/\x20_`\x20\|\x20\x
SF:20/\x20_\x20\\/\x20_`\x20\|\x20\|\x20\|\x20\|/\x20_`\x20\|\x20\|\n\|\x2
SF:0\|\x20\x20\x20\|\x20\|\x20\|\x20\(_\|\x20\|\x20\|_\|\x20\|\x20\|\x20\|
SF:\x20\|\x20\|\x20\|\x20\(_\|\x20\|\x20\|\x20\x20__/\x20\(_\|\x20\|\x20\|
SF:_\|\x20\|\x20\(_\|\x20\|_\|\n\\_\|\x20\x20\x20\|_\|\x20\x20\\__,_\|\\__
SF:,_\|_\|\x20\|_\|");
Aggressive OS guesses: Linux 3.11 - 4.1 (95%), Linux 4.4 (95%), Linux 3.16 (94%), Linux 3.13 (92%), Linux 3.10 - 3.12 (91%), Linux 2.6.32 (91%), Linux 3.2 - 3.8 (91%), Linux 3.8 (91%), WatchGuard Fireware 11.8 (91%), IPFire 2.11 firewall (Linux 2.6.32) (91%)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.92%E=4%D=11/6%OT=22%CT=1%CU=41633%PV=Y%DS=2%DC=T%G=Y%TM=6186659
OS:4%P=x86_64-pc-linux-gnu)SEQ(SP=101%GCD=1%ISR=10D%TI=Z%II=I%TS=8)SEQ(SP=1
OS:01%GCD=1%ISR=10D%TI=Z%TS=8)OPS(O1=M506ST11NW6%O2=M506ST11NW6%O3=M506NNT1
OS:1NW6%O4=M506ST11NW6%O5=M506ST11NW6%O6=M506ST11)WIN(W1=7120%W2=7120%W3=71
OS:20%W4=7120%W5=7120%W6=7120)ECN(R=Y%DF=Y%T=40%W=7210%O=M506NNSNW6%CC=Y%Q=
OS:)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=N)T5(R=Y%DF=Y
OS:%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=N)T7(R=N)U1(R=Y%DF=N%T=40%IPL=16
OS:4%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)U1(R=N)IE(R=Y%DFI=N%T=40%CD=S)

Uptime guess: 0.002 days (since Sat Nov  6 07:19:31 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=257 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 111/tcp)
HOP RTT      ADDRESS
1   98.90 ms 192.168.49.1
2   99.14 ms 192.168.129.48

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Nov  6 07:23:00 2021 -- 1 IP address (1 host up) scanned in 77.38 seconds

```
