```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/xml/_top_100_udp_nmap.xml" 192.168.129.48
```

[/home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.92 scan initiated Sat Nov  6 07:21:43 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Lampiao/results/192.168.129.48/scans/xml/_top_100_udp_nmap.xml 192.168.129.48
Increasing send delay for 192.168.129.48 from 50 to 100 due to 11 out of 12 dropped probes since last increase.
Increasing send delay for 192.168.129.48 from 100 to 200 due to 11 out of 12 dropped probes since last increase.
Increasing send delay for 192.168.129.48 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.129.48 from 400 to 800 due to 11 out of 12 dropped probes since last increase.
Nmap scan report for 192.168.129.48
Host is up, received user-set (0.10s latency).
Scanned at 2021-11-06 07:21:43 EDT for 297s

PORT      STATE         SERVICE         REASON              VERSION
7/udp     closed        echo            port-unreach ttl 63
9/udp     closed        discard         port-unreach ttl 63
17/udp    open|filtered qotd            no-response
19/udp    closed        chargen         port-unreach ttl 63
49/udp    open|filtered tacacs          no-response
53/udp    closed        domain          port-unreach ttl 63
67/udp    closed        dhcps           port-unreach ttl 63
68/udp    open|filtered dhcpc           no-response
69/udp    open|filtered tftp            no-response
80/udp    closed        http            port-unreach ttl 63
88/udp    closed        kerberos-sec    port-unreach ttl 63
111/udp   closed        rpcbind         port-unreach ttl 63
120/udp   closed        cfdptkt         port-unreach ttl 63
123/udp   closed        ntp             port-unreach ttl 63
135/udp   open|filtered msrpc           no-response
136/udp   open|filtered profile         no-response
137/udp   open|filtered netbios-ns      no-response
138/udp   closed        netbios-dgm     port-unreach ttl 63
139/udp   open|filtered netbios-ssn     no-response
158/udp   closed        pcmail-srv      port-unreach ttl 63
161/udp   open|filtered snmp            no-response
162/udp   closed        snmptrap        port-unreach ttl 63
177/udp   closed        xdmcp           port-unreach ttl 63
427/udp   open|filtered svrloc          no-response
443/udp   open|filtered https           no-response
445/udp   closed        microsoft-ds    port-unreach ttl 63
497/udp   open|filtered retrospect      no-response
500/udp   open|filtered isakmp          no-response
514/udp   closed        syslog          port-unreach ttl 63
515/udp   open|filtered printer         no-response
518/udp   closed        ntalk           port-unreach ttl 63
520/udp   closed        route           port-unreach ttl 63
593/udp   closed        http-rpc-epmap  port-unreach ttl 63
623/udp   open|filtered asf-rmcp        no-response
626/udp   closed        serialnumberd   port-unreach ttl 63
631/udp   closed        ipp             port-unreach ttl 63
996/udp   open|filtered vsinet          no-response
997/udp   open|filtered maitrd          no-response
998/udp   closed        puparp          port-unreach ttl 63
999/udp   closed        applix          port-unreach ttl 63
1022/udp  open|filtered exp2            no-response
1023/udp  open|filtered unknown         no-response
1025/udp  open|filtered blackjack       no-response
1026/udp  closed        win-rpc         port-unreach ttl 63
1027/udp  open|filtered unknown         no-response
1028/udp  closed        ms-lsa          port-unreach ttl 63
1029/udp  open|filtered solid-mux       no-response
1030/udp  closed        iad1            port-unreach ttl 63
1433/udp  open|filtered ms-sql-s        no-response
1434/udp  closed        ms-sql-m        port-unreach ttl 63
1645/udp  closed        radius          port-unreach ttl 63
1646/udp  closed        radacct         port-unreach ttl 63
1701/udp  closed        L2TP            port-unreach ttl 63
1718/udp  closed        h225gatedisc    port-unreach ttl 63
1719/udp  closed        h323gatestat    port-unreach ttl 63
1812/udp  open|filtered radius          no-response
1813/udp  closed        radacct         port-unreach ttl 63
1900/udp  closed        upnp            port-unreach ttl 63
2000/udp  open|filtered cisco-sccp      no-response
2048/udp  closed        dls-monitor     port-unreach ttl 63
2049/udp  closed        nfs             port-unreach ttl 63
2222/udp  open|filtered msantipiracy    no-response
2223/udp  open|filtered rockwell-csp2   no-response
3283/udp  closed        netassistant    port-unreach ttl 63
3456/udp  closed        IISrpc-or-vat   port-unreach ttl 63
3703/udp  closed        adobeserver-3   port-unreach ttl 63
4444/udp  closed        krb524          port-unreach ttl 63
4500/udp  closed        nat-t-ike       port-unreach ttl 63
5000/udp  closed        upnp            port-unreach ttl 63
5060/udp  closed        sip             port-unreach ttl 63
5353/udp  open|filtered zeroconf        no-response
5632/udp  closed        pcanywherestat  port-unreach ttl 63
9200/udp  open|filtered wap-wsp         no-response
10000/udp open|filtered ndmp            no-response
17185/udp open|filtered wdbrpc          no-response
20031/udp open|filtered bakbonenetvault no-response
30718/udp closed        unknown         port-unreach ttl 63
31337/udp closed        BackOrifice     port-unreach ttl 63
32768/udp closed        omad            port-unreach ttl 63
32769/udp closed        filenet-rpc     port-unreach ttl 63
32771/udp closed        sometimes-rpc6  port-unreach ttl 63
32815/udp closed        unknown         port-unreach ttl 63
33281/udp closed        unknown         port-unreach ttl 63
49152/udp closed        unknown         port-unreach ttl 63
49153/udp closed        unknown         port-unreach ttl 63
49154/udp closed        unknown         port-unreach ttl 63
49156/udp closed        unknown         port-unreach ttl 63
49181/udp open|filtered unknown         no-response
49182/udp open|filtered unknown         no-response
49185/udp closed        unknown         port-unreach ttl 63
49186/udp closed        unknown         port-unreach ttl 63
49188/udp closed        unknown         port-unreach ttl 63
49190/udp open|filtered unknown         no-response
49191/udp open|filtered unknown         no-response
49192/udp closed        unknown         port-unreach ttl 63
49193/udp closed        unknown         port-unreach ttl 63
49194/udp closed        unknown         port-unreach ttl 63
49200/udp closed        unknown         port-unreach ttl 63
49201/udp open|filtered unknown         no-response
65024/udp closed        unknown         port-unreach ttl 63
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.92%E=4%D=11/6%OT=%CT=%CU=7%PV=Y%DS=2%DC=T%G=N%TM=61866670%P=x86_64-pc-linux-gnu)
SEQ(II=I)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 67/udp)
HOP RTT       ADDRESS
1   101.57 ms 192.168.49.1
2   102.07 ms 192.168.129.48

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Nov  6 07:26:40 2021 -- 1 IP address (1 host up) scanned in 297.47 seconds

```
