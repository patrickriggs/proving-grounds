```bash
nmap -vv --reason -Pn -T4 -sV -p 80 --script="banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN "/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/tcp_80_http_nmap.txt" -oX "/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/xml/tcp_80_http_nmap.xml" 192.168.149.123
```

[/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/tcp_80_http_nmap.txt](file:///home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/tcp_80_http_nmap.txt):

```
# Nmap 7.91 scan initiated Sun Oct 24 14:08:38 2021 as: nmap -vv --reason -Pn -T4 -sV -p 80 "--script=banner,(http* or ssl*) and not (brute or broadcast or dos or external or http-slowloris* or fuzzer)" -oN /home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/tcp_80_http_nmap.txt -oX /home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/xml/tcp_80_http_nmap.xml 192.168.149.123
Nmap scan report for 192.168.149.123
Host is up, received user-set (0.096s latency).
Scanned at 2021-10-24 14:08:39 EDT for 26s

PORT   STATE SERVICE REASON         VERSION
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.38 ((Debian))
|_http-chrono: Request times for /; avg: 224.84ms; min: 207.53ms; max: 240.09ms
|_http-comments-displayer: Couldn't find any comments.
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-date: Sun, 24 Oct 2021 18:08:47 GMT; -1s from local time.
|_http-devframework: Couldn't determine the underlying framework or CMS. Try increasing 'httpspider.maxpagecount' value to spider more pages.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-drupal-enum: Nothing found amongst the top 100 resources,use --script-args number=<number|all> for deeper analysis)
| http-enum: 
|   /wordpress/: Blog
|   /robots.txt: Robots file
|_  /wordpress/wp-login.php: Wordpress login page.
|_http-errors: Couldn't find any error pages.
|_http-feed: Couldn't find any feeds.
|_http-fetch: Please enter the complete path of the directory to save data in.
| http-headers: 
|   Date: Sun, 24 Oct 2021 18:08:47 GMT
|   Server: Apache/2.4.38 (Debian)
|   Last-Modified: Mon, 14 Dec 2020 13:24:24 GMT
|   ETag: "17-5b66c8f90d927"
|   Accept-Ranges: bytes
|   Content-Length: 23
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)
|_http-jsonp-detection: Couldn't find any JSONP endpoints.
|_http-litespeed-sourcecode-download: Request with null byte did not work. This web server might not be vulnerable
|_http-malware-host: Host appears to be clean
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-mobileversion-checker: No mobile version detected.
| http-php-version: Logo query returned unknown hash d8f228c6ec8bed7d290d152f9532df00
|_Credits query returned unknown hash d8f228c6ec8bed7d290d152f9532df00
|_http-referer-checker: Couldn't find any cross-domain scripts.
|_http-security-headers: 
|_http-server-header: Apache/2.4.38 (Debian)
| http-sitemap-generator: 
|   Directory structure:
|     /
|       Other: 1
|   Longest directory structure:
|     Depth: 0
|     Dir: /
|   Total files found (by extension):
|_    Other: 1
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-title: Site doesn't have a title (text/html).
| http-useragent-tester: 
|   Status for browser useragent: 200
|   Allowed User Agents: 
|     Mozilla/5.0 (compatible; Nmap Scripting Engine; https://nmap.org/book/nse.html)
|     libwww
|     lwp-trivial
|     libcurl-agent/1.0
|     PHP/
|     Python-urllib/2.5
|     GT::WWW
|     Snoopy
|     MFC_Tear_Sample
|     HTTP::Lite
|     PHPCrawl
|     URI::Fetch
|     Zend_Http_Client
|     http client
|     PECL::HTTP
|     Wget/1.13.4 (linux-gnu)
|_    WWW-Mechanize/1.34
| http-vhosts: 
|_128 names had status 200
|_http-wordpress-enum: Nothing found amongst the top 100 resources,use --script-args search-limit=<number|all> for deeper analysis)
|_http-wordpress-users: [Error] Wordpress installation was not found. We couldn't find wp-login.php

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Oct 24 14:09:05 2021 -- 1 IP address (1 host up) scanned in 26.47 seconds

```
