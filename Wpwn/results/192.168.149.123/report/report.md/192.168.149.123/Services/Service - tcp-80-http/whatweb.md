```bash
whatweb --color=never --no-errors -a 3 -v http://192.168.149.123:80 2>&1
```

[/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/tcp_80_http_whatweb.txt](file:///home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/tcp80/tcp_80_http_whatweb.txt):

```
WhatWeb report for http://192.168.149.123:80
Status    : 200 OK
Title     : <None>
IP        : 192.168.149.123
Country   : RESERVED, ZZ

Summary   : Apache[2.4.38], HTTPServer[Debian Linux][Apache/2.4.38 (Debian)]

Detected Plugins:
[ Apache ]
	The Apache HTTP Server Project is an effort to develop and
	maintain an open-source HTTP server for modern operating
	systems including UNIX and Windows NT. The goal of this
	project is to provide a secure, efficient and extensible
	server that provides HTTP services in sync with the current
	HTTP standards.

	Version      : 2.4.38 (from HTTP Server Header)
	Google Dorks: (3)
	Website     : http://httpd.apache.org/

[ HTTPServer ]
	HTTP server header string. This plugin also attempts to
	identify the operating system from the server header.

	OS           : Debian Linux
	String       : Apache/2.4.38 (Debian) (from server string)

HTTP Headers:
	HTTP/1.1 200 OK
	Date: Sun, 24 Oct 2021 18:08:41 GMT
	Server: Apache/2.4.38 (Debian)
	Last-Modified: Mon, 14 Dec 2020 13:24:24 GMT
	ETag: "17-5b66c8f90d927"
	Accept-Ranges: bytes
	Content-Length: 23
	Connection: close
	Content-Type: text/html



```
