```bash
nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN "/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_top_100_udp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/xml/_top_100_udp_nmap.xml" 192.168.149.123
```

[/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_top_100_udp_nmap.txt](file:///home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_top_100_udp_nmap.txt):

```
# Nmap 7.91 scan initiated Sun Oct 24 14:08:14 2021 as: nmap -vv --reason -Pn -T4 -sU -A --top-ports 100 -oN /home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_top_100_udp_nmap.txt -oX /home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/xml/_top_100_udp_nmap.xml 192.168.149.123
Warning: 192.168.149.123 giving up on port because retransmission cap hit (6).
Increasing send delay for 192.168.149.123 from 100 to 200 due to 11 out of 12 dropped probes since last increase.
Increasing send delay for 192.168.149.123 from 200 to 400 due to 11 out of 11 dropped probes since last increase.
Increasing send delay for 192.168.149.123 from 400 to 800 due to 11 out of 11 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -53459 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -204756 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -204756 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -431882 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -431882 microseconds.  Ignoring time.
Nmap scan report for 192.168.149.123
Host is up, received user-set (0.097s latency).
Scanned at 2021-10-24 14:08:14 EDT for 195s
Not shown: 89 closed ports
Reason: 89 port-unreaches
PORT      STATE         SERVICE        REASON      VERSION
7/udp     open|filtered echo           no-response
9/udp     open|filtered discard        no-response
593/udp   open|filtered http-rpc-epmap no-response
1028/udp  open|filtered ms-lsa         no-response
1434/udp  open|filtered ms-sql-m       no-response
1701/udp  open|filtered L2TP           no-response
5632/udp  open|filtered pcanywherestat no-response
30718/udp open|filtered unknown        no-response
32815/udp open|filtered unknown        no-response
33281/udp open|filtered unknown        no-response
49192/udp open|filtered unknown        no-response
Too many fingerprints match this host to give specific OS details
TCP/IP fingerprint:
SCAN(V=7.91%E=4%D=10/24%OT=%CT=%CU=17%PV=Y%DS=2%DC=T%G=N%TM=6175A1D1%P=x86_64-pc-linux-gnu)
T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)
IE(R=Y%DFI=N%T=40%CD=S)

Network Distance: 2 hops

TRACEROUTE (using port 19/udp)
HOP RTT      ADDRESS
1   96.66 ms 192.168.49.1
2   96.77 ms 192.168.149.123

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Oct 24 14:11:29 2021 -- 1 IP address (1 host up) scanned in 195.75 seconds

```
