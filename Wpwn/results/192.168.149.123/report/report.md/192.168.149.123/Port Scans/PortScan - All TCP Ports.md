```bash
nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN "/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_full_tcp_nmap.txt" -oX "/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/xml/_full_tcp_nmap.xml" 192.168.149.123
```

[/home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_full_tcp_nmap.txt](file:///home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_full_tcp_nmap.txt):

```
# Nmap 7.91 scan initiated Sun Oct 24 14:08:14 2021 as: nmap -vv --reason -Pn -T4 -sV -sC --version-all -A --osscan-guess -p- -oN /home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/_full_tcp_nmap.txt -oX /home/tac0shell/hacking/pg/Wpwn/results/192.168.149.123/scans/xml/_full_tcp_nmap.xml 192.168.149.123
Increasing send delay for 192.168.149.123 from 0 to 5 due to 117 out of 292 dropped probes since last increase.
adjust_timeouts2: packet supposedly had rtt of -159556 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -159556 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -130721 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -130721 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -132460 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -132460 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -131961 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -131961 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -182908 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -182908 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -158668 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -158668 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -82332 microseconds.  Ignoring time.
adjust_timeouts2: packet supposedly had rtt of -82332 microseconds.  Ignoring time.
Nmap scan report for 192.168.149.123
Host is up, received user-set (0.097s latency).
Scanned at 2021-10-24 14:08:14 EDT for 496s
Not shown: 65533 closed ports
Reason: 65533 resets
PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 59:b7:db:e0:ba:63:76:af:d0:20:03:11:e1:3c:0e:34 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMeW+2AyVvlr6ePuYubrIG/bVmu/K0Ids1BYbag6YJINa5mbbPE2ATbqsOnKaBhyRSDCpRr7vdn+jAUhuLhf2VogMckwyBgd5/RLDaBTLrvQwE5KidaCHrPElMcuidzcBCoAmK41o/H/w1zdBpM5Fh8ySMr7WMNCDMON00sKoPecMVxWIxzXmfZXBvSdsSk2zJAP6ds+JGduvsFFCGuoIY4A3tLGW1ZQlALkZIt143KvkQrg4rXRjgVbSvryh6a5GJskvGA3QNpUiebqMHC1zXMrjfBoi/SX944LQ0hVLfuXTriH5QkzRhLxkN+K+lvkrGN5RzAqF3IhGIfJcEp7f1
|   256 2e:20:56:75:84:ca:35:ce:e3:6a:21:32:1f:e7:f5:9a (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNe7JLcAbKYhJdELk+ajEn9c68tE7GIr28etvuPibQZZIMFLwM/+Zso6zsYbUOptgjA0+y6YP1geoSoy8CQse9U=
|   256 0d:02:83:8b:1a:1c:ec:0f:ae:74:cc:7b:da:12:89:9e (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKZdajYQt+tMB0kowHtm64fUkzCdJbSS1dYaS/bWQrWJ
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.38 ((Debian))
| http-methods: 
|_  Supported Methods: GET POST OPTIONS HEAD
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Site doesn't have a title (text/html).
Aggressive OS guesses: Linux 2.6.32 (91%), Linux 2.6.39 (91%), Linux 3.10 - 3.12 (91%), Linux 3.4 (91%), Linux 3.5 (91%), Linux 4.2 (91%), Linux 4.4 (91%), Synology DiskStation Manager 5.1 (91%), WatchGuard Fireware 11.8 (91%), Linux 2.6.35 (90%)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.91%E=4%D=10/24%OT=22%CT=1%CU=43782%PV=Y%DS=2%DC=T%G=Y%TM=6175A2
OS:FE%P=x86_64-pc-linux-gnu)SEQ(SP=105%GCD=1%ISR=10B%TI=Z%TS=A)SEQ(SP=105%G
OS:CD=1%ISR=10B%TI=Z%II=I%TS=A)OPS(O1=M506ST11NW7%O2=M506ST11NW7%O3=M506NNT
OS:11NW7%O4=M506ST11NW7%O5=M506ST11NW7%O6=M506ST11)WIN(W1=FE88%W2=FE88%W3=F
OS:E88%W4=FE88%W5=FE88%W6=FE88)ECN(R=Y%DF=Y%T=40%W=FAF0%O=M506NNSNW7%CC=Y%Q
OS:=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=N)T5(R=Y%DF=
OS:Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=N)T7(R=N)U1(R=Y%DF=N%T=40%IPL=1
OS:64%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%CD=S)

Uptime guess: 33.076 days (since Tue Sep 21 12:27:15 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=261 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 8888/tcp)
HOP RTT      ADDRESS
1   96.72 ms 192.168.49.1
2   97.62 ms 192.168.149.123

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun Oct 24 14:16:30 2021 -- 1 IP address (1 host up) scanned in 496.73 seconds

```
